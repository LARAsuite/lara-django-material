"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material tests *

:details: lara_django_material application models tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

# from lara_django_material.models import

# Create your lara_django_material tests here.


from django.test import TestCase
from unittest import skip

from django.utils import timezone
from django.urls import reverse


from ..models import ExtraData, PartDeviceClass, PartShape, Part, Device, DeviceSetup, LabwareClass, WellShape, LabwareShape, Labware


class ExtraDataTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.extradata_setup()
        pass

    def test_extradata_model(self):
        # extradata_item = ExtraData.objects.create(extradata_id=None, data_type=None, text=None, XML=None, JSON=None, bin=None, file_type=None, IRI=None, url=None, description=None, image=None, file=None)
        # extradata_item_search = ExtraData.objects.get(<filter>)

        # self.assertEqual(extradata_item_search, <value>)
        pass


class PartDeviceClassTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.partdeviceclass_setup()
        pass

    def test_partdeviceclass_model(self):
        # partdeviceclass_item = PartDeviceClass.objects.create(class_id=None, name=None, description=None)
        # partdeviceclass_item_search = PartDeviceClass.objects.get(<filter>)

        # self.assertEqual(partdeviceclass_item_search, <value>)
        pass


class PartShapeTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.partshape_setup()
        pass

    def test_partshape_model(self):
        # partshape_item = PartShape.objects.create(name=None, name_standard=None, width=None, length=None, height_unlidded=None, hight_lidded=None, hight_stacked=None, hight_stacked_lidded=None, model=None, description=None, partshape_id=None)
        # partshape_item_search = PartShape.objects.get(<filter>)

        # self.assertEqual(partshape_item_search, <value>)
        pass


class PartTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.part_setup()
        pass

    def test_part_model(self):
        # part_item = Part.objects.create(name=None, name_full=None, url=None, handle=None, IRI=None, manufacturer=None, model_no=None, product_type=None, type_barcode=None, product_no=None, weight=None, spec_json=None, spec_json=None, icon=None, image=None, description=None, part_id=None, part_class=None, shape=None)
        # part_item_search = Part.objects.get(<filter>)

        # self.assertEqual(part_item_search, <value>)
        pass


class DeviceTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.device_setup()
        pass

    def test_device_model(self):
        # device_item = Device.objects.create(name=None, name_full=None, url=None, handle=None, IRI=None, manufacturer=None, model_no=None, product_type=None, type_barcode=None, product_no=None, weight=None, spec_json=None, spec_json=None, icon=None, image=None, description=None, device_id=None, device_class=None, shape=None)
        # device_item_search = Device.objects.get(<filter>)

        # self.assertEqual(device_item_search, <value>)
        pass


class DeviceSetupTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.devicesetup_setup()
        pass

    def test_devicesetup_model(self):
        # devicesetup_item = DeviceSetup.objects.create(name=None, name_full=None, url=None, handle=None, IRI=None, manufacturer=None, model_no=None, product_type=None, type_barcode=None, product_no=None, weight=None, spec_json=None, spec_json=None, icon=None, image=None, description=None, device_id=None, setup_class=None, blueprint_image=None, blueprint_file_type=None, blueprint_file=None, blueprint_pdf=None, scheme_svg=None, assembly=None)
        # devicesetup_item_search = DeviceSetup.objects.get(<filter>)

        # self.assertEqual(devicesetup_item_search, <value>)
        pass


class LabwareClassTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.labwareclass_setup()
        pass

    def test_labwareclass_model(self):
        # labwareclass_item = LabwareClass.objects.create(class_id=None, name=None, description=None)
        # labwareclass_item_search = LabwareClass.objects.get(<filter>)

        # self.assertEqual(labwareclass_item_search, <value>)
        pass


class WellShapeTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.wellshape_setup()
        pass

    def test_wellshape_model(self):
        # wellshape_item = WellShape.objects.create(wellshape_id=None, depth_well=None, shape_well=None, shape_botton=None, top_radius_xy=None, bottom_radius_xy=None, bottom_radius_z=None, cone_angle=None, cone_depth=None, shape_polygon_xy=None, shape_polygon_z=None, model=None)
        # wellshape_item_search = WellShape.objects.get(<filter>)

        # self.assertEqual(wellshape_item_search, <value>)
        pass


class LabwareShapeTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.labwareshape_setup()
        pass

    def test_labwareshape_model(self):
        # labwareshape_item = LabwareShape.objects.create(name=None, name_standard=None, width=None, length=None, height_unlidded=None, hight_lidded=None, hight_stacked=None, hight_stacked_lidded=None, model=None, labwareshape_id=None, well_dist_row=None, well_dist_col=None, well_shape=None, description=None)
        # labwareshape_item_search = LabwareShape.objects.get(<filter>)

        # self.assertEqual(labwareshape_item_search, <value>)
        pass


class LabwareTestCase(TestCase):
    def setUp(self):
        """ Test setup method """
        # call setup function
        # self.labware_setup()
        pass

    def test_labware_model(self):
        # labware_item = Labware.objects.create(name=None, name_full=None, url=None, handle=None, IRI=None, manufacturer=None, model_no=None, product_type=None, type_barcode=None, product_no=None, weight=None, spec_json=None, spec_json=None, icon=None, image=None, description=None, labware_id=None, labware_class=None, num_rows=None, num_cols=None, num_of_wells=None, shape=None, lid=None, liddable=None, sealable=None)
        # labware_item_search = Labware.objects.get(<filter>)

        # self.assertEqual(labware_item_search, <value>)
        pass
