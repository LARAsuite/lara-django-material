"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material tests *

:details: lara_django_material application views tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase

import pytest
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest, HttpResponseRedirect
from django.test import RequestFactory
from django.urls import reverse


# from lara_django_material.models import

# Create your lara_django_material tests here.

pytestmark = pytest.mark.django_db
