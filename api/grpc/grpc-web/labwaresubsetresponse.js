// source: lara_django_material_grpc/v1/lara_django_material.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.lara_django.lara_django_material.LabwareSubsetResponse');

goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');
goog.require('jspb.Message');

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.lara_django.lara_django_material.LabwareSubsetResponse.repeatedFields_, null);
};
goog.inherits(proto.lara_django.lara_django_material.LabwareSubsetResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.lara_django.lara_django_material.LabwareSubsetResponse.displayName = 'proto.lara_django.lara_django_material.LabwareSubsetResponse';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.repeatedFields_ = [5,6];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.lara_django.lara_django_material.LabwareSubsetResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.lara_django.lara_django_material.LabwareSubsetResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    labwaresubsetId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    namespace: jspb.Message.getFieldWithDefault(msg, 2, ""),
    group: jspb.Message.getFieldWithDefault(msg, 4, ""),
    labwareList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f,
    tagsList: (f = jspb.Message.getRepeatedField(msg, 6)) == null ? undefined : f,
    name: jspb.Message.getFieldWithDefault(msg, 7, ""),
    description: jspb.Message.getFieldWithDefault(msg, 8, ""),
    datetimeLastModified: jspb.Message.getFieldWithDefault(msg, 9, ""),
    nameDisplay: jspb.Message.getFieldWithDefault(msg, 10, ""),
    entity: jspb.Message.getFieldWithDefault(msg, 11, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.lara_django.lara_django_material.LabwareSubsetResponse;
  return proto.lara_django.lara_django_material.LabwareSubsetResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.lara_django.lara_django_material.LabwareSubsetResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLabwaresubsetId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNamespace(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setGroup(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.addLabware(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.addTags(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatetimeLastModified(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setNameDisplay(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setEntity(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.lara_django.lara_django_material.LabwareSubsetResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.lara_django.lara_django_material.LabwareSubsetResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getNamespace();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getLabwareList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      5,
      f
    );
  }
  f = message.getTagsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      6,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 8));
  if (f != null) {
    writer.writeString(
      8,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 9));
  if (f != null) {
    writer.writeString(
      9,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 10));
  if (f != null) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getEntity();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
};


/**
 * optional string labwaresubset_id = 1;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getLabwaresubsetId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setLabwaresubsetId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearLabwaresubsetId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.hasLabwaresubsetId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string namespace = 2;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getNamespace = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setNamespace = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string group = 4;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getGroup = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setGroup = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearGroup = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.hasGroup = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated string labware = 5;
 * @return {!Array<string>}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getLabwareList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setLabwareList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.addLabware = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearLabwareList = function() {
  return this.setLabwareList([]);
};


/**
 * repeated string tags = 6;
 * @return {!Array<string>}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getTagsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 6));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setTagsList = function(value) {
  return jspb.Message.setField(this, 6, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.addTags = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 6, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearTagsList = function() {
  return this.setTagsList([]);
};


/**
 * optional string name = 7;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string description = 8;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setDescription = function(value) {
  return jspb.Message.setField(this, 8, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearDescription = function() {
  return jspb.Message.setField(this, 8, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.hasDescription = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional string datetime_last_modified = 9;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getDatetimeLastModified = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setDatetimeLastModified = function(value) {
  return jspb.Message.setField(this, 9, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearDatetimeLastModified = function() {
  return jspb.Message.setField(this, 9, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.hasDatetimeLastModified = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string name_display = 10;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getNameDisplay = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setNameDisplay = function(value) {
  return jspb.Message.setField(this, 10, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.clearNameDisplay = function() {
  return jspb.Message.setField(this, 10, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.hasNameDisplay = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional string entity = 11;
 * @return {string}
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.getEntity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.LabwareSubsetResponse} returns this
 */
proto.lara_django.lara_django_material.LabwareSubsetResponse.prototype.setEntity = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


