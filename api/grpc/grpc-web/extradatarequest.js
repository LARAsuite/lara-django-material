// source: lara_django_material_grpc/v1/lara_django_material.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.lara_django.lara_django_material.ExtraDataRequest');

goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');
goog.require('jspb.Message');
goog.require('proto.google.protobuf.Struct');

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.lara_django.lara_django_material.ExtraDataRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.lara_django.lara_django_material.ExtraDataRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.lara_django.lara_django_material.ExtraDataRequest.displayName = 'proto.lara_django.lara_django_material.ExtraDataRequest';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.lara_django.lara_django_material.ExtraDataRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.lara_django.lara_django_material.ExtraDataRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.lara_django.lara_django_material.ExtraDataRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    extradataId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    dataType: jspb.Message.getFieldWithDefault(msg, 2, ""),
    namespace: jspb.Message.getFieldWithDefault(msg, 3, ""),
    mediaType: jspb.Message.getFieldWithDefault(msg, 4, ""),
    name: jspb.Message.getFieldWithDefault(msg, 6, ""),
    nameFull: jspb.Message.getFieldWithDefault(msg, 7, ""),
    version: jspb.Message.getFieldWithDefault(msg, 8, ""),
    hashSha256: jspb.Message.getFieldWithDefault(msg, 9, ""),
    dataJson: (f = msg.getDataJson()) && proto.google.protobuf.Struct.toObject(includeInstance, f),
    iri: jspb.Message.getFieldWithDefault(msg, 11, ""),
    url: jspb.Message.getFieldWithDefault(msg, 12, ""),
    datetimeCreated: jspb.Message.getFieldWithDefault(msg, 13, ""),
    datetimeLastModified: jspb.Message.getFieldWithDefault(msg, 14, ""),
    description: jspb.Message.getFieldWithDefault(msg, 15, ""),
    image: jspb.Message.getFieldWithDefault(msg, 16, ""),
    file: jspb.Message.getFieldWithDefault(msg, 17, ""),
    nameDisplay: jspb.Message.getFieldWithDefault(msg, 18, ""),
    searchVector: jspb.Message.getFieldWithDefault(msg, 19, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.lara_django.lara_django_material.ExtraDataRequest;
  return proto.lara_django.lara_django_material.ExtraDataRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.lara_django.lara_django_material.ExtraDataRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setExtradataId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDataType(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setNamespace(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMediaType(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setNameFull(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setVersion(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setHashSha256(value);
      break;
    case 10:
      var value = new proto.google.protobuf.Struct;
      reader.readMessage(value,proto.google.protobuf.Struct.deserializeBinaryFromReader);
      msg.setDataJson(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setIri(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatetimeCreated(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setDatetimeLastModified(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setImage(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setFile(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.setNameDisplay(value);
      break;
    case 19:
      var value = /** @type {string} */ (reader.readString());
      msg.setSearchVector(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.lara_django.lara_django_material.ExtraDataRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.lara_django.lara_django_material.ExtraDataRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.lara_django.lara_django_material.ExtraDataRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getNamespace();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 7));
  if (f != null) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getVersion();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 9));
  if (f != null) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getDataJson();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.google.protobuf.Struct.serializeBinaryToWriter
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 11));
  if (f != null) {
    writer.writeString(
      11,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 12));
  if (f != null) {
    writer.writeString(
      12,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 13));
  if (f != null) {
    writer.writeString(
      13,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 14));
  if (f != null) {
    writer.writeString(
      14,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 15));
  if (f != null) {
    writer.writeString(
      15,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 16));
  if (f != null) {
    writer.writeString(
      16,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 17));
  if (f != null) {
    writer.writeString(
      17,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 18));
  if (f != null) {
    writer.writeString(
      18,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 19));
  if (f != null) {
    writer.writeString(
      19,
      f
    );
  }
};


/**
 * optional string extradata_id = 1;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getExtradataId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setExtradataId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearExtradataId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasExtradataId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string data_type = 2;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getDataType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setDataType = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearDataType = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasDataType = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string namespace = 3;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getNamespace = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setNamespace = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string media_type = 4;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getMediaType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setMediaType = function(value) {
  return jspb.Message.setField(this, 4, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearMediaType = function() {
  return jspb.Message.setField(this, 4, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasMediaType = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional string name = 6;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string name_full = 7;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getNameFull = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setNameFull = function(value) {
  return jspb.Message.setField(this, 7, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearNameFull = function() {
  return jspb.Message.setField(this, 7, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasNameFull = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional string version = 8;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getVersion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setVersion = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string hash_sha256 = 9;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getHashSha256 = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setHashSha256 = function(value) {
  return jspb.Message.setField(this, 9, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearHashSha256 = function() {
  return jspb.Message.setField(this, 9, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasHashSha256 = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional google.protobuf.Struct data_json = 10;
 * @return {?proto.google.protobuf.Struct}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getDataJson = function() {
  return /** @type{?proto.google.protobuf.Struct} */ (
    jspb.Message.getWrapperField(this, proto.google.protobuf.Struct, 10));
};


/**
 * @param {?proto.google.protobuf.Struct|undefined} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
*/
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setDataJson = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearDataJson = function() {
  return this.setDataJson(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasDataJson = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional string iri = 11;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getIri = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setIri = function(value) {
  return jspb.Message.setField(this, 11, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearIri = function() {
  return jspb.Message.setField(this, 11, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasIri = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional string url = 12;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setUrl = function(value) {
  return jspb.Message.setField(this, 12, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearUrl = function() {
  return jspb.Message.setField(this, 12, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasUrl = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional string datetime_created = 13;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getDatetimeCreated = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setDatetimeCreated = function(value) {
  return jspb.Message.setField(this, 13, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearDatetimeCreated = function() {
  return jspb.Message.setField(this, 13, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasDatetimeCreated = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional string datetime_last_modified = 14;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getDatetimeLastModified = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setDatetimeLastModified = function(value) {
  return jspb.Message.setField(this, 14, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearDatetimeLastModified = function() {
  return jspb.Message.setField(this, 14, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasDatetimeLastModified = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional string description = 15;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setDescription = function(value) {
  return jspb.Message.setField(this, 15, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearDescription = function() {
  return jspb.Message.setField(this, 15, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasDescription = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional string image = 16;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getImage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setImage = function(value) {
  return jspb.Message.setField(this, 16, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearImage = function() {
  return jspb.Message.setField(this, 16, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasImage = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional string file = 17;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getFile = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setFile = function(value) {
  return jspb.Message.setField(this, 17, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearFile = function() {
  return jspb.Message.setField(this, 17, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasFile = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional string name_display = 18;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getNameDisplay = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 18, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setNameDisplay = function(value) {
  return jspb.Message.setField(this, 18, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearNameDisplay = function() {
  return jspb.Message.setField(this, 18, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasNameDisplay = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional string search_vector = 19;
 * @return {string}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.getSearchVector = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 19, ""));
};


/**
 * @param {string} value
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.setSearchVector = function(value) {
  return jspb.Message.setField(this, 19, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.lara_django.lara_django_material.ExtraDataRequest} returns this
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.clearSearchVector = function() {
  return jspb.Message.setField(this, 19, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.lara_django.lara_django_material.ExtraDataRequest.prototype.hasSearchVector = function() {
  return jspb.Message.getField(this, 19) != null;
};


