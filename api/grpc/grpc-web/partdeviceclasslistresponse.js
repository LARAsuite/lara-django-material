// source: lara_django_material_grpc/v1/lara_django_material.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.lara_django.lara_django_material.PartDeviceClassListResponse');

goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');
goog.require('jspb.Message');
goog.require('proto.lara_django.lara_django_material.PartDeviceClassResponse');

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.lara_django.lara_django_material.PartDeviceClassListResponse.repeatedFields_, null);
};
goog.inherits(proto.lara_django.lara_django_material.PartDeviceClassListResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.lara_django.lara_django_material.PartDeviceClassListResponse.displayName = 'proto.lara_django.lara_django_material.PartDeviceClassListResponse';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.lara_django.lara_django_material.PartDeviceClassListResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.lara_django.lara_django_material.PartDeviceClassListResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    resultsList: jspb.Message.toObjectList(msg.getResultsList(),
    proto.lara_django.lara_django_material.PartDeviceClassResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.lara_django.lara_django_material.PartDeviceClassListResponse}
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.lara_django.lara_django_material.PartDeviceClassListResponse;
  return proto.lara_django.lara_django_material.PartDeviceClassListResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.lara_django.lara_django_material.PartDeviceClassListResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.lara_django.lara_django_material.PartDeviceClassListResponse}
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.lara_django.lara_django_material.PartDeviceClassResponse;
      reader.readMessage(value,proto.lara_django.lara_django_material.PartDeviceClassResponse.deserializeBinaryFromReader);
      msg.addResults(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.lara_django.lara_django_material.PartDeviceClassListResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.lara_django.lara_django_material.PartDeviceClassListResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getResultsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.lara_django.lara_django_material.PartDeviceClassResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated PartDeviceClassResponse results = 1;
 * @return {!Array<!proto.lara_django.lara_django_material.PartDeviceClassResponse>}
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.prototype.getResultsList = function() {
  return /** @type{!Array<!proto.lara_django.lara_django_material.PartDeviceClassResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.lara_django.lara_django_material.PartDeviceClassResponse, 1));
};


/**
 * @param {!Array<!proto.lara_django.lara_django_material.PartDeviceClassResponse>} value
 * @return {!proto.lara_django.lara_django_material.PartDeviceClassListResponse} returns this
*/
proto.lara_django.lara_django_material.PartDeviceClassListResponse.prototype.setResultsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.lara_django.lara_django_material.PartDeviceClassResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.lara_django.lara_django_material.PartDeviceClassResponse}
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.prototype.addResults = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.lara_django.lara_django_material.PartDeviceClassResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.lara_django.lara_django_material.PartDeviceClassListResponse} returns this
 */
proto.lara_django.lara_django_material.PartDeviceClassListResponse.prototype.clearResultsList = function() {
  return this.setResultsList([]);
};


