"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material high_level_interface *

:details: lara_django_material high_level_interface.
         - generated with 'lara-django-dev generate_high_level_interface lara_django_material
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

import json
import asyncio
import logging
import grpc
from typing import Union

from google.protobuf.struct_pb2 import Struct
from google.protobuf.json_format import MessageToDict

import lara_django_base_grpc.v1.lara_django_base_pb2 as lara_django_base_pb2
import lara_django_base_grpc.v1.lara_django_base_pb2_grpc as lara_django_base_pb2_grpc

import lara_django_material_grpc.v1.lara_django_material_pb2 as lara_django_material_pb2
import lara_django_material_grpc.v1.lara_django_material_pb2_grpc as lara_django_material_pb2_grpc

import  lara_django_base_grpc.lara_django_base_data_model as base_dm
import  lara_django_base_grpc.lara_django_base_interface as base_if
from  lara_django_base_grpc.abstract_interface import LARAInterface, GRPCChannelSingleton, import_model_instance_from_file, export_model_instance_to_file, upload_file, download_file, update_file, upload_image, update_image, download_image, id_cache

import  lara_django_material_grpc.lara_django_material_data_model as material_dm

logger = logging.getLogger(__name__)



#_________________  ExtraData  ______________________


class ExtraDataInterface:
    def __init__(self, channel: grpc.Channel = None, 
                       default_namespace_uri : str = None,
                       default_namespace_id : str = None,
                       namespace_if = None ) -> None:
        self._default_namespace_uri = default_namespace_uri
        self._default_namespace_id = default_namespace_id

        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.namespace_if = namespace_if if namespace_if is not None else base_if.NamespaceInterface(channel)

        # self.extradata_class_client = (
        #     lara_django_material_pb2_grpc.ExtraDataclassByIRIControllerStub(channel)
        # )

        self.extradata_client = lara_django_material_pb2_grpc.ExtraDataControllerStub(channel)
        
        self.item_id = None
        self.update_item_id = "extradata_id"
        self.stub = self.extradata_client

        self.file_download_info = lara_django_material_pb2.FileDownloadInfo
        self.file_upload_chunk = lara_django_material_pb2.FileUploadChunk

        self.image_upload_chunk = lara_django_material_pb2.ImageUploadChunk


        # self.extradata_full_client = lara_django_material_pb2_grpc.ExtraDataFullControllerStub(
        #     channel
        # )

    @property
    def default_namespace_uri(self):
        return self._default_namespace_uri

    @property
    def default_namespace_id(self):
        return self._default_namespace_id

    @classmethod
    async def build_if(cls, channel: grpc.Channel = None, default_namespace_uri: str = None):
    
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel
    
        namespace_if = base_if.NamespaceInterface(channel)
        try:
            default_namespace_id = await namespace_if.retrieve_id(namespace_uri=default_namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
        
        return cls(channel=channel, default_namespace_uri=default_namespace_uri,  
                   default_namespace_id=default_namespace_id, namespace_if=namespace_if)
    
    async def set_default_namespace_uri(self, namespace_uri: str = None):
        if namespace_uri is not None:
            self._default_namespace_uri = namespace_uri.strip()
        try:
            self._default_namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
    
    @import_model_instance_from_file()
    @upload_file  # needed for file upload
    @upload_image  # needed for image upload
    async def create(self,  extradata:  list[material_dm.ExtraData] = None,  
                            ids_only: bool = False,
                            namespace_uri: str = None) -> Union[list[str], list[material_dm.ExtraData]]:
        """ create a list of extradata instances,
               returns a list of extradata data model objects or ids_only

        :param extradata: list of extradata data model objects
        :param ids_only: return only the ids of the created instances
        :return: list of extradata data model objects or ids_only
        """
        extradataclass_id = None

        namespace_id = None
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
            namespace_id = self._default_namespace_id
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        if namespace_id is None:
            try:
                namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
            except Exception as e:
                logger.error(f"Error retrieving namespace_id: {e}")

        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if extradata_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # try:
        #     res = await self.extradata_class_client.Retrieve(
        #         lara_django_material_pb2.ExtraDataclassRetrieveRequest(iri=extradata_class_iri)
        #     )
        #     extradataclass_id = res.extradataclass_id
        # except Exception as e:
        #     logger.error(f"Error retrieving extradataclass_id: {e}")
        for extradata in extradata:
            if extradata.namespace == "" or extradata.namespace == "default":
                    extradata.namespace = namespace_id
            # extradata.extradata_class = extradataclass_id # uncomment if a model class is used

        try:
            create_coroutines = ( self.extradata_client.Create(lara_django_material_pb2.ExtraDataRequest(**extradata.model_dump())) for extradata in extradata )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [ MessageToDict(item, preserving_proto_field_name=True).get("extradata_id") for item in res ]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating extradata: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        namespace_uri: str = None,
        extradata_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.ExtraData]:
        """ retrieve a list of extradata instances by extradata_id, name or filter_dict,
               returns a list of extradata data model objects,
               if raw is True, the raw protobuf objects are returned.
               If namespace_uri is "default", the default namespace_uri is used.
        """
        if namespace_uri == "default":
            if self._default_namespace_uri is not None:
                namespace_uri = self._default_namespace_uri
            else:
                raise ValueError("No default namespace URI is given.")
        
        results_list = []
        if  extradata_id is not None:
            try:
                res =  await self.extradata_client.Retrieve(
                    lara_django_material_pb2.ExtraDataRetrieveRequest(extradata_id=extradata_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.ExtraData(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving extradata_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        if namespace_uri is not None:
            filter_dict["namespace__uri"] = namespace_uri
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.extradata_client.List(
                    lara_django_material_pb2.ExtraDataListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # :TODO: a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.ExtraData(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving extradata name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          namespace_uri : str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the extradata_id for a given name,
               name_display or iri
        """

        results_list = await self.retrieve(name=name, 
                        namespace_uri=namespace_uri, filter_dict=filter_dict, raw=True)

        if len(results_list) == 1 :
            return results_list[0].extradata_id
        else:
            raise ValueError(f"Error retrieving extradata_id - no or too many results found.")
    
    @download_file  # needed for file download
    @download_image  # needed for image download
    async def get_by_id(self, extradata_id: str = None, id_only: bool = False) -> material_dm.ExtraData:
        """ retrieve a extradata data model by extradata_id """
        try:
            res = await self.extradata_client.Retrieve(
                lara_django_material_pb2.ExtraDataRetrieveRequest(extradata_id=extradata_id)
            )
            item = material_dm.ExtraData(**MessageToDict(res, preserving_proto_field_name=True))
            self.item_id = item.extradata_id
            return item
        except Exception as e:
            logger.error(f"Error retrieving extradata_id: {e}")
    
    @download_file  # needed for file download
    @download_image  # needed for image download
    async def get_by_name(self, name: str = None,
                                namespace_uri : str = None,
                                id_only: bool = False) -> material_dm.ExtraData | str:
        """ retrieve a extradata data model by name """
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        try:
            res = await self.extradata_client.RetrieveByNameNamespace(
                lara_django_material_pb2.ExtraDataRetrieveByNameNSRequest(name=name, namespace_uri=namespace_uri)
            )
            item = MessageToDict(res, preserving_proto_field_name=True)
            self.item_id = item["extradata_id"]
            if id_only:
                return self.item_id
            else:
                return material_dm.ExtraData(**item)
        except Exception as e:
            logger.error(f"Error retrieving extradata name: {e}")
   
    
    @download_file  # needed for file download
    async def get_file(self, extradata_id : str = None, id_only : bool = True ) -> bytes | None:
        """ retrieve a file by data_id """
        self.item_id = extradata_id
        
    
    async def get_file_by_name(self, name: str = None,
                                namespace_uri : str = None,
                                filename_target : str = None,
                                as_byte_str : bool = None ) -> bytes | None:
        """ retrieve a file by name """
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
            # logger.warning(f"no namespace_uri provided, using default namespace: {namespace_uri}")
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        try:
            extradata_id = await self.get_by_name(name=name, namespace_uri=namespace_uri, id_only=True)
            return await self.get_file(extradata_id=extradata_id, filename_target=filename_target, as_byte_str=as_byte_str, get_file=True)
        except Exception as e:
            logger.error(f"Error retrieving extradata name: {e}")
        
    
    @download_image  # needed for image download
    async def get_image(self, extradata_id : str = None, id_only : bool = True) -> bytes | None:
        """ retrieve an image by data_id """
        self.item_id = extradata_id
        
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all extradatas """
        if filter_dict is None:
            res = await self.extradata_client.List(lara_django_material_pb2.ExtraDataListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.extradata_client.List(
                lara_django_material_pb2.ExtraDataListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.extradata_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all extradatas """
        if self.extradata_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.extradata_full_client.List(
                lara_django_material_pb2.ExtraDataFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.extradata_full_client.List(
                lara_django_material_pb2.ExtraDataFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    @update_file  # needed for file update
    @update_image  # needed for image update
    async def update(self, extradata : material_dm.ExtraData = None) -> None:
        """ update a extradata model instance """
        try:
            res = await self.extradata_client.Update(
                lara_django_material_pb2.ExtraDataRequest(**extradata.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating extradata_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a extradata by extradata_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.extradata_client.Destroy(lara_django_material_pb2.ExtraDataDestroyRequest(extradata_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting extradata_id: {e}")

#_________________  PartDeviceClass  ______________________


class PartDeviceClassInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.partdeviceclass_client = lara_django_material_pb2_grpc.PartDeviceClassControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  partdeviceclasses:  list[material_dm.PartDeviceClass] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.PartDeviceClass]]:
        try:
            create_coroutines = ( self.partdeviceclass_client.Create(lara_django_material_pb2.PartDeviceClassRequest(**partdeviceclass.model_dump())) for partdeviceclass in partdeviceclasses )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.partdeviceclass_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating partdeviceclass: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        partdeviceclass_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.PartDeviceClass]:
        """ retrieve a list of partdeviceclass instances by partdeviceclass_id, name or filter_dict,
               returns a list of partdeviceclass data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  partdeviceclass_id is not None:
            try:
                res =  await self.partdeviceclass_client.Retrieve(
                    lara_django_material_pb2.PartDeviceClassRetrieveRequest(partdeviceclass_id=partdeviceclass_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.PartDeviceClass(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving partdeviceclass_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.partdeviceclass_client.List(
                    lara_django_material_pb2.PartDeviceClassListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.PartDeviceClass(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving partdeviceclass name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the partdeviceclass_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].partdeviceclass_id
        else:
            raise ValueError(f"Error retrieving partdeviceclass_id - no or too many results found.")
    
    async def get_by_id(self, partdeviceclass_id: str = None) -> material_dm.PartDeviceClass:
        """ retrieve a partdeviceclass data model by partdeviceclass_id """
        try:
            res = await self.partdeviceclass_client.Retrieve(
                lara_django_material_pb2.PartDeviceClassRetrieveRequest(partdeviceclass_id=partdeviceclass_id)
            )
            return material_dm.PartDeviceClass(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving partdeviceclass_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.PartDeviceClass | str:
        """ retrieve a partdeviceclass data model by name """
        try:
            res = await self.partdeviceclass_client.RetrieveByNameNamespace(
                lara_django_material_pb2.PartDeviceClassRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["partdeviceclass_id"]
            else:
                return material_dm.PartDeviceClass(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving partdeviceclass name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all partdeviceclasss """
        if filter_dict is None:
            res = await self.partdeviceclass_client.List(lara_django_material_pb2.PartDeviceClassListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.partdeviceclass_client.List(
                lara_django_material_pb2.PartDeviceClassListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.partdeviceclass_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all partdeviceclasss """
        if self.partdeviceclass_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.partdeviceclass_full_client.List(
                lara_django_material_pb2.PartDeviceClassFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.partdeviceclass_full_client.List(
                lara_django_material_pb2.PartDeviceClassFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, partdeviceclass : material_dm.PartDeviceClass = None) -> None:
        """ update a partdeviceclass model instance """
        try:
            res = await self.partdeviceclass_client.Update(
                lara_django_material_pb2.PartDeviceClassRequest(**partdeviceclass.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating partdeviceclass_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a partdeviceclass by partdeviceclass_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.partdeviceclass_client.Destroy(lara_django_material_pb2.PartDeviceClassDestroyRequest(partdeviceclass_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting partdeviceclass_id: {e}")

#_________________  PartShape  ______________________


class PartShapeInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.partshape_client = lara_django_material_pb2_grpc.PartShapeControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  partshapes:  list[material_dm.PartShape] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.PartShape]]:
        try:
            create_coroutines = ( self.partshape_client.Create(lara_django_material_pb2.PartShapeRequest(**partshape.model_dump())) for partshape in partshapes )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.partshape_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating partshape: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        partshape_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.PartShape]:
        """ retrieve a list of partshape instances by partshape_id, name or filter_dict,
               returns a list of partshape data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  partshape_id is not None:
            try:
                res =  await self.partshape_client.Retrieve(
                    lara_django_material_pb2.PartShapeRetrieveRequest(partshape_id=partshape_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.PartShape(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving partshape_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.partshape_client.List(
                    lara_django_material_pb2.PartShapeListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.PartShape(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving partshape name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the partshape_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].partshape_id
        else:
            raise ValueError(f"Error retrieving partshape_id - no or too many results found.")
    
    async def get_by_id(self, partshape_id: str = None) -> material_dm.PartShape:
        """ retrieve a partshape data model by partshape_id """
        try:
            res = await self.partshape_client.Retrieve(
                lara_django_material_pb2.PartShapeRetrieveRequest(partshape_id=partshape_id)
            )
            return material_dm.PartShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving partshape_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.PartShape | str:
        """ retrieve a partshape data model by name """
        try:
            res = await self.partshape_client.RetrieveByNameNamespace(
                lara_django_material_pb2.PartShapeRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["partshape_id"]
            else:
                return material_dm.PartShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving partshape name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all partshapes """
        if filter_dict is None:
            res = await self.partshape_client.List(lara_django_material_pb2.PartShapeListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.partshape_client.List(
                lara_django_material_pb2.PartShapeListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.partshape_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all partshapes """
        if self.partshape_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.partshape_full_client.List(
                lara_django_material_pb2.PartShapeFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.partshape_full_client.List(
                lara_django_material_pb2.PartShapeFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, partshape : material_dm.PartShape = None) -> None:
        """ update a partshape model instance """
        try:
            res = await self.partshape_client.Update(
                lara_django_material_pb2.PartShapeRequest(**partshape.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating partshape_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a partshape by partshape_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.partshape_client.Destroy(lara_django_material_pb2.PartShapeDestroyRequest(partshape_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting partshape_id: {e}")

#_________________  Part  ______________________


class PartInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.part_client = lara_django_material_pb2_grpc.PartControllerStub(channel)
        
        self.item_id = None
        self.update_item_id = "part_id"
        self.stub = self.part_client

        self.image_upload_chunk = lara_django_material_pb2.ImageUploadChunk

    
    @import_model_instance_from_file()
    @upload_image  # needed for image upload
    async def create(self,  parts:  list[material_dm.Part] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.Part]]:
        try:
            create_coroutines = ( self.part_client.Create(lara_django_material_pb2.PartRequest(**part.model_dump())) for part in parts )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.part_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating part: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        part_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.Part]:
        """ retrieve a list of part instances by part_id, name or filter_dict,
               returns a list of part data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  part_id is not None:
            try:
                res =  await self.part_client.Retrieve(
                    lara_django_material_pb2.PartRetrieveRequest(part_id=part_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.Part(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving part_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.part_client.List(
                    lara_django_material_pb2.PartListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.Part(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving part name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the part_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].part_id
        else:
            raise ValueError(f"Error retrieving part_id - no or too many results found.")
    
    @download_image  # needed for image download
    async def get_by_id(self, part_id: str = None) -> material_dm.Part:
        """ retrieve a part data model by part_id """
        try:
            res = await self.part_client.Retrieve(
                lara_django_material_pb2.PartRetrieveRequest(part_id=part_id)
            )
            return material_dm.Part(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving part_id: {e}")
    
    @download_image  # needed for image download
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.Part | str:
        """ retrieve a part data model by name """
        try:
            res = await self.part_client.RetrieveByNameNamespace(
                lara_django_material_pb2.PartRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["part_id"]
            else:
                return material_dm.Part(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving part name: {e}")
    
    
    
    @download_image  # needed for image download
    async def get_image(self, part_id : str = None, id_only : bool = True) -> bytes | None:
        """ retrieve an image by data_id """
        self.item_id = part_id
        
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all parts """
        if filter_dict is None:
            res = await self.part_client.List(lara_django_material_pb2.PartListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.part_client.List(
                lara_django_material_pb2.PartListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.part_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all parts """
        if self.part_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.part_full_client.List(
                lara_django_material_pb2.PartFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.part_full_client.List(
                lara_django_material_pb2.PartFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    @update_image  # needed for image update
    async def update(self, part : material_dm.Part = None) -> None:
        """ update a part model instance """
        try:
            res = await self.part_client.Update(
                lara_django_material_pb2.PartRequest(**part.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating part_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a part by part_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.part_client.Destroy(lara_django_material_pb2.PartDestroyRequest(part_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting part_id: {e}")

#_________________  PartsSubset  ______________________


class PartsSubsetInterface:
    def __init__(self, channel: grpc.Channel = None, 
                       default_namespace_uri : str = None,
                       default_namespace_id : str = None,
                       namespace_if = None ) -> None:
        self._default_namespace_uri = default_namespace_uri
        self._default_namespace_id = default_namespace_id

        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.namespace_if = namespace_if if namespace_if is not None else base_if.NamespaceInterface(channel)

        # self.partssubset_class_client = (
        #     lara_django_material_pb2_grpc.PartsSubsetclassByIRIControllerStub(channel)
        # )

        self.partssubset_client = lara_django_material_pb2_grpc.PartsSubsetControllerStub(channel)
        

        # self.partssubset_full_client = lara_django_material_pb2_grpc.PartsSubsetFullControllerStub(
        #     channel
        # )

    @property
    def default_namespace_uri(self):
        return self._default_namespace_uri

    @property
    def default_namespace_id(self):
        return self._default_namespace_id

    @classmethod
    async def build_if(cls, channel: grpc.Channel = None, default_namespace_uri: str = None):
    
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel
    
        namespace_if = base_if.NamespaceInterface(channel)
        try:
            default_namespace_id = await namespace_if.retrieve_id(namespace_uri=default_namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
        
        return cls(channel=channel, default_namespace_uri=default_namespace_uri,  
                   default_namespace_id=default_namespace_id, namespace_if=namespace_if)
    
    async def set_default_namespace_uri(self, namespace_uri: str = None):
        if namespace_uri is not None:
            self._default_namespace_uri = namespace_uri.strip()
        try:
            self._default_namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
    
    @import_model_instance_from_file()
    async def create(self,  partssubsets:  list[material_dm.PartsSubset] = None,  
                            ids_only: bool = False,
                            namespace_uri: str = None) -> Union[list[str], list[material_dm.PartsSubset]]:
        """ create a list of partssubset instances,
               returns a list of partssubset data model objects or ids_only

        :param partssubsets: list of partssubset data model objects
        :param ids_only: return only the ids of the created instances
        :return: list of partssubset data model objects or ids_only
        """
        partssubsetclass_id = None

        namespace_id = None
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
            namespace_id = self._default_namespace_id
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        if namespace_id is None:
            try:
                namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
            except Exception as e:
                logger.error(f"Error retrieving namespace_id: {e}")

        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if partssubset_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # try:
        #     res = await self.partssubset_class_client.Retrieve(
        #         lara_django_material_pb2.PartsSubsetclassRetrieveRequest(iri=partssubset_class_iri)
        #     )
        #     partssubsetclass_id = res.partssubsetclass_id
        # except Exception as e:
        #     logger.error(f"Error retrieving partssubsetclass_id: {e}")
        for partssubset in partssubsets:
            if partssubset.namespace == "" or partssubset.namespace == "default":
                    partssubset.namespace = namespace_id
            # partssubset.partssubset_class = partssubsetclass_id # uncomment if a model class is used

        try:
            create_coroutines = ( self.partssubset_client.Create(lara_django_material_pb2.PartsSubsetRequest(**partssubset.model_dump())) for partssubset in partssubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [ MessageToDict(item, preserving_proto_field_name=True).get("partssubset_id") for item in res ]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating partssubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        namespace_uri: str = None,
        partssubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.PartsSubset]:
        """ retrieve a list of partssubset instances by partssubset_id, name or filter_dict,
               returns a list of partssubset data model objects,
               if raw is True, the raw protobuf objects are returned.
               If namespace_uri is "default", the default namespace_uri is used.
        """
        if namespace_uri == "default":
            if self._default_namespace_uri is not None:
                namespace_uri = self._default_namespace_uri
            else:
                raise ValueError("No default namespace URI is given.")
        
        results_list = []
        if  partssubset_id is not None:
            try:
                res =  await self.partssubset_client.Retrieve(
                    lara_django_material_pb2.PartsSubsetRetrieveRequest(partssubset_id=partssubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.PartsSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving partssubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        if namespace_uri is not None:
            filter_dict["namespace__uri"] = namespace_uri
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.partssubset_client.List(
                    lara_django_material_pb2.PartsSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # :TODO: a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.PartsSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving partssubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          namespace_uri : str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the partssubset_id for a given name,
               name_display or iri
        """

        results_list = await self.retrieve(name=name, 
                        namespace_uri=namespace_uri, filter_dict=filter_dict, raw=True)

        if len(results_list) == 1 :
            return results_list[0].partssubset_id
        else:
            raise ValueError(f"Error retrieving partssubset_id - no or too many results found.")
    
    async def get_by_id(self, partssubset_id: str = None, id_only: bool = False) -> material_dm.PartsSubset:
        """ retrieve a partssubset data model by partssubset_id """
        try:
            res = await self.partssubset_client.Retrieve(
                lara_django_material_pb2.PartsSubsetRetrieveRequest(partssubset_id=partssubset_id)
            )
            item = material_dm.PartsSubset(**MessageToDict(res, preserving_proto_field_name=True))
            self.item_id = item.partssubset_id
            return item
        except Exception as e:
            logger.error(f"Error retrieving partssubset_id: {e}")
    
    async def get_by_name(self, name: str = None,
                                namespace_uri : str = None,
                                id_only: bool = False) -> material_dm.PartsSubset | str:
        """ retrieve a partssubset data model by name """
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        try:
            res = await self.partssubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.PartsSubsetRetrieveByNameNSRequest(name=name, namespace_uri=namespace_uri)
            )
            item = MessageToDict(res, preserving_proto_field_name=True)
            self.item_id = item["partssubset_id"]
            if id_only:
                return self.item_id
            else:
                return material_dm.PartsSubset(**item)
        except Exception as e:
            logger.error(f"Error retrieving partssubset name: {e}")
   
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all partssubsets """
        if filter_dict is None:
            res = await self.partssubset_client.List(lara_django_material_pb2.PartsSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.partssubset_client.List(
                lara_django_material_pb2.PartsSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.partssubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all partssubsets """
        if self.partssubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.partssubset_full_client.List(
                lara_django_material_pb2.PartsSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.partssubset_full_client.List(
                lara_django_material_pb2.PartsSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, partssubset : material_dm.PartsSubset = None) -> None:
        """ update a partssubset model instance """
        try:
            res = await self.partssubset_client.Update(
                lara_django_material_pb2.PartsSubsetRequest(**partssubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating partssubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a partssubset by partssubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.partssubset_client.Destroy(lara_django_material_pb2.PartsSubsetDestroyRequest(partssubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting partssubset_id: {e}")

#_________________  OrderedPartsSubset  ______________________


class OrderedPartsSubsetInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.orderedpartssubset_client = lara_django_material_pb2_grpc.OrderedPartsSubsetControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  orderedpartssubsets:  list[material_dm.OrderedPartsSubset] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.OrderedPartsSubset]]:
        try:
            create_coroutines = ( self.orderedpartssubset_client.Create(lara_django_material_pb2.OrderedPartsSubsetRequest(**orderedpartssubset.model_dump())) for orderedpartssubset in orderedpartssubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.orderedpartssubset_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating orderedpartssubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        orderedpartssubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.OrderedPartsSubset]:
        """ retrieve a list of orderedpartssubset instances by orderedpartssubset_id, name or filter_dict,
               returns a list of orderedpartssubset data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  orderedpartssubset_id is not None:
            try:
                res =  await self.orderedpartssubset_client.Retrieve(
                    lara_django_material_pb2.OrderedPartsSubsetRetrieveRequest(orderedpartssubset_id=orderedpartssubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.OrderedPartsSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving orderedpartssubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.orderedpartssubset_client.List(
                    lara_django_material_pb2.OrderedPartsSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.OrderedPartsSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving orderedpartssubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the orderedpartssubset_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].orderedpartssubset_id
        else:
            raise ValueError(f"Error retrieving orderedpartssubset_id - no or too many results found.")
    
    async def get_by_id(self, orderedpartssubset_id: str = None) -> material_dm.OrderedPartsSubset:
        """ retrieve a orderedpartssubset data model by orderedpartssubset_id """
        try:
            res = await self.orderedpartssubset_client.Retrieve(
                lara_django_material_pb2.OrderedPartsSubsetRetrieveRequest(orderedpartssubset_id=orderedpartssubset_id)
            )
            return material_dm.OrderedPartsSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving orderedpartssubset_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.OrderedPartsSubset | str:
        """ retrieve a orderedpartssubset data model by name """
        try:
            res = await self.orderedpartssubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.OrderedPartsSubsetRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["orderedpartssubset_id"]
            else:
                return material_dm.OrderedPartsSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving orderedpartssubset name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all orderedpartssubsets """
        if filter_dict is None:
            res = await self.orderedpartssubset_client.List(lara_django_material_pb2.OrderedPartsSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.orderedpartssubset_client.List(
                lara_django_material_pb2.OrderedPartsSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.orderedpartssubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all orderedpartssubsets """
        if self.orderedpartssubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.orderedpartssubset_full_client.List(
                lara_django_material_pb2.OrderedPartsSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.orderedpartssubset_full_client.List(
                lara_django_material_pb2.OrderedPartsSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, orderedpartssubset : material_dm.OrderedPartsSubset = None) -> None:
        """ update a orderedpartssubset model instance """
        try:
            res = await self.orderedpartssubset_client.Update(
                lara_django_material_pb2.OrderedPartsSubsetRequest(**orderedpartssubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating orderedpartssubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a orderedpartssubset by orderedpartssubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.orderedpartssubset_client.Destroy(lara_django_material_pb2.OrderedPartsSubsetDestroyRequest(orderedpartssubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting orderedpartssubset_id: {e}")

#_________________  DeviceShape  ______________________


class DeviceShapeInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.deviceshape_client = lara_django_material_pb2_grpc.DeviceShapeControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  deviceshapes:  list[material_dm.DeviceShape] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.DeviceShape]]:
        try:
            create_coroutines = ( self.deviceshape_client.Create(lara_django_material_pb2.DeviceShapeRequest(**deviceshape.model_dump())) for deviceshape in deviceshapes )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.deviceshape_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating deviceshape: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        deviceshape_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.DeviceShape]:
        """ retrieve a list of deviceshape instances by deviceshape_id, name or filter_dict,
               returns a list of deviceshape data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  deviceshape_id is not None:
            try:
                res =  await self.deviceshape_client.Retrieve(
                    lara_django_material_pb2.DeviceShapeRetrieveRequest(deviceshape_id=deviceshape_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.DeviceShape(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving deviceshape_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.deviceshape_client.List(
                    lara_django_material_pb2.DeviceShapeListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.DeviceShape(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving deviceshape name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the deviceshape_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].deviceshape_id
        else:
            raise ValueError(f"Error retrieving deviceshape_id - no or too many results found.")
    
    async def get_by_id(self, deviceshape_id: str = None) -> material_dm.DeviceShape:
        """ retrieve a deviceshape data model by deviceshape_id """
        try:
            res = await self.deviceshape_client.Retrieve(
                lara_django_material_pb2.DeviceShapeRetrieveRequest(deviceshape_id=deviceshape_id)
            )
            return material_dm.DeviceShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving deviceshape_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.DeviceShape | str:
        """ retrieve a deviceshape data model by name """
        try:
            res = await self.deviceshape_client.RetrieveByNameNamespace(
                lara_django_material_pb2.DeviceShapeRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["deviceshape_id"]
            else:
                return material_dm.DeviceShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving deviceshape name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all deviceshapes """
        if filter_dict is None:
            res = await self.deviceshape_client.List(lara_django_material_pb2.DeviceShapeListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.deviceshape_client.List(
                lara_django_material_pb2.DeviceShapeListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.deviceshape_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all deviceshapes """
        if self.deviceshape_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.deviceshape_full_client.List(
                lara_django_material_pb2.DeviceShapeFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.deviceshape_full_client.List(
                lara_django_material_pb2.DeviceShapeFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, deviceshape : material_dm.DeviceShape = None) -> None:
        """ update a deviceshape model instance """
        try:
            res = await self.deviceshape_client.Update(
                lara_django_material_pb2.DeviceShapeRequest(**deviceshape.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating deviceshape_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a deviceshape by deviceshape_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.deviceshape_client.Destroy(lara_django_material_pb2.DeviceShapeDestroyRequest(deviceshape_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting deviceshape_id: {e}")

#_________________  Device  ______________________


class DeviceInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.device_client = lara_django_material_pb2_grpc.DeviceControllerStub(channel)
        
        self.item_id = None
        self.update_item_id = "device_id"
        self.stub = self.device_client

        self.image_upload_chunk = lara_django_material_pb2.ImageUploadChunk

    
    @import_model_instance_from_file()
    @upload_image  # needed for image upload
    async def create(self,  devices:  list[material_dm.Device] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.Device]]:
        try:
            create_coroutines = ( self.device_client.Create(lara_django_material_pb2.DeviceRequest(**device.model_dump())) for device in devices )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.device_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating device: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        device_id: str = None,
        name: str = None,
        name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.Device]:
        """ retrieve a list of device instances by device_id, name or filter_dict,
               returns a list of device data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  device_id is not None:
            try:
                res =  await self.device_client.Retrieve(
                    lara_django_material_pb2.DeviceRetrieveRequest(device_id=device_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.Device(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving device_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        if name_display is not None:
           filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.device_client.List(
                    lara_django_material_pb2.DeviceListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.Device(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving device name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the device_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, name_display=name_display, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].device_id
        else:
            raise ValueError(f"Error retrieving device_id - no or too many results found.")
    
    @download_image  # needed for image download
    async def get_by_id(self, device_id: str = None) -> material_dm.Device:
        """ retrieve a device data model by device_id """
        try:
            res = await self.device_client.Retrieve(
                lara_django_material_pb2.DeviceRetrieveRequest(device_id=device_id)
            )
            return material_dm.Device(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving device_id: {e}")
    
    @download_image  # needed for image download
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.Device | str:
        """ retrieve a device data model by name """
        try:
            res = await self.device_client.RetrieveByNameNamespace(
                lara_django_material_pb2.DeviceRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["device_id"]
            else:
                return material_dm.Device(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving device name: {e}")
    
    
    
    @download_image  # needed for image download
    async def get_image(self, device_id : str = None, id_only : bool = True) -> bytes | None:
        """ retrieve an image by data_id """
        self.item_id = device_id
        
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all devices """
        if filter_dict is None:
            res = await self.device_client.List(lara_django_material_pb2.DeviceListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.device_client.List(
                lara_django_material_pb2.DeviceListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.device_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all devices """
        if self.device_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.device_full_client.List(
                lara_django_material_pb2.DeviceFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.device_full_client.List(
                lara_django_material_pb2.DeviceFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    @update_image  # needed for image update
    async def update(self, device : material_dm.Device = None) -> None:
        """ update a device model instance """
        try:
            res = await self.device_client.Update(
                lara_django_material_pb2.DeviceRequest(**device.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating device_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a device by device_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.device_client.Destroy(lara_django_material_pb2.DeviceDestroyRequest(device_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting device_id: {e}")

#_________________  DevicesSubset  ______________________


class DevicesSubsetInterface:
    def __init__(self, channel: grpc.Channel = None, 
                       default_namespace_uri : str = None,
                       default_namespace_id : str = None,
                       namespace_if = None ) -> None:
        self._default_namespace_uri = default_namespace_uri
        self._default_namespace_id = default_namespace_id

        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.namespace_if = namespace_if if namespace_if is not None else base_if.NamespaceInterface(channel)

        # self.devicessubset_class_client = (
        #     lara_django_material_pb2_grpc.DevicesSubsetclassByIRIControllerStub(channel)
        # )

        self.devicessubset_client = lara_django_material_pb2_grpc.DevicesSubsetControllerStub(channel)
        

        # self.devicessubset_full_client = lara_django_material_pb2_grpc.DevicesSubsetFullControllerStub(
        #     channel
        # )

    @property
    def default_namespace_uri(self):
        return self._default_namespace_uri

    @property
    def default_namespace_id(self):
        return self._default_namespace_id

    @classmethod
    async def build_if(cls, channel: grpc.Channel = None, default_namespace_uri: str = None):
    
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel
    
        namespace_if = base_if.NamespaceInterface(channel)
        try:
            default_namespace_id = await namespace_if.retrieve_id(namespace_uri=default_namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
        
        return cls(channel=channel, default_namespace_uri=default_namespace_uri,  
                   default_namespace_id=default_namespace_id, namespace_if=namespace_if)
    
    async def set_default_namespace_uri(self, namespace_uri: str = None):
        if namespace_uri is not None:
            self._default_namespace_uri = namespace_uri.strip()
        try:
            self._default_namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
    
    @import_model_instance_from_file()
    async def create(self,  devicessubsets:  list[material_dm.DevicesSubset] = None,  
                            ids_only: bool = False,
                            namespace_uri: str = None) -> Union[list[str], list[material_dm.DevicesSubset]]:
        """ create a list of devicessubset instances,
               returns a list of devicessubset data model objects or ids_only

        :param devicessubsets: list of devicessubset data model objects
        :param ids_only: return only the ids of the created instances
        :return: list of devicessubset data model objects or ids_only
        """
        devicessubsetclass_id = None

        namespace_id = None
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
            namespace_id = self._default_namespace_id
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        if namespace_id is None:
            try:
                namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
            except Exception as e:
                logger.error(f"Error retrieving namespace_id: {e}")

        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if devicessubset_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # try:
        #     res = await self.devicessubset_class_client.Retrieve(
        #         lara_django_material_pb2.DevicesSubsetclassRetrieveRequest(iri=devicessubset_class_iri)
        #     )
        #     devicessubsetclass_id = res.devicessubsetclass_id
        # except Exception as e:
        #     logger.error(f"Error retrieving devicessubsetclass_id: {e}")
        for devicessubset in devicessubsets:
            if devicessubset.namespace == "" or devicessubset.namespace == "default":
                    devicessubset.namespace = namespace_id
            # devicessubset.devicessubset_class = devicessubsetclass_id # uncomment if a model class is used

        try:
            create_coroutines = ( self.devicessubset_client.Create(lara_django_material_pb2.DevicesSubsetRequest(**devicessubset.model_dump())) for devicessubset in devicessubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [ MessageToDict(item, preserving_proto_field_name=True).get("devicessubset_id") for item in res ]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating devicessubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        namespace_uri: str = None,
        devicessubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.DevicesSubset]:
        """ retrieve a list of devicessubset instances by devicessubset_id, name or filter_dict,
               returns a list of devicessubset data model objects,
               if raw is True, the raw protobuf objects are returned.
               If namespace_uri is "default", the default namespace_uri is used.
        """
        if namespace_uri == "default":
            if self._default_namespace_uri is not None:
                namespace_uri = self._default_namespace_uri
            else:
                raise ValueError("No default namespace URI is given.")
        
        results_list = []
        if  devicessubset_id is not None:
            try:
                res =  await self.devicessubset_client.Retrieve(
                    lara_django_material_pb2.DevicesSubsetRetrieveRequest(devicessubset_id=devicessubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.DevicesSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving devicessubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        if namespace_uri is not None:
            filter_dict["namespace__uri"] = namespace_uri
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.devicessubset_client.List(
                    lara_django_material_pb2.DevicesSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # :TODO: a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.DevicesSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving devicessubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          namespace_uri : str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the devicessubset_id for a given name,
               name_display or iri
        """

        results_list = await self.retrieve(name=name, 
                        namespace_uri=namespace_uri, filter_dict=filter_dict, raw=True)

        if len(results_list) == 1 :
            return results_list[0].devicessubset_id
        else:
            raise ValueError(f"Error retrieving devicessubset_id - no or too many results found.")
    
    async def get_by_id(self, devicessubset_id: str = None, id_only: bool = False) -> material_dm.DevicesSubset:
        """ retrieve a devicessubset data model by devicessubset_id """
        try:
            res = await self.devicessubset_client.Retrieve(
                lara_django_material_pb2.DevicesSubsetRetrieveRequest(devicessubset_id=devicessubset_id)
            )
            item = material_dm.DevicesSubset(**MessageToDict(res, preserving_proto_field_name=True))
            self.item_id = item.devicessubset_id
            return item
        except Exception as e:
            logger.error(f"Error retrieving devicessubset_id: {e}")
    
    async def get_by_name(self, name: str = None,
                                namespace_uri : str = None,
                                id_only: bool = False) -> material_dm.DevicesSubset | str:
        """ retrieve a devicessubset data model by name """
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        try:
            res = await self.devicessubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.DevicesSubsetRetrieveByNameNSRequest(name=name, namespace_uri=namespace_uri)
            )
            item = MessageToDict(res, preserving_proto_field_name=True)
            self.item_id = item["devicessubset_id"]
            if id_only:
                return self.item_id
            else:
                return material_dm.DevicesSubset(**item)
        except Exception as e:
            logger.error(f"Error retrieving devicessubset name: {e}")
   
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all devicessubsets """
        if filter_dict is None:
            res = await self.devicessubset_client.List(lara_django_material_pb2.DevicesSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.devicessubset_client.List(
                lara_django_material_pb2.DevicesSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.devicessubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all devicessubsets """
        if self.devicessubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.devicessubset_full_client.List(
                lara_django_material_pb2.DevicesSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.devicessubset_full_client.List(
                lara_django_material_pb2.DevicesSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, devicessubset : material_dm.DevicesSubset = None) -> None:
        """ update a devicessubset model instance """
        try:
            res = await self.devicessubset_client.Update(
                lara_django_material_pb2.DevicesSubsetRequest(**devicessubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating devicessubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a devicessubset by devicessubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.devicessubset_client.Destroy(lara_django_material_pb2.DevicesSubsetDestroyRequest(devicessubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting devicessubset_id: {e}")

#_________________  OrderedDevicesSubset  ______________________


class OrderedDevicesSubsetInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.ordereddevicessubset_client = lara_django_material_pb2_grpc.OrderedDevicesSubsetControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  ordereddevicessubsets:  list[material_dm.OrderedDevicesSubset] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.OrderedDevicesSubset]]:
        try:
            create_coroutines = ( self.ordereddevicessubset_client.Create(lara_django_material_pb2.OrderedDevicesSubsetRequest(**ordereddevicessubset.model_dump())) for ordereddevicessubset in ordereddevicessubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.ordereddevicessubset_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating ordereddevicessubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        ordereddevicessubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.OrderedDevicesSubset]:
        """ retrieve a list of ordereddevicessubset instances by ordereddevicessubset_id, name or filter_dict,
               returns a list of ordereddevicessubset data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  ordereddevicessubset_id is not None:
            try:
                res =  await self.ordereddevicessubset_client.Retrieve(
                    lara_django_material_pb2.OrderedDevicesSubsetRetrieveRequest(ordereddevicessubset_id=ordereddevicessubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.OrderedDevicesSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving ordereddevicessubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.ordereddevicessubset_client.List(
                    lara_django_material_pb2.OrderedDevicesSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.OrderedDevicesSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving ordereddevicessubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the ordereddevicessubset_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].ordereddevicessubset_id
        else:
            raise ValueError(f"Error retrieving ordereddevicessubset_id - no or too many results found.")
    
    async def get_by_id(self, ordereddevicessubset_id: str = None) -> material_dm.OrderedDevicesSubset:
        """ retrieve a ordereddevicessubset data model by ordereddevicessubset_id """
        try:
            res = await self.ordereddevicessubset_client.Retrieve(
                lara_django_material_pb2.OrderedDevicesSubsetRetrieveRequest(ordereddevicessubset_id=ordereddevicessubset_id)
            )
            return material_dm.OrderedDevicesSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving ordereddevicessubset_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.OrderedDevicesSubset | str:
        """ retrieve a ordereddevicessubset data model by name """
        try:
            res = await self.ordereddevicessubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.OrderedDevicesSubsetRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["ordereddevicessubset_id"]
            else:
                return material_dm.OrderedDevicesSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving ordereddevicessubset name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all ordereddevicessubsets """
        if filter_dict is None:
            res = await self.ordereddevicessubset_client.List(lara_django_material_pb2.OrderedDevicesSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.ordereddevicessubset_client.List(
                lara_django_material_pb2.OrderedDevicesSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.ordereddevicessubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all ordereddevicessubsets """
        if self.ordereddevicessubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.ordereddevicessubset_full_client.List(
                lara_django_material_pb2.OrderedDevicesSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.ordereddevicessubset_full_client.List(
                lara_django_material_pb2.OrderedDevicesSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, ordereddevicessubset : material_dm.OrderedDevicesSubset = None) -> None:
        """ update a ordereddevicessubset model instance """
        try:
            res = await self.ordereddevicessubset_client.Update(
                lara_django_material_pb2.OrderedDevicesSubsetRequest(**ordereddevicessubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating ordereddevicessubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a ordereddevicessubset by ordereddevicessubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.ordereddevicessubset_client.Destroy(lara_django_material_pb2.OrderedDevicesSubsetDestroyRequest(ordereddevicessubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting ordereddevicessubset_id: {e}")

#_________________  DeviceSetupShape  ______________________


class DeviceSetupShapeInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.devicesetupshape_client = lara_django_material_pb2_grpc.DeviceSetupShapeControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  devicesetupshapes:  list[material_dm.DeviceSetupShape] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.DeviceSetupShape]]:
        try:
            create_coroutines = ( self.devicesetupshape_client.Create(lara_django_material_pb2.DeviceSetupShapeRequest(**devicesetupshape.model_dump())) for devicesetupshape in devicesetupshapes )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.devicesetupshape_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating devicesetupshape: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        devicesetupshape_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.DeviceSetupShape]:
        """ retrieve a list of devicesetupshape instances by devicesetupshape_id, name or filter_dict,
               returns a list of devicesetupshape data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  devicesetupshape_id is not None:
            try:
                res =  await self.devicesetupshape_client.Retrieve(
                    lara_django_material_pb2.DeviceSetupShapeRetrieveRequest(devicesetupshape_id=devicesetupshape_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.DeviceSetupShape(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving devicesetupshape_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.devicesetupshape_client.List(
                    lara_django_material_pb2.DeviceSetupShapeListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.DeviceSetupShape(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving devicesetupshape name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the devicesetupshape_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].devicesetupshape_id
        else:
            raise ValueError(f"Error retrieving devicesetupshape_id - no or too many results found.")
    
    async def get_by_id(self, devicesetupshape_id: str = None) -> material_dm.DeviceSetupShape:
        """ retrieve a devicesetupshape data model by devicesetupshape_id """
        try:
            res = await self.devicesetupshape_client.Retrieve(
                lara_django_material_pb2.DeviceSetupShapeRetrieveRequest(devicesetupshape_id=devicesetupshape_id)
            )
            return material_dm.DeviceSetupShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving devicesetupshape_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.DeviceSetupShape | str:
        """ retrieve a devicesetupshape data model by name """
        try:
            res = await self.devicesetupshape_client.RetrieveByNameNamespace(
                lara_django_material_pb2.DeviceSetupShapeRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["devicesetupshape_id"]
            else:
                return material_dm.DeviceSetupShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving devicesetupshape name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all devicesetupshapes """
        if filter_dict is None:
            res = await self.devicesetupshape_client.List(lara_django_material_pb2.DeviceSetupShapeListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.devicesetupshape_client.List(
                lara_django_material_pb2.DeviceSetupShapeListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.devicesetupshape_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all devicesetupshapes """
        if self.devicesetupshape_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.devicesetupshape_full_client.List(
                lara_django_material_pb2.DeviceSetupShapeFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.devicesetupshape_full_client.List(
                lara_django_material_pb2.DeviceSetupShapeFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, devicesetupshape : material_dm.DeviceSetupShape = None) -> None:
        """ update a devicesetupshape model instance """
        try:
            res = await self.devicesetupshape_client.Update(
                lara_django_material_pb2.DeviceSetupShapeRequest(**devicesetupshape.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating devicesetupshape_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a devicesetupshape by devicesetupshape_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.devicesetupshape_client.Destroy(lara_django_material_pb2.DeviceSetupShapeDestroyRequest(devicesetupshape_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting devicesetupshape_id: {e}")

#_________________  DeviceSetup  ______________________


class DeviceSetupInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.devicesetup_client = lara_django_material_pb2_grpc.DeviceSetupControllerStub(channel)
        
        self.item_id = None
        self.update_item_id = "devicesetup_id"
        self.stub = self.devicesetup_client

        self.file_download_info = lara_django_material_pb2.FileDownloadInfo
        self.file_upload_chunk = lara_django_material_pb2.FileUploadChunk

        self.image_upload_chunk = lara_django_material_pb2.ImageUploadChunk

    
    @import_model_instance_from_file()
    @upload_file  # needed for file upload
    @upload_image  # needed for image upload
    async def create(self,  devicesetups:  list[material_dm.DeviceSetup] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.DeviceSetup]]:
        try:
            create_coroutines = ( self.devicesetup_client.Create(lara_django_material_pb2.DeviceSetupRequest(**devicesetup.model_dump())) for devicesetup in devicesetups )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.devicesetup_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating devicesetup: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        devicesetup_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.DeviceSetup]:
        """ retrieve a list of devicesetup instances by devicesetup_id, name or filter_dict,
               returns a list of devicesetup data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  devicesetup_id is not None:
            try:
                res =  await self.devicesetup_client.Retrieve(
                    lara_django_material_pb2.DeviceSetupRetrieveRequest(devicesetup_id=devicesetup_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.DeviceSetup(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving devicesetup_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.devicesetup_client.List(
                    lara_django_material_pb2.DeviceSetupListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.DeviceSetup(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving devicesetup name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the devicesetup_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].devicesetup_id
        else:
            raise ValueError(f"Error retrieving devicesetup_id - no or too many results found.")
    
    @download_file  # needed for file download
    @download_image  # needed for image download
    async def get_by_id(self, devicesetup_id: str = None) -> material_dm.DeviceSetup:
        """ retrieve a devicesetup data model by devicesetup_id """
        try:
            res = await self.devicesetup_client.Retrieve(
                lara_django_material_pb2.DeviceSetupRetrieveRequest(devicesetup_id=devicesetup_id)
            )
            return material_dm.DeviceSetup(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving devicesetup_id: {e}")
    
    @download_file  # needed for file download
    @download_image  # needed for image download
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.DeviceSetup | str:
        """ retrieve a devicesetup data model by name """
        try:
            res = await self.devicesetup_client.RetrieveByNameNamespace(
                lara_django_material_pb2.DeviceSetupRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["devicesetup_id"]
            else:
                return material_dm.DeviceSetup(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving devicesetup name: {e}")
    
    @download_file  # needed for file download
    async def get_file(self, devicesetup_id : str = None, id_only : bool = True ) -> bytes | None:
        """ retrieve a file by data_id """
        self.item_id = devicesetup_id
        
    
    async def get_file_by_name(self, name: str = None,
                                filename_target : str = None,
                                as_byte_str : bool = None ) -> bytes | None:
        """ retrieve a file by name """
        try:
            devicesetup_id = await self.get_by_name(name=name, id_only=True)
            return await self.get_file(devicesetup_id=devicesetup_id, filename_target=filename_target, as_byte_str=as_byte_str,  get_file=True)
        except Exception as e:
            logger.error(f"Error retrieving devicesetup name: {e}")
        
    
    @download_image  # needed for image download
    async def get_image(self, devicesetup_id : str = None, id_only : bool = True) -> bytes | None:
        """ retrieve an image by data_id """
        self.item_id = devicesetup_id
        
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all devicesetups """
        if filter_dict is None:
            res = await self.devicesetup_client.List(lara_django_material_pb2.DeviceSetupListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.devicesetup_client.List(
                lara_django_material_pb2.DeviceSetupListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.devicesetup_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all devicesetups """
        if self.devicesetup_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.devicesetup_full_client.List(
                lara_django_material_pb2.DeviceSetupFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.devicesetup_full_client.List(
                lara_django_material_pb2.DeviceSetupFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    @update_file  # needed for file update
    @update_image  # needed for image update
    async def update(self, devicesetup : material_dm.DeviceSetup = None) -> None:
        """ update a devicesetup model instance """
        try:
            res = await self.devicesetup_client.Update(
                lara_django_material_pb2.DeviceSetupRequest(**devicesetup.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating devicesetup_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a devicesetup by devicesetup_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.devicesetup_client.Destroy(lara_django_material_pb2.DeviceSetupDestroyRequest(devicesetup_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting devicesetup_id: {e}")

#_________________  DeviceSetupsSubset  ______________________


class DeviceSetupsSubsetInterface:
    def __init__(self, channel: grpc.Channel = None, 
                       default_namespace_uri : str = None,
                       default_namespace_id : str = None,
                       namespace_if = None ) -> None:
        self._default_namespace_uri = default_namespace_uri
        self._default_namespace_id = default_namespace_id

        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.namespace_if = namespace_if if namespace_if is not None else base_if.NamespaceInterface(channel)

        # self.devicesetupssubset_class_client = (
        #     lara_django_material_pb2_grpc.DeviceSetupsSubsetclassByIRIControllerStub(channel)
        # )

        self.devicesetupssubset_client = lara_django_material_pb2_grpc.DeviceSetupsSubsetControllerStub(channel)
        

        # self.devicesetupssubset_full_client = lara_django_material_pb2_grpc.DeviceSetupsSubsetFullControllerStub(
        #     channel
        # )

    @property
    def default_namespace_uri(self):
        return self._default_namespace_uri

    @property
    def default_namespace_id(self):
        return self._default_namespace_id

    @classmethod
    async def build_if(cls, channel: grpc.Channel = None, default_namespace_uri: str = None):
    
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel
    
        namespace_if = base_if.NamespaceInterface(channel)
        try:
            default_namespace_id = await namespace_if.retrieve_id(namespace_uri=default_namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
        
        return cls(channel=channel, default_namespace_uri=default_namespace_uri,  
                   default_namespace_id=default_namespace_id, namespace_if=namespace_if)
    
    async def set_default_namespace_uri(self, namespace_uri: str = None):
        if namespace_uri is not None:
            self._default_namespace_uri = namespace_uri.strip()
        try:
            self._default_namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
    
    @import_model_instance_from_file()
    async def create(self,  devicesetupssubsets:  list[material_dm.DeviceSetupsSubset] = None,  
                            ids_only: bool = False,
                            namespace_uri: str = None) -> Union[list[str], list[material_dm.DeviceSetupsSubset]]:
        """ create a list of devicesetupssubset instances,
               returns a list of devicesetupssubset data model objects or ids_only

        :param devicesetupssubsets: list of devicesetupssubset data model objects
        :param ids_only: return only the ids of the created instances
        :return: list of devicesetupssubset data model objects or ids_only
        """
        devicesetupssubsetclass_id = None

        namespace_id = None
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
            namespace_id = self._default_namespace_id
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        if namespace_id is None:
            try:
                namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
            except Exception as e:
                logger.error(f"Error retrieving namespace_id: {e}")

        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if devicesetupssubset_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # try:
        #     res = await self.devicesetupssubset_class_client.Retrieve(
        #         lara_django_material_pb2.DeviceSetupsSubsetclassRetrieveRequest(iri=devicesetupssubset_class_iri)
        #     )
        #     devicesetupssubsetclass_id = res.devicesetupssubsetclass_id
        # except Exception as e:
        #     logger.error(f"Error retrieving devicesetupssubsetclass_id: {e}")
        for devicesetupssubset in devicesetupssubsets:
            if devicesetupssubset.namespace == "" or devicesetupssubset.namespace == "default":
                    devicesetupssubset.namespace = namespace_id
            # devicesetupssubset.devicesetupssubset_class = devicesetupssubsetclass_id # uncomment if a model class is used

        try:
            create_coroutines = ( self.devicesetupssubset_client.Create(lara_django_material_pb2.DeviceSetupsSubsetRequest(**devicesetupssubset.model_dump())) for devicesetupssubset in devicesetupssubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [ MessageToDict(item, preserving_proto_field_name=True).get("devicesetupssubset_id") for item in res ]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating devicesetupssubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        namespace_uri: str = None,
        devicesetupssubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.DeviceSetupsSubset]:
        """ retrieve a list of devicesetupssubset instances by devicesetupssubset_id, name or filter_dict,
               returns a list of devicesetupssubset data model objects,
               if raw is True, the raw protobuf objects are returned.
               If namespace_uri is "default", the default namespace_uri is used.
        """
        if namespace_uri == "default":
            if self._default_namespace_uri is not None:
                namespace_uri = self._default_namespace_uri
            else:
                raise ValueError("No default namespace URI is given.")
        
        results_list = []
        if  devicesetupssubset_id is not None:
            try:
                res =  await self.devicesetupssubset_client.Retrieve(
                    lara_django_material_pb2.DeviceSetupsSubsetRetrieveRequest(devicesetupssubset_id=devicesetupssubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.DeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving devicesetupssubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        if namespace_uri is not None:
            filter_dict["namespace__uri"] = namespace_uri
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.devicesetupssubset_client.List(
                    lara_django_material_pb2.DeviceSetupsSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # :TODO: a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.DeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving devicesetupssubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          namespace_uri : str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the devicesetupssubset_id for a given name,
               name_display or iri
        """

        results_list = await self.retrieve(name=name, 
                        namespace_uri=namespace_uri, filter_dict=filter_dict, raw=True)

        if len(results_list) == 1 :
            return results_list[0].devicesetupssubset_id
        else:
            raise ValueError(f"Error retrieving devicesetupssubset_id - no or too many results found.")
    
    async def get_by_id(self, devicesetupssubset_id: str = None, id_only: bool = False) -> material_dm.DeviceSetupsSubset:
        """ retrieve a devicesetupssubset data model by devicesetupssubset_id """
        try:
            res = await self.devicesetupssubset_client.Retrieve(
                lara_django_material_pb2.DeviceSetupsSubsetRetrieveRequest(devicesetupssubset_id=devicesetupssubset_id)
            )
            item = material_dm.DeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True))
            self.item_id = item.devicesetupssubset_id
            return item
        except Exception as e:
            logger.error(f"Error retrieving devicesetupssubset_id: {e}")
    
    async def get_by_name(self, name: str = None,
                                namespace_uri : str = None,
                                id_only: bool = False) -> material_dm.DeviceSetupsSubset | str:
        """ retrieve a devicesetupssubset data model by name """
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        try:
            res = await self.devicesetupssubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.DeviceSetupsSubsetRetrieveByNameNSRequest(name=name, namespace_uri=namespace_uri)
            )
            item = MessageToDict(res, preserving_proto_field_name=True)
            self.item_id = item["devicesetupssubset_id"]
            if id_only:
                return self.item_id
            else:
                return material_dm.DeviceSetupsSubset(**item)
        except Exception as e:
            logger.error(f"Error retrieving devicesetupssubset name: {e}")
   
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all devicesetupssubsets """
        if filter_dict is None:
            res = await self.devicesetupssubset_client.List(lara_django_material_pb2.DeviceSetupsSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.devicesetupssubset_client.List(
                lara_django_material_pb2.DeviceSetupsSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.devicesetupssubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all devicesetupssubsets """
        if self.devicesetupssubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.devicesetupssubset_full_client.List(
                lara_django_material_pb2.DeviceSetupsSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.devicesetupssubset_full_client.List(
                lara_django_material_pb2.DeviceSetupsSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, devicesetupssubset : material_dm.DeviceSetupsSubset = None) -> None:
        """ update a devicesetupssubset model instance """
        try:
            res = await self.devicesetupssubset_client.Update(
                lara_django_material_pb2.DeviceSetupsSubsetRequest(**devicesetupssubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating devicesetupssubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a devicesetupssubset by devicesetupssubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.devicesetupssubset_client.Destroy(lara_django_material_pb2.DeviceSetupsSubsetDestroyRequest(devicesetupssubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting devicesetupssubset_id: {e}")

#_________________  OrderedDeviceSetupsSubset  ______________________


class OrderedDeviceSetupsSubsetInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.ordereddevicesetupssubset_client = lara_django_material_pb2_grpc.OrderedDeviceSetupsSubsetControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  ordereddevicesetupssubsets:  list[material_dm.OrderedDeviceSetupsSubset] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.OrderedDeviceSetupsSubset]]:
        try:
            create_coroutines = ( self.ordereddevicesetupssubset_client.Create(lara_django_material_pb2.OrderedDeviceSetupsSubsetRequest(**ordereddevicesetupssubset.model_dump())) for ordereddevicesetupssubset in ordereddevicesetupssubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.ordereddevicesetupssubset_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating ordereddevicesetupssubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        ordereddevicesetupssubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.OrderedDeviceSetupsSubset]:
        """ retrieve a list of ordereddevicesetupssubset instances by ordereddevicesetupssubset_id, name or filter_dict,
               returns a list of ordereddevicesetupssubset data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  ordereddevicesetupssubset_id is not None:
            try:
                res =  await self.ordereddevicesetupssubset_client.Retrieve(
                    lara_django_material_pb2.OrderedDeviceSetupsSubsetRetrieveRequest(ordereddevicesetupssubset_id=ordereddevicesetupssubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.OrderedDeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving ordereddevicesetupssubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.ordereddevicesetupssubset_client.List(
                    lara_django_material_pb2.OrderedDeviceSetupsSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.OrderedDeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving ordereddevicesetupssubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the ordereddevicesetupssubset_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].ordereddevicesetupssubset_id
        else:
            raise ValueError(f"Error retrieving ordereddevicesetupssubset_id - no or too many results found.")
    
    async def get_by_id(self, ordereddevicesetupssubset_id: str = None) -> material_dm.OrderedDeviceSetupsSubset:
        """ retrieve a ordereddevicesetupssubset data model by ordereddevicesetupssubset_id """
        try:
            res = await self.ordereddevicesetupssubset_client.Retrieve(
                lara_django_material_pb2.OrderedDeviceSetupsSubsetRetrieveRequest(ordereddevicesetupssubset_id=ordereddevicesetupssubset_id)
            )
            return material_dm.OrderedDeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving ordereddevicesetupssubset_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.OrderedDeviceSetupsSubset | str:
        """ retrieve a ordereddevicesetupssubset data model by name """
        try:
            res = await self.ordereddevicesetupssubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.OrderedDeviceSetupsSubsetRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["ordereddevicesetupssubset_id"]
            else:
                return material_dm.OrderedDeviceSetupsSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving ordereddevicesetupssubset name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all ordereddevicesetupssubsets """
        if filter_dict is None:
            res = await self.ordereddevicesetupssubset_client.List(lara_django_material_pb2.OrderedDeviceSetupsSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.ordereddevicesetupssubset_client.List(
                lara_django_material_pb2.OrderedDeviceSetupsSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.ordereddevicesetupssubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all ordereddevicesetupssubsets """
        if self.ordereddevicesetupssubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.ordereddevicesetupssubset_full_client.List(
                lara_django_material_pb2.OrderedDeviceSetupsSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.ordereddevicesetupssubset_full_client.List(
                lara_django_material_pb2.OrderedDeviceSetupsSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, ordereddevicesetupssubset : material_dm.OrderedDeviceSetupsSubset = None) -> None:
        """ update a ordereddevicesetupssubset model instance """
        try:
            res = await self.ordereddevicesetupssubset_client.Update(
                lara_django_material_pb2.OrderedDeviceSetupsSubsetRequest(**ordereddevicesetupssubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating ordereddevicesetupssubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a ordereddevicesetupssubset by ordereddevicesetupssubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.ordereddevicesetupssubset_client.Destroy(lara_django_material_pb2.OrderedDeviceSetupsSubsetDestroyRequest(ordereddevicesetupssubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting ordereddevicesetupssubset_id: {e}")

#_________________  LabwareClass  ______________________


class LabwareClassInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.labwareclass_client = lara_django_material_pb2_grpc.LabwareClassControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  labwareclasses:  list[material_dm.LabwareClass] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.LabwareClass]]:
        try:
            create_coroutines = ( self.labwareclass_client.Create(lara_django_material_pb2.LabwareClassRequest(**labwareclass.model_dump())) for labwareclass in labwareclasses )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.labwareclass_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating labwareclass: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        labwareclass_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.LabwareClass]:
        """ retrieve a list of labwareclass instances by labwareclass_id, name or filter_dict,
               returns a list of labwareclass data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  labwareclass_id is not None:
            try:
                res =  await self.labwareclass_client.Retrieve(
                    lara_django_material_pb2.LabwareClassRetrieveRequest(labwareclass_id=labwareclass_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.LabwareClass(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving labwareclass_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.labwareclass_client.List(
                    lara_django_material_pb2.LabwareClassListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.LabwareClass(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving labwareclass name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the labwareclass_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].labwareclass_id
        else:
            raise ValueError(f"Error retrieving labwareclass_id - no or too many results found.")
    
    async def get_by_id(self, labwareclass_id: str = None) -> material_dm.LabwareClass:
        """ retrieve a labwareclass data model by labwareclass_id """
        try:
            res = await self.labwareclass_client.Retrieve(
                lara_django_material_pb2.LabwareClassRetrieveRequest(labwareclass_id=labwareclass_id)
            )
            return material_dm.LabwareClass(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving labwareclass_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.LabwareClass | str:
        """ retrieve a labwareclass data model by name """
        try:
            res = await self.labwareclass_client.RetrieveByNameNamespace(
                lara_django_material_pb2.LabwareClassRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["labwareclass_id"]
            else:
                return material_dm.LabwareClass(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving labwareclass name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all labwareclasss """
        if filter_dict is None:
            res = await self.labwareclass_client.List(lara_django_material_pb2.LabwareClassListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.labwareclass_client.List(
                lara_django_material_pb2.LabwareClassListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.labwareclass_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all labwareclasss """
        if self.labwareclass_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.labwareclass_full_client.List(
                lara_django_material_pb2.LabwareClassFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.labwareclass_full_client.List(
                lara_django_material_pb2.LabwareClassFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, labwareclass : material_dm.LabwareClass = None) -> None:
        """ update a labwareclass model instance """
        try:
            res = await self.labwareclass_client.Update(
                lara_django_material_pb2.LabwareClassRequest(**labwareclass.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating labwareclass_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a labwareclass by labwareclass_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.labwareclass_client.Destroy(lara_django_material_pb2.LabwareClassDestroyRequest(labwareclass_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting labwareclass_id: {e}")

#_________________  WellShape  ______________________


class WellShapeInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.wellshape_client = lara_django_material_pb2_grpc.WellShapeControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  wellshapes:  list[material_dm.WellShape] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.WellShape]]:
        try:
            create_coroutines = ( self.wellshape_client.Create(lara_django_material_pb2.WellShapeRequest(**wellshape.model_dump())) for wellshape in wellshapes )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.wellshape_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating wellshape: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        wellshape_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.WellShape]:
        """ retrieve a list of wellshape instances by wellshape_id, name or filter_dict,
               returns a list of wellshape data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  wellshape_id is not None:
            try:
                res =  await self.wellshape_client.Retrieve(
                    lara_django_material_pb2.WellShapeRetrieveRequest(wellshape_id=wellshape_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.WellShape(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving wellshape_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.wellshape_client.List(
                    lara_django_material_pb2.WellShapeListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.WellShape(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving wellshape name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the wellshape_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].wellshape_id
        else:
            raise ValueError(f"Error retrieving wellshape_id - no or too many results found.")
    
    async def get_by_id(self, wellshape_id: str = None) -> material_dm.WellShape:
        """ retrieve a wellshape data model by wellshape_id """
        try:
            res = await self.wellshape_client.Retrieve(
                lara_django_material_pb2.WellShapeRetrieveRequest(wellshape_id=wellshape_id)
            )
            return material_dm.WellShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving wellshape_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.WellShape | str:
        """ retrieve a wellshape data model by name """
        try:
            res = await self.wellshape_client.RetrieveByNameNamespace(
                lara_django_material_pb2.WellShapeRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["wellshape_id"]
            else:
                return material_dm.WellShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving wellshape name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all wellshapes """
        if filter_dict is None:
            res = await self.wellshape_client.List(lara_django_material_pb2.WellShapeListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.wellshape_client.List(
                lara_django_material_pb2.WellShapeListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.wellshape_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all wellshapes """
        if self.wellshape_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.wellshape_full_client.List(
                lara_django_material_pb2.WellShapeFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.wellshape_full_client.List(
                lara_django_material_pb2.WellShapeFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, wellshape : material_dm.WellShape = None) -> None:
        """ update a wellshape model instance """
        try:
            res = await self.wellshape_client.Update(
                lara_django_material_pb2.WellShapeRequest(**wellshape.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating wellshape_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a wellshape by wellshape_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.wellshape_client.Destroy(lara_django_material_pb2.WellShapeDestroyRequest(wellshape_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting wellshape_id: {e}")

#_________________  LabwareShape  ______________________


class LabwareShapeInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.labwareshape_client = lara_django_material_pb2_grpc.LabwareShapeControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  labwareshapes:  list[material_dm.LabwareShape] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.LabwareShape]]:
        try:
            create_coroutines = ( self.labwareshape_client.Create(lara_django_material_pb2.LabwareShapeRequest(**labwareshape.model_dump())) for labwareshape in labwareshapes )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.labwareshape_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating labwareshape: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        labwareshape_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.LabwareShape]:
        """ retrieve a list of labwareshape instances by labwareshape_id, name or filter_dict,
               returns a list of labwareshape data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  labwareshape_id is not None:
            try:
                res =  await self.labwareshape_client.Retrieve(
                    lara_django_material_pb2.LabwareShapeRetrieveRequest(labwareshape_id=labwareshape_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.LabwareShape(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving labwareshape_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.labwareshape_client.List(
                    lara_django_material_pb2.LabwareShapeListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.LabwareShape(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving labwareshape name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the labwareshape_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].labwareshape_id
        else:
            raise ValueError(f"Error retrieving labwareshape_id - no or too many results found.")
    
    async def get_by_id(self, labwareshape_id: str = None) -> material_dm.LabwareShape:
        """ retrieve a labwareshape data model by labwareshape_id """
        try:
            res = await self.labwareshape_client.Retrieve(
                lara_django_material_pb2.LabwareShapeRetrieveRequest(labwareshape_id=labwareshape_id)
            )
            return material_dm.LabwareShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving labwareshape_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.LabwareShape | str:
        """ retrieve a labwareshape data model by name """
        try:
            res = await self.labwareshape_client.RetrieveByNameNamespace(
                lara_django_material_pb2.LabwareShapeRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["labwareshape_id"]
            else:
                return material_dm.LabwareShape(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving labwareshape name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all labwareshapes """
        if filter_dict is None:
            res = await self.labwareshape_client.List(lara_django_material_pb2.LabwareShapeListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.labwareshape_client.List(
                lara_django_material_pb2.LabwareShapeListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.labwareshape_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all labwareshapes """
        if self.labwareshape_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.labwareshape_full_client.List(
                lara_django_material_pb2.LabwareShapeFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.labwareshape_full_client.List(
                lara_django_material_pb2.LabwareShapeFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, labwareshape : material_dm.LabwareShape = None) -> None:
        """ update a labwareshape model instance """
        try:
            res = await self.labwareshape_client.Update(
                lara_django_material_pb2.LabwareShapeRequest(**labwareshape.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating labwareshape_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a labwareshape by labwareshape_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.labwareshape_client.Destroy(lara_django_material_pb2.LabwareShapeDestroyRequest(labwareshape_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting labwareshape_id: {e}")

#_________________  Labware  ______________________


class LabwareInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.labware_client = lara_django_material_pb2_grpc.LabwareControllerStub(channel)
        
        self.item_id = None
        self.update_item_id = "labware_id"
        self.stub = self.labware_client

        self.image_upload_chunk = lara_django_material_pb2.ImageUploadChunk

    
    @import_model_instance_from_file()
    @upload_image  # needed for image upload
    async def create(self,  labwares:  list[material_dm.Labware] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.Labware]]:
        try:
            create_coroutines = ( self.labware_client.Create(lara_django_material_pb2.LabwareRequest(**labware.model_dump())) for labware in labwares )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.labware_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating labware: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        labware_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.Labware]:
        """ retrieve a list of labware instances by labware_id, name or filter_dict,
               returns a list of labware data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  labware_id is not None:
            try:
                res =  await self.labware_client.Retrieve(
                    lara_django_material_pb2.LabwareRetrieveRequest(labware_id=labware_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.Labware(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving labware_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.labware_client.List(
                    lara_django_material_pb2.LabwareListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.Labware(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving labware name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the labware_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].labware_id
        else:
            raise ValueError(f"Error retrieving labware_id - no or too many results found.")
    
    @download_image  # needed for image download
    async def get_by_id(self, labware_id: str = None) -> material_dm.Labware:
        """ retrieve a labware data model by labware_id """
        try:
            res = await self.labware_client.Retrieve(
                lara_django_material_pb2.LabwareRetrieveRequest(labware_id=labware_id)
            )
            return material_dm.Labware(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving labware_id: {e}")
    
    @download_image  # needed for image download
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.Labware | str:
        """ retrieve a labware data model by name """
        try:
            res = await self.labware_client.RetrieveByNameNamespace(
                lara_django_material_pb2.LabwareRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["labware_id"]
            else:
                return material_dm.Labware(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving labware name: {e}")
    
    
    
    @download_image  # needed for image download
    async def get_image(self, labware_id : str = None, id_only : bool = True) -> bytes | None:
        """ retrieve an image by data_id """
        self.item_id = labware_id
        
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all labwares """
        if filter_dict is None:
            res = await self.labware_client.List(lara_django_material_pb2.LabwareListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.labware_client.List(
                lara_django_material_pb2.LabwareListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.labware_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all labwares """
        if self.labware_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.labware_full_client.List(
                lara_django_material_pb2.LabwareFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.labware_full_client.List(
                lara_django_material_pb2.LabwareFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    @update_image  # needed for image update
    async def update(self, labware : material_dm.Labware = None) -> None:
        """ update a labware model instance """
        try:
            res = await self.labware_client.Update(
                lara_django_material_pb2.LabwareRequest(**labware.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating labware_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a labware by labware_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.labware_client.Destroy(lara_django_material_pb2.LabwareDestroyRequest(labware_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting labware_id: {e}")

#_________________  LabwareSubset  ______________________


class LabwareSubsetInterface:
    def __init__(self, channel: grpc.Channel = None, 
                       default_namespace_uri : str = None,
                       default_namespace_id : str = None,
                       namespace_if = None ) -> None:
        self._default_namespace_uri = default_namespace_uri
        self._default_namespace_id = default_namespace_id

        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.namespace_if = namespace_if if namespace_if is not None else base_if.NamespaceInterface(channel)

        # self.labwaresubset_class_client = (
        #     lara_django_material_pb2_grpc.LabwareSubsetclassByIRIControllerStub(channel)
        # )

        self.labwaresubset_client = lara_django_material_pb2_grpc.LabwareSubsetControllerStub(channel)
        

        # self.labwaresubset_full_client = lara_django_material_pb2_grpc.LabwareSubsetFullControllerStub(
        #     channel
        # )

    @property
    def default_namespace_uri(self):
        return self._default_namespace_uri

    @property
    def default_namespace_id(self):
        return self._default_namespace_id

    @classmethod
    async def build_if(cls, channel: grpc.Channel = None, default_namespace_uri: str = None):
    
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel
    
        namespace_if = base_if.NamespaceInterface(channel)
        try:
            default_namespace_id = await namespace_if.retrieve_id(namespace_uri=default_namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
        
        return cls(channel=channel, default_namespace_uri=default_namespace_uri,  
                   default_namespace_id=default_namespace_id, namespace_if=namespace_if)
    
    async def set_default_namespace_uri(self, namespace_uri: str = None):
        if namespace_uri is not None:
            self._default_namespace_uri = namespace_uri.strip()
        try:
            self._default_namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
        except Exception as e:
            logger.error(f"Error retrieving namespace_id: {e}")
    
    @import_model_instance_from_file()
    async def create(self,  labwaresubsets:  list[material_dm.LabwareSubset] = None,  
                            ids_only: bool = False,
                            namespace_uri: str = None) -> Union[list[str], list[material_dm.LabwareSubset]]:
        """ create a list of labwaresubset instances,
               returns a list of labwaresubset data model objects or ids_only

        :param labwaresubsets: list of labwaresubset data model objects
        :param ids_only: return only the ids of the created instances
        :return: list of labwaresubset data model objects or ids_only
        """
        labwaresubsetclass_id = None

        namespace_id = None
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
            namespace_id = self._default_namespace_id
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        if namespace_id is None:
            try:
                namespace_id = await self.namespace_if.retrieve_id(namespace_uri=namespace_uri)
            except Exception as e:
                logger.error(f"Error retrieving namespace_id: {e}")

        # some sample code for retrieving a class IRI and adding namespace and class to the model
        # if labwaresubset_class_iri is None:
        #     raise ValueError("No sample class IRI is given.")
        # try:
        #     res = await self.labwaresubset_class_client.Retrieve(
        #         lara_django_material_pb2.LabwareSubsetclassRetrieveRequest(iri=labwaresubset_class_iri)
        #     )
        #     labwaresubsetclass_id = res.labwaresubsetclass_id
        # except Exception as e:
        #     logger.error(f"Error retrieving labwaresubsetclass_id: {e}")
        for labwaresubset in labwaresubsets:
            if labwaresubset.namespace == "" or labwaresubset.namespace == "default":
                    labwaresubset.namespace = namespace_id
            # labwaresubset.labwaresubset_class = labwaresubsetclass_id # uncomment if a model class is used

        try:
            create_coroutines = ( self.labwaresubset_client.Create(lara_django_material_pb2.LabwareSubsetRequest(**labwaresubset.model_dump())) for labwaresubset in labwaresubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [ MessageToDict(item, preserving_proto_field_name=True).get("labwaresubset_id") for item in res ]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating labwaresubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        namespace_uri: str = None,
        labwaresubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.LabwareSubset]:
        """ retrieve a list of labwaresubset instances by labwaresubset_id, name or filter_dict,
               returns a list of labwaresubset data model objects,
               if raw is True, the raw protobuf objects are returned.
               If namespace_uri is "default", the default namespace_uri is used.
        """
        if namespace_uri == "default":
            if self._default_namespace_uri is not None:
                namespace_uri = self._default_namespace_uri
            else:
                raise ValueError("No default namespace URI is given.")
        
        results_list = []
        if  labwaresubset_id is not None:
            try:
                res =  await self.labwaresubset_client.Retrieve(
                    lara_django_material_pb2.LabwareSubsetRetrieveRequest(labwaresubset_id=labwaresubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.LabwareSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving labwaresubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        if namespace_uri is not None:
            filter_dict["namespace__uri"] = namespace_uri
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.labwaresubset_client.List(
                    lara_django_material_pb2.LabwareSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # :TODO: a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.LabwareSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving labwaresubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          namespace_uri : str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the labwaresubset_id for a given name,
               name_display or iri
        """

        results_list = await self.retrieve(name=name, 
                        namespace_uri=namespace_uri, filter_dict=filter_dict, raw=True)

        if len(results_list) == 1 :
            return results_list[0].labwaresubset_id
        else:
            raise ValueError(f"Error retrieving labwaresubset_id - no or too many results found.")
    
    async def get_by_id(self, labwaresubset_id: str = None, id_only: bool = False) -> material_dm.LabwareSubset:
        """ retrieve a labwaresubset data model by labwaresubset_id """
        try:
            res = await self.labwaresubset_client.Retrieve(
                lara_django_material_pb2.LabwareSubsetRetrieveRequest(labwaresubset_id=labwaresubset_id)
            )
            item = material_dm.LabwareSubset(**MessageToDict(res, preserving_proto_field_name=True))
            self.item_id = item.labwaresubset_id
            return item
        except Exception as e:
            logger.error(f"Error retrieving labwaresubset_id: {e}")
    
    async def get_by_name(self, name: str = None,
                                namespace_uri : str = None,
                                id_only: bool = False) -> material_dm.LabwareSubset | str:
        """ retrieve a labwaresubset data model by name """
        if namespace_uri is None and self._default_namespace_uri is not None:
            namespace_uri = self._default_namespace_uri
        if namespace_uri is None:
            raise ValueError("No namespace URI is given.")
        try:
            res = await self.labwaresubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.LabwareSubsetRetrieveByNameNSRequest(name=name, namespace_uri=namespace_uri)
            )
            item = MessageToDict(res, preserving_proto_field_name=True)
            self.item_id = item["labwaresubset_id"]
            if id_only:
                return self.item_id
            else:
                return material_dm.LabwareSubset(**item)
        except Exception as e:
            logger.error(f"Error retrieving labwaresubset name: {e}")
   
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all labwaresubsets """
        if filter_dict is None:
            res = await self.labwaresubset_client.List(lara_django_material_pb2.LabwareSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.labwaresubset_client.List(
                lara_django_material_pb2.LabwareSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.labwaresubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all labwaresubsets """
        if self.labwaresubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.labwaresubset_full_client.List(
                lara_django_material_pb2.LabwareSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.labwaresubset_full_client.List(
                lara_django_material_pb2.LabwareSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, labwaresubset : material_dm.LabwareSubset = None) -> None:
        """ update a labwaresubset model instance """
        try:
            res = await self.labwaresubset_client.Update(
                lara_django_material_pb2.LabwareSubsetRequest(**labwaresubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating labwaresubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a labwaresubset by labwaresubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.labwaresubset_client.Destroy(lara_django_material_pb2.LabwareSubsetDestroyRequest(labwaresubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting labwaresubset_id: {e}")

#_________________  OrderedLabwareSubset  ______________________


class OrderedLabwareSubsetInterface:
    def __init__(self, channel: grpc.Channel = None) -> None:
        if channel is None:
            cs = GRPCChannelSingleton()
            channel = cs.channel

        self.orderedlabwaresubset_client = lara_django_material_pb2_grpc.OrderedLabwareSubsetControllerStub(channel)
        
    
    @import_model_instance_from_file()
    async def create(self,  orderedlabwaresubsets:  list[material_dm.OrderedLabwareSubset] = None,  ids_only: bool = False) -> Union[list[str], list[material_dm.OrderedLabwareSubset]]:
        try:
            create_coroutines = ( self.orderedlabwaresubset_client.Create(lara_django_material_pb2.OrderedLabwareSubsetRequest(**orderedlabwaresubset.model_dump())) for orderedlabwaresubset in orderedlabwaresubsets )
            res = await asyncio.gather(*create_coroutines)
            if ids_only:
                return [item.orderedlabwaresubset_id for item in res]
            else:
                return res

        except Exception as e:
            logger.error(f"Error creating orderedlabwaresubset: {e}")
        
    @export_model_instance_to_file
    async def retrieve(
        self,
        orderedlabwaresubset_id: str = None,
        name: str = None,
        # name_display: str = None,
        filter_dict: dict = None,
        raw : bool = False
        ) -> list[material_dm.OrderedLabwareSubset]:
        """ retrieve a list of orderedlabwaresubset instances by orderedlabwaresubset_id, name or filter_dict,
               returns a list of orderedlabwaresubset data model objects,
               if raw is True, the raw protobuf objects are returned
        """
        results_list = []
        if  orderedlabwaresubset_id is not None:
            try:
                res =  await self.orderedlabwaresubset_client.Retrieve(
                    lara_django_material_pb2.OrderedLabwareSubsetRetrieveRequest(orderedlabwaresubset_id=orderedlabwaresubset_id) )
                if raw:
                    results_list.append(res)
                else:
                    results_list.append( material_dm.OrderedLabwareSubset(**MessageToDict(res, preserving_proto_field_name=True)) )
            except Exception as e:
                logger.error(f"Error retrieving orderedlabwaresubset_id: {e}")

        if filter_dict is None:
            filter_dict = {}

        if name is not None:
            filter_dict["name"] = name
        # if name_display is not None:
        #    filter_dict["name_display"] = name_display

        metadata = (("filters", (json.dumps(filter_dict))),)
        try:
            res = await self.orderedlabwaresubset_client.List(
                    lara_django_material_pb2.OrderedLabwareSubsetListRequest(), metadata=metadata
                    )
            if raw:
                    results_list += res.results
            else:
                # a queue could be used here to speed up the retrieval
                results = res.results
                results_list += [ material_dm.OrderedLabwareSubset(**MessageToDict(res, preserving_proto_field_name=True)) for res in results ]
        except Exception as e:
            logger.error(f"Error retrieving orderedlabwaresubset name: {e}")

        return results_list

    #@id_cache
    async def retrieve_id(self,
                          name: str = None,
                          # name_display: str = None,
                          filter_dict : dict = None,
                          iri: str = None,) -> str:
        """ retrieve the orderedlabwaresubset_id for a given name,
               (name_display) or iri
        """
        
        results_list = await self.retrieve(name=name, filter_dict=filter_dict, raw=True)
        if len(results_list) == 1 :
            return results_list[0].orderedlabwaresubset_id
        else:
            raise ValueError(f"Error retrieving orderedlabwaresubset_id - no or too many results found.")
    
    async def get_by_id(self, orderedlabwaresubset_id: str = None) -> material_dm.OrderedLabwareSubset:
        """ retrieve a orderedlabwaresubset data model by orderedlabwaresubset_id """
        try:
            res = await self.orderedlabwaresubset_client.Retrieve(
                lara_django_material_pb2.OrderedLabwareSubsetRetrieveRequest(orderedlabwaresubset_id=orderedlabwaresubset_id)
            )
            return material_dm.OrderedLabwareSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving orderedlabwaresubset_id: {e}")
    
    async def get_by_name(self, name: str = None, id_only: bool = False) -> material_dm.OrderedLabwareSubset | str:
        """ retrieve a orderedlabwaresubset data model by name """
        try:
            res = await self.orderedlabwaresubset_client.RetrieveByNameNamespace(
                lara_django_material_pb2.OrderedLabwareSubsetRetrieveByNameNSRequest(name=name, namespace_uri="")
            )
            if id_only:
                return MessageToDict(res, preserving_proto_field_name=True)["orderedlabwaresubset_id"]
            else:
                return material_dm.OrderedLabwareSubset(**MessageToDict(res, preserving_proto_field_name=True))
        except Exception as e:
            logger.error(f"Error retrieving orderedlabwaresubset name: {e}")
    
    
    
    
    async def list_items(self, filter_dict: dict = None, ids_only: bool = False) -> list[str]:
        """ list all orderedlabwaresubsets """
        if filter_dict is None:
            res = await self.orderedlabwaresubset_client.List(lara_django_material_pb2.OrderedLabwareSubsetListRequest())
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            res = await self.orderedlabwaresubset_client.List(
                lara_django_material_pb2.OrderedLabwareSubsetListRequest(), metadata=metadata
            )
        if res is not None and ids_only:
            return [item.orderedlabwaresubset_id for item in res.results]
        else:
            return res.results

    async def list_full(self, filter_dict: dict = None) -> list:
        """ list all orderedlabwaresubsets """
        if self.orderedlabwaresubset_full_client is None:
            raise ValueError("No full client available.")
        if filter_dict is None:
            return await self.orderedlabwaresubset_full_client.List(
                lara_django_material_pb2.OrderedLabwareSubsetFullListRequest()
            )
        else:
            metadata = (("filters", (json.dumps(filter_dict))),)
            return await self.orderedlabwaresubset_full_client.List(
                lara_django_material_pb2.OrderedLabwareSubsetFullListRequest(), metadata=metadata
            )
    
    @import_model_instance_from_file()
    async def update(self, orderedlabwaresubset : material_dm.OrderedLabwareSubset = None) -> None:
        """ update a orderedlabwaresubset model instance """
        try:
            res = await self.orderedlabwaresubset_client.Update(
                lara_django_material_pb2.OrderedLabwareSubsetRequest(**orderedlabwaresubset.model_dump()
            ))
        except Exception as e:
            logger.error(f"Error updating orderedlabwaresubset_id: {e}")
        else:
            return res
    
    async def delete(self, id_list: list[str] = None, filter_dict: dict = None) -> None:
        """ delete a orderedlabwaresubset by orderedlabwaresubset_id or filter_dict """
        if filter_dict is not None:
            id_list = await self.list_items(filter_dict=filter_dict, ids_only=True)

        try:
            delete_coroutines = ( self.orderedlabwaresubset_client.Destroy(lara_django_material_pb2.OrderedLabwareSubsetDestroyRequest(orderedlabwaresubset_id=id)) for id in id_list )
            await asyncio.gather(*delete_coroutines)

        except Exception as e:
            logger.error(f"Error deleting orderedlabwaresubset_id: {e}")

