from google.protobuf import empty_pb2 as _empty_pb2
from google.protobuf import struct_pb2 as _struct_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class DeviceDestroyRequest(_message.Message):
    __slots__ = ("device_id",)
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    device_id: str
    def __init__(self, device_id: _Optional[str] = ...) -> None: ...

class DeviceListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[DeviceResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[DeviceResponse, _Mapping]]] = ...) -> None: ...

class DevicePartialUpdateRequest(_message.Message):
    __slots__ = ("device_id", "manufacturer", "device_class", "components", "shape", "resources_external", "data_extra", "_partial_update_fields", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "labware_min_capacity", "labware_max_capacity", "labware_max_height", "labware_storage_layout", "tags", "name_display", "hardware_config", "search_vector")
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    DEVICE_CLASS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MIN_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MAX_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MAX_HEIGHT_FIELD_NUMBER: _ClassVar[int]
    LABWARE_STORAGE_LAYOUT_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    device_id: str
    manufacturer: str
    device_class: str
    components: _containers.RepeatedScalarFieldContainer[str]
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    labware_min_capacity: int
    labware_max_capacity: int
    labware_max_height: float
    labware_storage_layout: _struct_pb2.Struct
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    def __init__(self, device_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., device_class: _Optional[str] = ..., components: _Optional[_Iterable[str]] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., labware_min_capacity: _Optional[int] = ..., labware_max_capacity: _Optional[int] = ..., labware_max_height: _Optional[float] = ..., labware_storage_layout: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ...) -> None: ...

class DeviceRequest(_message.Message):
    __slots__ = ("device_id", "manufacturer", "device_class", "components", "shape", "resources_external", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "labware_min_capacity", "labware_max_capacity", "labware_max_height", "labware_storage_layout", "tags", "name_display", "hardware_config", "search_vector")
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    DEVICE_CLASS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MIN_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MAX_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MAX_HEIGHT_FIELD_NUMBER: _ClassVar[int]
    LABWARE_STORAGE_LAYOUT_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    device_id: str
    manufacturer: str
    device_class: str
    components: _containers.RepeatedScalarFieldContainer[str]
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    labware_min_capacity: int
    labware_max_capacity: int
    labware_max_height: float
    labware_storage_layout: _struct_pb2.Struct
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    def __init__(self, device_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., device_class: _Optional[str] = ..., components: _Optional[_Iterable[str]] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., labware_min_capacity: _Optional[int] = ..., labware_max_capacity: _Optional[int] = ..., labware_max_height: _Optional[float] = ..., labware_storage_layout: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ...) -> None: ...

class DeviceResponse(_message.Message):
    __slots__ = ("device_id", "manufacturer", "device_class", "components", "shape", "resources_external", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "labware_min_capacity", "labware_max_capacity", "labware_max_height", "labware_storage_layout", "tags", "name_display", "hardware_config", "search_vector")
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    DEVICE_CLASS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MIN_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MAX_CAPACITY_FIELD_NUMBER: _ClassVar[int]
    LABWARE_MAX_HEIGHT_FIELD_NUMBER: _ClassVar[int]
    LABWARE_STORAGE_LAYOUT_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    device_id: str
    manufacturer: str
    device_class: str
    components: _containers.RepeatedScalarFieldContainer[str]
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    labware_min_capacity: int
    labware_max_capacity: int
    labware_max_height: float
    labware_storage_layout: _struct_pb2.Struct
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    def __init__(self, device_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., device_class: _Optional[str] = ..., components: _Optional[_Iterable[str]] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., labware_min_capacity: _Optional[int] = ..., labware_max_capacity: _Optional[int] = ..., labware_max_height: _Optional[float] = ..., labware_storage_layout: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ...) -> None: ...

class DeviceRetrieveByNameNSRequest(_message.Message):
    __slots__ = ("name", "namespace_uri")
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_URI_FIELD_NUMBER: _ClassVar[int]
    name: str
    namespace_uri: str
    def __init__(self, name: _Optional[str] = ..., namespace_uri: _Optional[str] = ...) -> None: ...

class DeviceRetrieveRequest(_message.Message):
    __slots__ = ("device_id",)
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    device_id: str
    def __init__(self, device_id: _Optional[str] = ...) -> None: ...

class DeviceSetupDestroyRequest(_message.Message):
    __slots__ = ("devicesetup_id",)
    DEVICESETUP_ID_FIELD_NUMBER: _ClassVar[int]
    devicesetup_id: str
    def __init__(self, devicesetup_id: _Optional[str] = ...) -> None: ...

class DeviceSetupListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceSetupListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[DeviceSetupResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[DeviceSetupResponse, _Mapping]]] = ...) -> None: ...

class DeviceSetupPartialUpdateRequest(_message.Message):
    __slots__ = ("devicesetup_id", "manufacturer", "setup_class", "shape", "blueprint_media_type", "resources_external", "components", "data_extra", "_partial_update_fields", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "blueprint_image", "blueprint_file", "assembly", "tags", "name_display", "hardware_config", "search_vector", "blueprint_pdf", "scheme_svg")
    DEVICESETUP_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    SETUP_CLASS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_MEDIA_TYPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_IMAGE_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_FILE_FIELD_NUMBER: _ClassVar[int]
    ASSEMBLY_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_PDF_FIELD_NUMBER: _ClassVar[int]
    SCHEME_SVG_FIELD_NUMBER: _ClassVar[int]
    devicesetup_id: str
    manufacturer: str
    setup_class: str
    shape: str
    blueprint_media_type: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    components: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    blueprint_image: str
    blueprint_file: str
    assembly: _struct_pb2.Struct
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    blueprint_pdf: str
    scheme_svg: str
    def __init__(self, devicesetup_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., setup_class: _Optional[str] = ..., shape: _Optional[str] = ..., blueprint_media_type: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., components: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., blueprint_image: _Optional[str] = ..., blueprint_file: _Optional[str] = ..., assembly: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ..., blueprint_pdf: _Optional[str] = ..., scheme_svg: _Optional[str] = ...) -> None: ...

class DeviceSetupRequest(_message.Message):
    __slots__ = ("devicesetup_id", "manufacturer", "setup_class", "shape", "blueprint_media_type", "resources_external", "components", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "blueprint_image", "blueprint_file", "assembly", "tags", "name_display", "hardware_config", "search_vector", "blueprint_pdf", "scheme_svg")
    DEVICESETUP_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    SETUP_CLASS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_MEDIA_TYPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_IMAGE_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_FILE_FIELD_NUMBER: _ClassVar[int]
    ASSEMBLY_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_PDF_FIELD_NUMBER: _ClassVar[int]
    SCHEME_SVG_FIELD_NUMBER: _ClassVar[int]
    devicesetup_id: str
    manufacturer: str
    setup_class: str
    shape: str
    blueprint_media_type: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    components: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    blueprint_image: str
    blueprint_file: str
    assembly: _struct_pb2.Struct
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    blueprint_pdf: str
    scheme_svg: str
    def __init__(self, devicesetup_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., setup_class: _Optional[str] = ..., shape: _Optional[str] = ..., blueprint_media_type: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., components: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., blueprint_image: _Optional[str] = ..., blueprint_file: _Optional[str] = ..., assembly: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ..., blueprint_pdf: _Optional[str] = ..., scheme_svg: _Optional[str] = ...) -> None: ...

class DeviceSetupResponse(_message.Message):
    __slots__ = ("devicesetup_id", "manufacturer", "setup_class", "shape", "blueprint_media_type", "resources_external", "components", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "blueprint_image", "blueprint_file", "assembly", "tags", "name_display", "hardware_config", "search_vector", "blueprint_pdf", "scheme_svg")
    DEVICESETUP_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    SETUP_CLASS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_MEDIA_TYPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_IMAGE_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_FILE_FIELD_NUMBER: _ClassVar[int]
    ASSEMBLY_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    BLUEPRINT_PDF_FIELD_NUMBER: _ClassVar[int]
    SCHEME_SVG_FIELD_NUMBER: _ClassVar[int]
    devicesetup_id: str
    manufacturer: str
    setup_class: str
    shape: str
    blueprint_media_type: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    components: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    blueprint_image: str
    blueprint_file: str
    assembly: _struct_pb2.Struct
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    blueprint_pdf: str
    scheme_svg: str
    def __init__(self, devicesetup_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., setup_class: _Optional[str] = ..., shape: _Optional[str] = ..., blueprint_media_type: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., components: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., blueprint_image: _Optional[str] = ..., blueprint_file: _Optional[str] = ..., assembly: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ..., blueprint_pdf: _Optional[str] = ..., scheme_svg: _Optional[str] = ...) -> None: ...

class DeviceSetupRetrieveByNameNSRequest(_message.Message):
    __slots__ = ("name", "namespace_uri")
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_URI_FIELD_NUMBER: _ClassVar[int]
    name: str
    namespace_uri: str
    def __init__(self, name: _Optional[str] = ..., namespace_uri: _Optional[str] = ...) -> None: ...

class DeviceSetupRetrieveRequest(_message.Message):
    __slots__ = ("devicesetup_id",)
    DEVICESETUP_ID_FIELD_NUMBER: _ClassVar[int]
    devicesetup_id: str
    def __init__(self, devicesetup_id: _Optional[str] = ...) -> None: ...

class DeviceSetupShapeDestroyRequest(_message.Message):
    __slots__ = ("devicesetupshape_id",)
    DEVICESETUPSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    devicesetupshape_id: str
    def __init__(self, devicesetupshape_id: _Optional[str] = ...) -> None: ...

class DeviceSetupShapeListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceSetupShapeListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[DeviceSetupShapeResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[DeviceSetupShapeResponse, _Mapping]]] = ...) -> None: ...

class DeviceSetupShapePartialUpdateRequest(_message.Message):
    __slots__ = ("devicesetupshape_id", "data_extra", "_partial_update_fields", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    DEVICESETUPSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    devicesetupshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, devicesetupshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class DeviceSetupShapeRequest(_message.Message):
    __slots__ = ("devicesetupshape_id", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    DEVICESETUPSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    devicesetupshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, devicesetupshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class DeviceSetupShapeResponse(_message.Message):
    __slots__ = ("devicesetupshape_id", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    DEVICESETUPSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    devicesetupshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, devicesetupshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class DeviceSetupShapeRetrieveRequest(_message.Message):
    __slots__ = ("devicesetupshape_id",)
    DEVICESETUPSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    devicesetupshape_id: str
    def __init__(self, devicesetupshape_id: _Optional[str] = ...) -> None: ...

class DeviceSetupShapeStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceSetupStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceSetupsSubsetDestroyRequest(_message.Message):
    __slots__ = ("devicesetupssubset_id",)
    DEVICESETUPSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    devicesetupssubset_id: str
    def __init__(self, devicesetupssubset_id: _Optional[str] = ...) -> None: ...

class DeviceSetupsSubsetListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceSetupsSubsetListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[DeviceSetupsSubsetResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[DeviceSetupsSubsetResponse, _Mapping]]] = ...) -> None: ...

class DeviceSetupsSubsetPartialUpdateRequest(_message.Message):
    __slots__ = ("devicesetupssubset_id", "namespace", "group", "devicesetups", "tags", "_partial_update_fields", "name", "description", "datetime_last_modified", "name_display", "entity")
    DEVICESETUPSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    DEVICESETUPS_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    devicesetupssubset_id: str
    namespace: str
    group: str
    devicesetups: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, devicesetupssubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., devicesetups: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class DeviceSetupsSubsetRequest(_message.Message):
    __slots__ = ("devicesetupssubset_id", "namespace", "group", "devicesetups", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    DEVICESETUPSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    DEVICESETUPS_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    devicesetupssubset_id: str
    namespace: str
    group: str
    devicesetups: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, devicesetupssubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., devicesetups: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class DeviceSetupsSubsetResponse(_message.Message):
    __slots__ = ("devicesetupssubset_id", "namespace", "group", "devicesetups", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    DEVICESETUPSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    DEVICESETUPS_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    devicesetupssubset_id: str
    namespace: str
    group: str
    devicesetups: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, devicesetupssubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., devicesetups: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class DeviceSetupsSubsetRetrieveRequest(_message.Message):
    __slots__ = ("devicesetupssubset_id",)
    DEVICESETUPSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    devicesetupssubset_id: str
    def __init__(self, devicesetupssubset_id: _Optional[str] = ...) -> None: ...

class DeviceSetupsSubsetStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceShapeDestroyRequest(_message.Message):
    __slots__ = ("deviceshape_id",)
    DEVICESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    deviceshape_id: str
    def __init__(self, deviceshape_id: _Optional[str] = ...) -> None: ...

class DeviceShapeListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceShapeListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[DeviceShapeResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[DeviceShapeResponse, _Mapping]]] = ...) -> None: ...

class DeviceShapePartialUpdateRequest(_message.Message):
    __slots__ = ("deviceshape_id", "data_extra", "_partial_update_fields", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    DEVICESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    deviceshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, deviceshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class DeviceShapeRequest(_message.Message):
    __slots__ = ("deviceshape_id", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    DEVICESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    deviceshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, deviceshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class DeviceShapeResponse(_message.Message):
    __slots__ = ("deviceshape_id", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    DEVICESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    deviceshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, deviceshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class DeviceShapeRetrieveRequest(_message.Message):
    __slots__ = ("deviceshape_id",)
    DEVICESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    deviceshape_id: str
    def __init__(self, deviceshape_id: _Optional[str] = ...) -> None: ...

class DeviceShapeStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DeviceStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DevicesSubsetDestroyRequest(_message.Message):
    __slots__ = ("devicessubset_id",)
    DEVICESSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    devicessubset_id: str
    def __init__(self, devicessubset_id: _Optional[str] = ...) -> None: ...

class DevicesSubsetListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class DevicesSubsetListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[DevicesSubsetResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[DevicesSubsetResponse, _Mapping]]] = ...) -> None: ...

class DevicesSubsetPartialUpdateRequest(_message.Message):
    __slots__ = ("devicessubset_id", "namespace", "group", "devices", "tags", "_partial_update_fields", "name", "description", "datetime_last_modified", "name_display", "entity")
    DEVICESSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    DEVICES_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    devicessubset_id: str
    namespace: str
    group: str
    devices: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, devicessubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., devices: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class DevicesSubsetRequest(_message.Message):
    __slots__ = ("devicessubset_id", "namespace", "group", "devices", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    DEVICESSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    DEVICES_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    devicessubset_id: str
    namespace: str
    group: str
    devices: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, devicessubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., devices: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class DevicesSubsetResponse(_message.Message):
    __slots__ = ("devicessubset_id", "namespace", "group", "devices", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    DEVICESSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    DEVICES_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    devicessubset_id: str
    namespace: str
    group: str
    devices: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, devicessubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., devices: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class DevicesSubsetRetrieveRequest(_message.Message):
    __slots__ = ("devicessubset_id",)
    DEVICESSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    devicessubset_id: str
    def __init__(self, devicessubset_id: _Optional[str] = ...) -> None: ...

class DevicesSubsetStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class ExtraDataDestroyRequest(_message.Message):
    __slots__ = ("extradata_id",)
    EXTRADATA_ID_FIELD_NUMBER: _ClassVar[int]
    extradata_id: str
    def __init__(self, extradata_id: _Optional[str] = ...) -> None: ...

class ExtraDataListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class ExtraDataListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[ExtraDataResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[ExtraDataResponse, _Mapping]]] = ...) -> None: ...

class ExtraDataPartialUpdateRequest(_message.Message):
    __slots__ = ("extradata_id", "data_type", "namespace", "media_type", "_partial_update_fields", "name", "name_full", "version", "hash_sha256", "data_json", "iri", "url", "datetime_created", "datetime_last_modified", "description", "image", "file", "name_display", "search_vector")
    EXTRADATA_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_TYPE_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    MEDIA_TYPE_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_FULL_FIELD_NUMBER: _ClassVar[int]
    VERSION_FIELD_NUMBER: _ClassVar[int]
    HASH_SHA256_FIELD_NUMBER: _ClassVar[int]
    DATA_JSON_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    DATETIME_CREATED_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    FILE_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    extradata_id: str
    data_type: str
    namespace: str
    media_type: str
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_full: str
    version: str
    hash_sha256: str
    data_json: _struct_pb2.Struct
    iri: str
    url: str
    datetime_created: str
    datetime_last_modified: str
    description: str
    image: str
    file: str
    name_display: str
    search_vector: str
    def __init__(self, extradata_id: _Optional[str] = ..., data_type: _Optional[str] = ..., namespace: _Optional[str] = ..., media_type: _Optional[str] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_full: _Optional[str] = ..., version: _Optional[str] = ..., hash_sha256: _Optional[str] = ..., data_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., iri: _Optional[str] = ..., url: _Optional[str] = ..., datetime_created: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., image: _Optional[str] = ..., file: _Optional[str] = ..., name_display: _Optional[str] = ..., search_vector: _Optional[str] = ...) -> None: ...

class ExtraDataRequest(_message.Message):
    __slots__ = ("extradata_id", "data_type", "namespace", "media_type", "name", "name_full", "version", "hash_sha256", "data_json", "iri", "url", "datetime_created", "datetime_last_modified", "description", "image", "file", "name_display", "search_vector")
    EXTRADATA_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_TYPE_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    MEDIA_TYPE_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_FULL_FIELD_NUMBER: _ClassVar[int]
    VERSION_FIELD_NUMBER: _ClassVar[int]
    HASH_SHA256_FIELD_NUMBER: _ClassVar[int]
    DATA_JSON_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    DATETIME_CREATED_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    FILE_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    extradata_id: str
    data_type: str
    namespace: str
    media_type: str
    name: str
    name_full: str
    version: str
    hash_sha256: str
    data_json: _struct_pb2.Struct
    iri: str
    url: str
    datetime_created: str
    datetime_last_modified: str
    description: str
    image: str
    file: str
    name_display: str
    search_vector: str
    def __init__(self, extradata_id: _Optional[str] = ..., data_type: _Optional[str] = ..., namespace: _Optional[str] = ..., media_type: _Optional[str] = ..., name: _Optional[str] = ..., name_full: _Optional[str] = ..., version: _Optional[str] = ..., hash_sha256: _Optional[str] = ..., data_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., iri: _Optional[str] = ..., url: _Optional[str] = ..., datetime_created: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., image: _Optional[str] = ..., file: _Optional[str] = ..., name_display: _Optional[str] = ..., search_vector: _Optional[str] = ...) -> None: ...

class ExtraDataResponse(_message.Message):
    __slots__ = ("extradata_id", "data_type", "namespace", "media_type", "name", "name_full", "version", "hash_sha256", "data_json", "iri", "url", "datetime_created", "datetime_last_modified", "description", "image", "file", "name_display", "search_vector")
    EXTRADATA_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_TYPE_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    MEDIA_TYPE_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_FULL_FIELD_NUMBER: _ClassVar[int]
    VERSION_FIELD_NUMBER: _ClassVar[int]
    HASH_SHA256_FIELD_NUMBER: _ClassVar[int]
    DATA_JSON_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    DATETIME_CREATED_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    FILE_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    extradata_id: str
    data_type: str
    namespace: str
    media_type: str
    name: str
    name_full: str
    version: str
    hash_sha256: str
    data_json: _struct_pb2.Struct
    iri: str
    url: str
    datetime_created: str
    datetime_last_modified: str
    description: str
    image: str
    file: str
    name_display: str
    search_vector: str
    def __init__(self, extradata_id: _Optional[str] = ..., data_type: _Optional[str] = ..., namespace: _Optional[str] = ..., media_type: _Optional[str] = ..., name: _Optional[str] = ..., name_full: _Optional[str] = ..., version: _Optional[str] = ..., hash_sha256: _Optional[str] = ..., data_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., iri: _Optional[str] = ..., url: _Optional[str] = ..., datetime_created: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., image: _Optional[str] = ..., file: _Optional[str] = ..., name_display: _Optional[str] = ..., search_vector: _Optional[str] = ...) -> None: ...

class ExtraDataRetrieveRequest(_message.Message):
    __slots__ = ("extradata_id",)
    EXTRADATA_ID_FIELD_NUMBER: _ClassVar[int]
    extradata_id: str
    def __init__(self, extradata_id: _Optional[str] = ...) -> None: ...

class ExtraDataStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class FileDownloadChunk(_message.Message):
    __slots__ = ("filename", "data", "success")
    FILENAME_FIELD_NUMBER: _ClassVar[int]
    DATA_FIELD_NUMBER: _ClassVar[int]
    SUCCESS_FIELD_NUMBER: _ClassVar[int]
    filename: str
    data: bytes
    success: bool
    def __init__(self, filename: _Optional[str] = ..., data: _Optional[bytes] = ..., success: bool = ...) -> None: ...

class FileDownloadInfo(_message.Message):
    __slots__ = ("item_id", "chunk_size")
    ITEM_ID_FIELD_NUMBER: _ClassVar[int]
    CHUNK_SIZE_FIELD_NUMBER: _ClassVar[int]
    item_id: str
    chunk_size: int
    def __init__(self, item_id: _Optional[str] = ..., chunk_size: _Optional[int] = ...) -> None: ...

class FileUploadChunk(_message.Message):
    __slots__ = ("filename", "update_item_id", "data")
    FILENAME_FIELD_NUMBER: _ClassVar[int]
    UPDATE_ITEM_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_FIELD_NUMBER: _ClassVar[int]
    filename: str
    update_item_id: str
    data: bytes
    def __init__(self, filename: _Optional[str] = ..., update_item_id: _Optional[str] = ..., data: _Optional[bytes] = ...) -> None: ...

class FileUploadInfo(_message.Message):
    __slots__ = ("success",)
    SUCCESS_FIELD_NUMBER: _ClassVar[int]
    success: bool
    def __init__(self, success: bool = ...) -> None: ...

class ImageDownloadChunk(_message.Message):
    __slots__ = ("filename_image", "data", "success")
    FILENAME_IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATA_FIELD_NUMBER: _ClassVar[int]
    SUCCESS_FIELD_NUMBER: _ClassVar[int]
    filename_image: str
    data: bytes
    success: bool
    def __init__(self, filename_image: _Optional[str] = ..., data: _Optional[bytes] = ..., success: bool = ...) -> None: ...

class ImageDownloadInfo(_message.Message):
    __slots__ = ("item_id", "chunk_size")
    ITEM_ID_FIELD_NUMBER: _ClassVar[int]
    CHUNK_SIZE_FIELD_NUMBER: _ClassVar[int]
    item_id: str
    chunk_size: int
    def __init__(self, item_id: _Optional[str] = ..., chunk_size: _Optional[int] = ...) -> None: ...

class ImageUploadChunk(_message.Message):
    __slots__ = ("filename", "update_item_id", "data")
    FILENAME_FIELD_NUMBER: _ClassVar[int]
    UPDATE_ITEM_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_FIELD_NUMBER: _ClassVar[int]
    filename: str
    update_item_id: str
    data: bytes
    def __init__(self, filename: _Optional[str] = ..., update_item_id: _Optional[str] = ..., data: _Optional[bytes] = ...) -> None: ...

class ImageUploadInfo(_message.Message):
    __slots__ = ("success",)
    SUCCESS_FIELD_NUMBER: _ClassVar[int]
    success: bool
    def __init__(self, success: bool = ...) -> None: ...

class LabwareClassDestroyRequest(_message.Message):
    __slots__ = ("labwareclass_id",)
    LABWARECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    labwareclass_id: str
    def __init__(self, labwareclass_id: _Optional[str] = ...) -> None: ...

class LabwareClassListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareClassListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[LabwareClassResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[LabwareClassResponse, _Mapping]]] = ...) -> None: ...

class LabwareClassPartialUpdateRequest(_message.Message):
    __slots__ = ("labwareclass_id", "_partial_update_fields", "name", "iri", "description")
    LABWARECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    labwareclass_id: str
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    iri: str
    description: str
    def __init__(self, labwareclass_id: _Optional[str] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class LabwareClassRequest(_message.Message):
    __slots__ = ("labwareclass_id", "name", "iri", "description")
    LABWARECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    labwareclass_id: str
    name: str
    iri: str
    description: str
    def __init__(self, labwareclass_id: _Optional[str] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class LabwareClassResponse(_message.Message):
    __slots__ = ("labwareclass_id", "name", "iri", "description")
    LABWARECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    labwareclass_id: str
    name: str
    iri: str
    description: str
    def __init__(self, labwareclass_id: _Optional[str] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class LabwareClassRetrieveRequest(_message.Message):
    __slots__ = ("labwareclass_id",)
    LABWARECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    labwareclass_id: str
    def __init__(self, labwareclass_id: _Optional[str] = ...) -> None: ...

class LabwareClassStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareDestroyRequest(_message.Message):
    __slots__ = ("labware_id",)
    LABWARE_ID_FIELD_NUMBER: _ClassVar[int]
    labware_id: str
    def __init__(self, labware_id: _Optional[str] = ...) -> None: ...

class LabwareListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[LabwareResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[LabwareResponse, _Mapping]]] = ...) -> None: ...

class LabwarePartialUpdateRequest(_message.Message):
    __slots__ = ("labware_id", "manufacturer", "labware_class", "shape", "resources_external", "lids", "components", "data_extra", "_partial_update_fields", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "num_rows", "num_cols", "num_wells", "liddable", "sealable", "tags", "name_display", "hardware_config", "search_vector", "height")
    LABWARE_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    LABWARE_CLASS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    LIDS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    NUM_ROWS_FIELD_NUMBER: _ClassVar[int]
    NUM_COLS_FIELD_NUMBER: _ClassVar[int]
    NUM_WELLS_FIELD_NUMBER: _ClassVar[int]
    LIDDABLE_FIELD_NUMBER: _ClassVar[int]
    SEALABLE_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_FIELD_NUMBER: _ClassVar[int]
    labware_id: str
    manufacturer: str
    labware_class: str
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    lids: _containers.RepeatedScalarFieldContainer[str]
    components: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    num_rows: int
    num_cols: int
    num_wells: int
    liddable: bool
    sealable: bool
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    height: float
    def __init__(self, labware_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., labware_class: _Optional[str] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., lids: _Optional[_Iterable[str]] = ..., components: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., num_rows: _Optional[int] = ..., num_cols: _Optional[int] = ..., num_wells: _Optional[int] = ..., liddable: bool = ..., sealable: bool = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ..., height: _Optional[float] = ...) -> None: ...

class LabwareRequest(_message.Message):
    __slots__ = ("labware_id", "manufacturer", "labware_class", "shape", "resources_external", "lids", "components", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "num_rows", "num_cols", "num_wells", "liddable", "sealable", "tags", "name_display", "hardware_config", "search_vector", "height")
    LABWARE_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    LABWARE_CLASS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    LIDS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    NUM_ROWS_FIELD_NUMBER: _ClassVar[int]
    NUM_COLS_FIELD_NUMBER: _ClassVar[int]
    NUM_WELLS_FIELD_NUMBER: _ClassVar[int]
    LIDDABLE_FIELD_NUMBER: _ClassVar[int]
    SEALABLE_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_FIELD_NUMBER: _ClassVar[int]
    labware_id: str
    manufacturer: str
    labware_class: str
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    lids: _containers.RepeatedScalarFieldContainer[str]
    components: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    num_rows: int
    num_cols: int
    num_wells: int
    liddable: bool
    sealable: bool
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    height: float
    def __init__(self, labware_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., labware_class: _Optional[str] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., lids: _Optional[_Iterable[str]] = ..., components: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., num_rows: _Optional[int] = ..., num_cols: _Optional[int] = ..., num_wells: _Optional[int] = ..., liddable: bool = ..., sealable: bool = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ..., height: _Optional[float] = ...) -> None: ...

class LabwareResponse(_message.Message):
    __slots__ = ("labware_id", "manufacturer", "labware_class", "shape", "resources_external", "lids", "components", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "num_rows", "num_cols", "num_wells", "liddable", "sealable", "tags", "name_display", "hardware_config", "search_vector", "height")
    LABWARE_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    LABWARE_CLASS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    LIDS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    NUM_ROWS_FIELD_NUMBER: _ClassVar[int]
    NUM_COLS_FIELD_NUMBER: _ClassVar[int]
    NUM_WELLS_FIELD_NUMBER: _ClassVar[int]
    LIDDABLE_FIELD_NUMBER: _ClassVar[int]
    SEALABLE_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_FIELD_NUMBER: _ClassVar[int]
    labware_id: str
    manufacturer: str
    labware_class: str
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    lids: _containers.RepeatedScalarFieldContainer[str]
    components: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    num_rows: int
    num_cols: int
    num_wells: int
    liddable: bool
    sealable: bool
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    height: float
    def __init__(self, labware_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., labware_class: _Optional[str] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., lids: _Optional[_Iterable[str]] = ..., components: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., num_rows: _Optional[int] = ..., num_cols: _Optional[int] = ..., num_wells: _Optional[int] = ..., liddable: bool = ..., sealable: bool = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ..., height: _Optional[float] = ...) -> None: ...

class LabwareRetrieveByNameNSRequest(_message.Message):
    __slots__ = ("name", "namespace_uri")
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_URI_FIELD_NUMBER: _ClassVar[int]
    name: str
    namespace_uri: str
    def __init__(self, name: _Optional[str] = ..., namespace_uri: _Optional[str] = ...) -> None: ...

class LabwareRetrieveRequest(_message.Message):
    __slots__ = ("labware_id",)
    LABWARE_ID_FIELD_NUMBER: _ClassVar[int]
    labware_id: str
    def __init__(self, labware_id: _Optional[str] = ...) -> None: ...

class LabwareShapeDestroyRequest(_message.Message):
    __slots__ = ("labwareshape_id",)
    LABWARESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    labwareshape_id: str
    def __init__(self, labwareshape_id: _Optional[str] = ...) -> None: ...

class LabwareShapeListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareShapeListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[LabwareShapeResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[LabwareShapeResponse, _Mapping]]] = ...) -> None: ...

class LabwareShapePartialUpdateRequest(_message.Message):
    __slots__ = ("labwareshape_id", "well_shape", "data_extra", "_partial_update_fields", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "well_dist_row", "well_dist_col", "description")
    LABWARESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    WELL_SHAPE_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    WELL_DIST_ROW_FIELD_NUMBER: _ClassVar[int]
    WELL_DIST_COL_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    labwareshape_id: str
    well_shape: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    well_dist_row: float
    well_dist_col: float
    description: str
    def __init__(self, labwareshape_id: _Optional[str] = ..., well_shape: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., well_dist_row: _Optional[float] = ..., well_dist_col: _Optional[float] = ..., description: _Optional[str] = ...) -> None: ...

class LabwareShapeRequest(_message.Message):
    __slots__ = ("labwareshape_id", "well_shape", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "well_dist_row", "well_dist_col", "description")
    LABWARESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    WELL_SHAPE_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    WELL_DIST_ROW_FIELD_NUMBER: _ClassVar[int]
    WELL_DIST_COL_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    labwareshape_id: str
    well_shape: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    well_dist_row: float
    well_dist_col: float
    description: str
    def __init__(self, labwareshape_id: _Optional[str] = ..., well_shape: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., well_dist_row: _Optional[float] = ..., well_dist_col: _Optional[float] = ..., description: _Optional[str] = ...) -> None: ...

class LabwareShapeResponse(_message.Message):
    __slots__ = ("labwareshape_id", "well_shape", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "well_dist_row", "well_dist_col", "description")
    LABWARESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    WELL_SHAPE_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    WELL_DIST_ROW_FIELD_NUMBER: _ClassVar[int]
    WELL_DIST_COL_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    labwareshape_id: str
    well_shape: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    well_dist_row: float
    well_dist_col: float
    description: str
    def __init__(self, labwareshape_id: _Optional[str] = ..., well_shape: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., well_dist_row: _Optional[float] = ..., well_dist_col: _Optional[float] = ..., description: _Optional[str] = ...) -> None: ...

class LabwareShapeRetrieveRequest(_message.Message):
    __slots__ = ("labwareshape_id",)
    LABWARESHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    labwareshape_id: str
    def __init__(self, labwareshape_id: _Optional[str] = ...) -> None: ...

class LabwareShapeStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareSubsetDestroyRequest(_message.Message):
    __slots__ = ("labwaresubset_id",)
    LABWARESUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    labwaresubset_id: str
    def __init__(self, labwaresubset_id: _Optional[str] = ...) -> None: ...

class LabwareSubsetListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class LabwareSubsetListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[LabwareSubsetResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[LabwareSubsetResponse, _Mapping]]] = ...) -> None: ...

class LabwareSubsetPartialUpdateRequest(_message.Message):
    __slots__ = ("labwaresubset_id", "namespace", "group", "labware", "tags", "_partial_update_fields", "name", "description", "datetime_last_modified", "name_display", "entity")
    LABWARESUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    LABWARE_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    labwaresubset_id: str
    namespace: str
    group: str
    labware: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, labwaresubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., labware: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class LabwareSubsetRequest(_message.Message):
    __slots__ = ("labwaresubset_id", "namespace", "group", "labware", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    LABWARESUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    LABWARE_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    labwaresubset_id: str
    namespace: str
    group: str
    labware: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, labwaresubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., labware: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class LabwareSubsetResponse(_message.Message):
    __slots__ = ("labwaresubset_id", "namespace", "group", "labware", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    LABWARESUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    LABWARE_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    labwaresubset_id: str
    namespace: str
    group: str
    labware: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, labwaresubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., labware: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class LabwareSubsetRetrieveRequest(_message.Message):
    __slots__ = ("labwaresubset_id",)
    LABWARESUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    labwaresubset_id: str
    def __init__(self, labwaresubset_id: _Optional[str] = ...) -> None: ...

class LabwareSubsetStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartDestroyRequest(_message.Message):
    __slots__ = ("part_id",)
    PART_ID_FIELD_NUMBER: _ClassVar[int]
    part_id: str
    def __init__(self, part_id: _Optional[str] = ...) -> None: ...

class PartDeviceClassByNameListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartDeviceClassByNameListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[PartDeviceClassByNameResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[PartDeviceClassByNameResponse, _Mapping]]] = ...) -> None: ...

class PartDeviceClassByNameResponse(_message.Message):
    __slots__ = ("partdeviceclass_id", "name", "iri", "description")
    PARTDEVICECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partdeviceclass_id: str
    name: str
    iri: str
    description: str
    def __init__(self, partdeviceclass_id: _Optional[str] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class PartDeviceClassByNameRetrieveByNameNSRequest(_message.Message):
    __slots__ = ("name", "namespace_uri")
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_URI_FIELD_NUMBER: _ClassVar[int]
    name: str
    namespace_uri: str
    def __init__(self, name: _Optional[str] = ..., namespace_uri: _Optional[str] = ...) -> None: ...

class PartDeviceClassDestroyRequest(_message.Message):
    __slots__ = ("partdeviceclass_id",)
    PARTDEVICECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    partdeviceclass_id: str
    def __init__(self, partdeviceclass_id: _Optional[str] = ...) -> None: ...

class PartDeviceClassListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartDeviceClassListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[PartDeviceClassResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[PartDeviceClassResponse, _Mapping]]] = ...) -> None: ...

class PartDeviceClassPartialUpdateRequest(_message.Message):
    __slots__ = ("partdeviceclass_id", "_partial_update_fields", "name", "iri", "description")
    PARTDEVICECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partdeviceclass_id: str
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    iri: str
    description: str
    def __init__(self, partdeviceclass_id: _Optional[str] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class PartDeviceClassRequest(_message.Message):
    __slots__ = ("partdeviceclass_id", "name", "iri", "description")
    PARTDEVICECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partdeviceclass_id: str
    name: str
    iri: str
    description: str
    def __init__(self, partdeviceclass_id: _Optional[str] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class PartDeviceClassResponse(_message.Message):
    __slots__ = ("partdeviceclass_id", "name", "iri", "description")
    PARTDEVICECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partdeviceclass_id: str
    name: str
    iri: str
    description: str
    def __init__(self, partdeviceclass_id: _Optional[str] = ..., name: _Optional[str] = ..., iri: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class PartDeviceClassRetrieveRequest(_message.Message):
    __slots__ = ("partdeviceclass_id",)
    PARTDEVICECLASS_ID_FIELD_NUMBER: _ClassVar[int]
    partdeviceclass_id: str
    def __init__(self, partdeviceclass_id: _Optional[str] = ...) -> None: ...

class PartDeviceClassStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[PartResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[PartResponse, _Mapping]]] = ...) -> None: ...

class PartPartialUpdateRequest(_message.Message):
    __slots__ = ("part_id", "manufacturer", "part_class", "components", "shape", "resources_external", "data_extra", "_partial_update_fields", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "tags", "name_display", "hardware_config", "search_vector")
    PART_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    PART_CLASS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    part_id: str
    manufacturer: str
    part_class: str
    components: _containers.RepeatedScalarFieldContainer[str]
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    def __init__(self, part_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., part_class: _Optional[str] = ..., components: _Optional[_Iterable[str]] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ...) -> None: ...

class PartRequest(_message.Message):
    __slots__ = ("part_id", "manufacturer", "part_class", "components", "shape", "resources_external", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "tags", "name_display", "hardware_config", "search_vector")
    PART_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    PART_CLASS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    part_id: str
    manufacturer: str
    part_class: str
    components: _containers.RepeatedScalarFieldContainer[str]
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    def __init__(self, part_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., part_class: _Optional[str] = ..., components: _Optional[_Iterable[str]] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ...) -> None: ...

class PartResponse(_message.Message):
    __slots__ = ("part_id", "manufacturer", "part_class", "components", "shape", "resources_external", "data_extra", "name", "url", "pid", "pac_id", "iri", "model_no", "product_type", "type_barcode", "type_barcode_json", "product_no", "weight", "energy_consumption", "spec_json", "icon", "image", "datetime_last_modified", "description", "tags", "name_display", "hardware_config", "search_vector")
    PART_ID_FIELD_NUMBER: _ClassVar[int]
    MANUFACTURER_FIELD_NUMBER: _ClassVar[int]
    PART_CLASS_FIELD_NUMBER: _ClassVar[int]
    COMPONENTS_FIELD_NUMBER: _ClassVar[int]
    SHAPE_FIELD_NUMBER: _ClassVar[int]
    RESOURCES_EXTERNAL_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PID_FIELD_NUMBER: _ClassVar[int]
    PAC_ID_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    MODEL_NO_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_TYPE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_FIELD_NUMBER: _ClassVar[int]
    TYPE_BARCODE_JSON_FIELD_NUMBER: _ClassVar[int]
    PRODUCT_NO_FIELD_NUMBER: _ClassVar[int]
    WEIGHT_FIELD_NUMBER: _ClassVar[int]
    ENERGY_CONSUMPTION_FIELD_NUMBER: _ClassVar[int]
    SPEC_JSON_FIELD_NUMBER: _ClassVar[int]
    ICON_FIELD_NUMBER: _ClassVar[int]
    IMAGE_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    HARDWARE_CONFIG_FIELD_NUMBER: _ClassVar[int]
    SEARCH_VECTOR_FIELD_NUMBER: _ClassVar[int]
    part_id: str
    manufacturer: str
    part_class: str
    components: _containers.RepeatedScalarFieldContainer[str]
    shape: str
    resources_external: _containers.RepeatedScalarFieldContainer[str]
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    url: str
    pid: str
    pac_id: str
    iri: str
    model_no: str
    product_type: str
    type_barcode: str
    type_barcode_json: _struct_pb2.Struct
    product_no: str
    weight: float
    energy_consumption: float
    spec_json: _struct_pb2.Struct
    icon: str
    image: str
    datetime_last_modified: str
    description: str
    tags: _containers.RepeatedScalarFieldContainer[str]
    name_display: str
    hardware_config: _struct_pb2.Struct
    search_vector: str
    def __init__(self, part_id: _Optional[str] = ..., manufacturer: _Optional[str] = ..., part_class: _Optional[str] = ..., components: _Optional[_Iterable[str]] = ..., shape: _Optional[str] = ..., resources_external: _Optional[_Iterable[str]] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., url: _Optional[str] = ..., pid: _Optional[str] = ..., pac_id: _Optional[str] = ..., iri: _Optional[str] = ..., model_no: _Optional[str] = ..., product_type: _Optional[str] = ..., type_barcode: _Optional[str] = ..., type_barcode_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., product_no: _Optional[str] = ..., weight: _Optional[float] = ..., energy_consumption: _Optional[float] = ..., spec_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., icon: _Optional[str] = ..., image: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., description: _Optional[str] = ..., tags: _Optional[_Iterable[str]] = ..., name_display: _Optional[str] = ..., hardware_config: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., search_vector: _Optional[str] = ...) -> None: ...

class PartRetrieveByNameNSRequest(_message.Message):
    __slots__ = ("name", "namespace_uri")
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_URI_FIELD_NUMBER: _ClassVar[int]
    name: str
    namespace_uri: str
    def __init__(self, name: _Optional[str] = ..., namespace_uri: _Optional[str] = ...) -> None: ...

class PartRetrieveRequest(_message.Message):
    __slots__ = ("part_id",)
    PART_ID_FIELD_NUMBER: _ClassVar[int]
    part_id: str
    def __init__(self, part_id: _Optional[str] = ...) -> None: ...

class PartShapeDestroyRequest(_message.Message):
    __slots__ = ("partshape_id",)
    PARTSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    partshape_id: str
    def __init__(self, partshape_id: _Optional[str] = ...) -> None: ...

class PartShapeListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartShapeListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[PartShapeResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[PartShapeResponse, _Mapping]]] = ...) -> None: ...

class PartShapePartialUpdateRequest(_message.Message):
    __slots__ = ("partshape_id", "data_extra", "_partial_update_fields", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    PARTSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, partshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class PartShapeRequest(_message.Message):
    __slots__ = ("partshape_id", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    PARTSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, partshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class PartShapeResponse(_message.Message):
    __slots__ = ("partshape_id", "data_extra", "name", "name_standard", "iri", "width", "length", "height_unlidded", "hight_lidded", "hight_stacked", "hight_stacked_lidded", "model_2d", "model_3d", "model_json", "description")
    PARTSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    NAME_STANDARD_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    WIDTH_FIELD_NUMBER: _ClassVar[int]
    LENGTH_FIELD_NUMBER: _ClassVar[int]
    HEIGHT_UNLIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_LIDDED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_FIELD_NUMBER: _ClassVar[int]
    HIGHT_STACKED_LIDDED_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    partshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    name_standard: str
    iri: str
    width: float
    length: float
    height_unlidded: float
    hight_lidded: float
    hight_stacked: float
    hight_stacked_lidded: float
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, partshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., name_standard: _Optional[str] = ..., iri: _Optional[str] = ..., width: _Optional[float] = ..., length: _Optional[float] = ..., height_unlidded: _Optional[float] = ..., hight_lidded: _Optional[float] = ..., hight_stacked: _Optional[float] = ..., hight_stacked_lidded: _Optional[float] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class PartShapeRetrieveRequest(_message.Message):
    __slots__ = ("partshape_id",)
    PARTSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    partshape_id: str
    def __init__(self, partshape_id: _Optional[str] = ...) -> None: ...

class PartShapeStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartsSubsetDestroyRequest(_message.Message):
    __slots__ = ("partssubset_id",)
    PARTSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    partssubset_id: str
    def __init__(self, partssubset_id: _Optional[str] = ...) -> None: ...

class PartsSubsetListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class PartsSubsetListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[PartsSubsetResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[PartsSubsetResponse, _Mapping]]] = ...) -> None: ...

class PartsSubsetPartialUpdateRequest(_message.Message):
    __slots__ = ("partssubset_id", "namespace", "group", "parts", "tags", "_partial_update_fields", "name", "description", "datetime_last_modified", "name_display", "entity")
    PARTSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    PARTS_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    partssubset_id: str
    namespace: str
    group: str
    parts: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, partssubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., parts: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class PartsSubsetRequest(_message.Message):
    __slots__ = ("partssubset_id", "namespace", "group", "parts", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    PARTSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    PARTS_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    partssubset_id: str
    namespace: str
    group: str
    parts: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, partssubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., parts: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class PartsSubsetResponse(_message.Message):
    __slots__ = ("partssubset_id", "namespace", "group", "parts", "tags", "name", "description", "datetime_last_modified", "name_display", "entity")
    PARTSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    NAMESPACE_FIELD_NUMBER: _ClassVar[int]
    GROUP_FIELD_NUMBER: _ClassVar[int]
    PARTS_FIELD_NUMBER: _ClassVar[int]
    TAGS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_LAST_MODIFIED_FIELD_NUMBER: _ClassVar[int]
    NAME_DISPLAY_FIELD_NUMBER: _ClassVar[int]
    ENTITY_FIELD_NUMBER: _ClassVar[int]
    partssubset_id: str
    namespace: str
    group: str
    parts: _containers.RepeatedScalarFieldContainer[str]
    tags: _containers.RepeatedScalarFieldContainer[str]
    name: str
    description: str
    datetime_last_modified: str
    name_display: str
    entity: str
    def __init__(self, partssubset_id: _Optional[str] = ..., namespace: _Optional[str] = ..., group: _Optional[str] = ..., parts: _Optional[_Iterable[str]] = ..., tags: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., datetime_last_modified: _Optional[str] = ..., name_display: _Optional[str] = ..., entity: _Optional[str] = ...) -> None: ...

class PartsSubsetRetrieveRequest(_message.Message):
    __slots__ = ("partssubset_id",)
    PARTSSUBSET_ID_FIELD_NUMBER: _ClassVar[int]
    partssubset_id: str
    def __init__(self, partssubset_id: _Optional[str] = ...) -> None: ...

class PartsSubsetStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class WellShapeDestroyRequest(_message.Message):
    __slots__ = ("wellshape_id",)
    WELLSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    wellshape_id: str
    def __init__(self, wellshape_id: _Optional[str] = ...) -> None: ...

class WellShapeListRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...

class WellShapeListResponse(_message.Message):
    __slots__ = ("results",)
    RESULTS_FIELD_NUMBER: _ClassVar[int]
    results: _containers.RepeatedCompositeFieldContainer[WellShapeResponse]
    def __init__(self, results: _Optional[_Iterable[_Union[WellShapeResponse, _Mapping]]] = ...) -> None: ...

class WellShapePartialUpdateRequest(_message.Message):
    __slots__ = ("wellshape_id", "data_extra", "_partial_update_fields", "name", "uri", "iri", "depth_well", "shape_well", "shape_bottom", "top_radius_xy", "bottom_radius_xy", "bottom_radius_z", "cone_angle", "cone_depth", "shape_polygon_xy", "shape_polygon_z", "model_2d", "model_3d", "model_json", "description")
    WELLSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    _PARTIAL_UPDATE_FIELDS_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URI_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DEPTH_WELL_FIELD_NUMBER: _ClassVar[int]
    SHAPE_WELL_FIELD_NUMBER: _ClassVar[int]
    SHAPE_BOTTOM_FIELD_NUMBER: _ClassVar[int]
    TOP_RADIUS_XY_FIELD_NUMBER: _ClassVar[int]
    BOTTOM_RADIUS_XY_FIELD_NUMBER: _ClassVar[int]
    BOTTOM_RADIUS_Z_FIELD_NUMBER: _ClassVar[int]
    CONE_ANGLE_FIELD_NUMBER: _ClassVar[int]
    CONE_DEPTH_FIELD_NUMBER: _ClassVar[int]
    SHAPE_POLYGON_XY_FIELD_NUMBER: _ClassVar[int]
    SHAPE_POLYGON_Z_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    wellshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    _partial_update_fields: _containers.RepeatedScalarFieldContainer[str]
    name: str
    uri: str
    iri: str
    depth_well: float
    shape_well: str
    shape_bottom: str
    top_radius_xy: float
    bottom_radius_xy: float
    bottom_radius_z: float
    cone_angle: float
    cone_depth: float
    shape_polygon_xy: str
    shape_polygon_z: str
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, wellshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., _partial_update_fields: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., uri: _Optional[str] = ..., iri: _Optional[str] = ..., depth_well: _Optional[float] = ..., shape_well: _Optional[str] = ..., shape_bottom: _Optional[str] = ..., top_radius_xy: _Optional[float] = ..., bottom_radius_xy: _Optional[float] = ..., bottom_radius_z: _Optional[float] = ..., cone_angle: _Optional[float] = ..., cone_depth: _Optional[float] = ..., shape_polygon_xy: _Optional[str] = ..., shape_polygon_z: _Optional[str] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class WellShapeRequest(_message.Message):
    __slots__ = ("wellshape_id", "data_extra", "name", "uri", "iri", "depth_well", "shape_well", "shape_bottom", "top_radius_xy", "bottom_radius_xy", "bottom_radius_z", "cone_angle", "cone_depth", "shape_polygon_xy", "shape_polygon_z", "model_2d", "model_3d", "model_json", "description")
    WELLSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URI_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DEPTH_WELL_FIELD_NUMBER: _ClassVar[int]
    SHAPE_WELL_FIELD_NUMBER: _ClassVar[int]
    SHAPE_BOTTOM_FIELD_NUMBER: _ClassVar[int]
    TOP_RADIUS_XY_FIELD_NUMBER: _ClassVar[int]
    BOTTOM_RADIUS_XY_FIELD_NUMBER: _ClassVar[int]
    BOTTOM_RADIUS_Z_FIELD_NUMBER: _ClassVar[int]
    CONE_ANGLE_FIELD_NUMBER: _ClassVar[int]
    CONE_DEPTH_FIELD_NUMBER: _ClassVar[int]
    SHAPE_POLYGON_XY_FIELD_NUMBER: _ClassVar[int]
    SHAPE_POLYGON_Z_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    wellshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    uri: str
    iri: str
    depth_well: float
    shape_well: str
    shape_bottom: str
    top_radius_xy: float
    bottom_radius_xy: float
    bottom_radius_z: float
    cone_angle: float
    cone_depth: float
    shape_polygon_xy: str
    shape_polygon_z: str
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, wellshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., uri: _Optional[str] = ..., iri: _Optional[str] = ..., depth_well: _Optional[float] = ..., shape_well: _Optional[str] = ..., shape_bottom: _Optional[str] = ..., top_radius_xy: _Optional[float] = ..., bottom_radius_xy: _Optional[float] = ..., bottom_radius_z: _Optional[float] = ..., cone_angle: _Optional[float] = ..., cone_depth: _Optional[float] = ..., shape_polygon_xy: _Optional[str] = ..., shape_polygon_z: _Optional[str] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class WellShapeResponse(_message.Message):
    __slots__ = ("wellshape_id", "data_extra", "name", "uri", "iri", "depth_well", "shape_well", "shape_bottom", "top_radius_xy", "bottom_radius_xy", "bottom_radius_z", "cone_angle", "cone_depth", "shape_polygon_xy", "shape_polygon_z", "model_2d", "model_3d", "model_json", "description")
    WELLSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    DATA_EXTRA_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    URI_FIELD_NUMBER: _ClassVar[int]
    IRI_FIELD_NUMBER: _ClassVar[int]
    DEPTH_WELL_FIELD_NUMBER: _ClassVar[int]
    SHAPE_WELL_FIELD_NUMBER: _ClassVar[int]
    SHAPE_BOTTOM_FIELD_NUMBER: _ClassVar[int]
    TOP_RADIUS_XY_FIELD_NUMBER: _ClassVar[int]
    BOTTOM_RADIUS_XY_FIELD_NUMBER: _ClassVar[int]
    BOTTOM_RADIUS_Z_FIELD_NUMBER: _ClassVar[int]
    CONE_ANGLE_FIELD_NUMBER: _ClassVar[int]
    CONE_DEPTH_FIELD_NUMBER: _ClassVar[int]
    SHAPE_POLYGON_XY_FIELD_NUMBER: _ClassVar[int]
    SHAPE_POLYGON_Z_FIELD_NUMBER: _ClassVar[int]
    MODEL_2D_FIELD_NUMBER: _ClassVar[int]
    MODEL_3D_FIELD_NUMBER: _ClassVar[int]
    MODEL_JSON_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    wellshape_id: str
    data_extra: _containers.RepeatedScalarFieldContainer[str]
    name: str
    uri: str
    iri: str
    depth_well: float
    shape_well: str
    shape_bottom: str
    top_radius_xy: float
    bottom_radius_xy: float
    bottom_radius_z: float
    cone_angle: float
    cone_depth: float
    shape_polygon_xy: str
    shape_polygon_z: str
    model_2d: str
    model_3d: str
    model_json: _struct_pb2.Struct
    description: str
    def __init__(self, wellshape_id: _Optional[str] = ..., data_extra: _Optional[_Iterable[str]] = ..., name: _Optional[str] = ..., uri: _Optional[str] = ..., iri: _Optional[str] = ..., depth_well: _Optional[float] = ..., shape_well: _Optional[str] = ..., shape_bottom: _Optional[str] = ..., top_radius_xy: _Optional[float] = ..., bottom_radius_xy: _Optional[float] = ..., bottom_radius_z: _Optional[float] = ..., cone_angle: _Optional[float] = ..., cone_depth: _Optional[float] = ..., shape_polygon_xy: _Optional[str] = ..., shape_polygon_z: _Optional[str] = ..., model_2d: _Optional[str] = ..., model_3d: _Optional[str] = ..., model_json: _Optional[_Union[_struct_pb2.Struct, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class WellShapeRetrieveRequest(_message.Message):
    __slots__ = ("wellshape_id",)
    WELLSHAPE_ID_FIELD_NUMBER: _ClassVar[int]
    wellshape_id: str
    def __init__(self, wellshape_id: _Optional[str] = ...) -> None: ...

class WellShapeStreamRequest(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...
