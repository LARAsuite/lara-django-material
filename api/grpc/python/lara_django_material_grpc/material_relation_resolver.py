"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django material relation resolver *

:details: The relation resolver is used to resolve foreign keys, e.g. by name or IRI instead of an id.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

import logging
import grpc
from typing import Any, Union, List, Dict, Tuple, Optional

from lara_django_base_grpc.relation_resolver_interface import (
    RetrMethod,
    RelationResolverBase,
)

# from lara_django_base_grpc.lara_django_base_interface import AddressInterface
from lara_django_people_grpc.lara_django_people_interface import EntityInterface, EntityRoleInterface, EntityClassInterface
from lara_django_material_grpc.lara_django_material_interface import (
    PartDeviceClassInterface,
)

logger = logging.getLogger(__name__)


class PartRelationResolver(RelationResolverBase):
    """Relation Resolver for LARA part models"""

    def __init__(self, channel: grpc.Channel = None):
        try:
            self.partdevice_class_if = PartDeviceClassInterface(channel=channel)
        except Exception as e:
            logger.error(f"Error creating interfaces: {e}")

        self.relation_finder_dict = {
            "part_class": [
                RetrMethod(
                    method=self.partdevice_class_if.retrieve_id,
                    params={"name": "part_class"},
                ),
            ],
        }


class DeviceRelationResolver(RelationResolverBase):
    """Relation Resolver for LARA device models"""

    def __init__(self, channel: grpc.Channel = None):
        try:
            self.partdevice_class_if = PartDeviceClassInterface(channel=channel)
            self.entity_if = EntityInterface(channel=channel)
        except Exception as e:
            logger.error(f"Error creating interfaces: {e}")

        self.relation_finder_dict = {
            "device_class": [
                RetrMethod(
                    method=self.partdevice_class_if.retrieve_id,
                    params={"name": "device_class"},
                ),
            ],
            "manufacturer": [
                RetrMethod(
                    method=self.entity_if.retrieve_id,
                    params={"name": "manufacturer"},
                ),
                RetrMethod(
                    method=self.entity_if.retrieve_id,
                    params={"name_full": "manufacturer"},
                ),
                RetrMethod(
                    method=self.entity_if.retrieve_id,
                    params={"name_display": "manufacturer"},
                ),
            ],
        }


class LabwareRelationResolver(RelationResolverBase):
    """Relation Resolver for LARA labware models"""

    def __init__(self, channel: grpc.Channel = None):
        try:
            self.partdevice_class_if = PartDeviceClassInterface(channel=channel)
        except Exception as e:
            logger.error(f"Error creating interfaces: {e}")

        self.relation_finder_dict = {
            "labware_class": [
                RetrMethod(
                    method=self.partdevice_class_if.retrieve_id,
                    params={"name": "labware_class"},
                ),
            ],
        }
