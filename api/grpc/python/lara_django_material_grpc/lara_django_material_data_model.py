from __future__ import annotations 

import re
import sys
from datetime import (
    date,
    datetime,
    time
)
from decimal import Decimal 
from enum import Enum 
from typing import (
    Any,
    ClassVar,
    Dict,
    List,
    Literal,
    Optional,
    Union
)

from pydantic import (
    BaseModel,
    ConfigDict,
    Field,
    RootModel,
    field_validator
)


metamodel_version = "None"
version = "None"


class ConfiguredBaseModel(BaseModel):
    model_config = ConfigDict(
        validate_assignment = True,
        validate_default = True,
        extra = "forbid",
        arbitrary_types_allowed = True,
        use_enum_values = True,
        strict = False,
    )
    pass




class LinkMLMeta(RootModel):
    root: Dict[str, Any] = {}
    model_config = ConfigDict(frozen=True)

    def __getattr__(self, key:str):
        return getattr(self.root, key)

    def __getitem__(self, key:str):
        return self.root[key]

    def __setitem__(self, key:str, value):
        self.root[key] = value

    def __contains__(self, key:str) -> bool:
        return key in self.root


linkml_meta = LinkMLMeta({'default_prefix': 'lara',
     'default_range': 'string',
     'id': 'https://w3id.org/lara/lara_django_material',
     'imports': ['linkml:types'],
     'name': 'lara_django_material',
     'prefixes': {'lara': {'prefix_prefix': 'lara',
                           'prefix_reference': 'http://w3id.org/lara/'},
                  'linkml': {'prefix_prefix': 'linkml',
                             'prefix_reference': 'https://w3id.org/linkml/'},
                  'oso': {'prefix_prefix': 'oso',
                          'prefix_reference': 'http://w3id.org/oso/'}},
     'source_file': 'lara_django_material_schema.yaml',
     'types': {'FileField': {'base': 'str',
                             'description': 'A file field',
                             'from_schema': 'https://w3id.org/lara/lara_django_material',
                             'name': 'FileField',
                             'uri': 'https://w3id.org/lara/FileField'},
               'ImageField': {'base': 'str',
                              'description': 'An image field',
                              'from_schema': 'https://w3id.org/lara/lara_django_material',
                              'name': 'ImageField',
                              'uri': 'https://w3id.org/lara/ImageField'},
               'JSON': {'base': 'string',
                        'description': 'A JSON object',
                        'from_schema': 'https://w3id.org/lara/lara_django_material',
                        'name': 'JSON',
                        'repr': 'dict',
                        'uri': 'https://json.org'},
               'UUID': {'base': 'str',
                        'description': 'A UUID',
                        'from_schema': 'https://w3id.org/lara/lara_django_material',
                        'name': 'UUID',
                        'pattern': '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$',
                        'uri': 'https://datatracker.ietf.org/doc/html/rfc9562'}}} )


class MediaType(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.MediaType'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class ExternalResource(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.ExternalResource'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class Tag(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Tag'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class Group(ConfiguredBaseModel):
    """
    <class 'lara_django_people.models.Group'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class Entity(ConfiguredBaseModel):
    """
    <class 'lara_django_people.models.Entity'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class Namespace(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.Namespace'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class DataType(ConfiguredBaseModel):
    """
    <class 'lara_django_base.models.DataType'>
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material'})

    pass


class ExtraData(ConfiguredBaseModel):
    """
    \" This class can be used to extend material models by extra information,
        e.g., maintenance plan, maintenance history, incidents history,  image filename , ...
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/ExtraData',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    extradata_id: Optional[str] = Field(None, description="""ExtraData - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'extradata_id',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/extradata_id'} })
    data_type: Optional[str] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'data_type',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:base/DataType'} })
    namespace: str = Field(..., description="""namespace of data""", json_schema_extra = { "linkml_meta": {'alias': 'namespace',
         'domain_of': ['ExtraData',
                       'PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Namespace'} })
    name_display: Optional[str] = Field(None, description="""Human readable display name or title of extra data""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/ExtraData/name_display'} })
    name: Optional[str] = Field(None, description="""name of extra data""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/ExtraData/name'} })
    name_full: Optional[str] = Field(None, description="""full name of extra data""", json_schema_extra = { "linkml_meta": {'alias': 'name_full',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/name_full'} })
    version: Optional[str] = Field(None, description="""version of extra data""", json_schema_extra = { "linkml_meta": {'alias': 'version',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/version'} })
    hash_sha256: Optional[str] = Field(None, description="""SHA256 hash of all data (JSON and XML)""", json_schema_extra = { "linkml_meta": {'alias': 'hash_sha256',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/hash_sha256'} })
    data_json: Optional[dict] = Field(None, description="""generic JSON field""", json_schema_extra = { "linkml_meta": {'alias': 'data_json',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/data_json'} })
    media_type: Optional[str] = Field(None, description="""IANA media type of extra data file""", json_schema_extra = { "linkml_meta": {'alias': 'media_type',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:base/MediaType'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData/iri'} })
    url: Optional[str] = Field(None, description="""Universal Resource Locator - this can be used to link the data to external locations""", json_schema_extra = { "linkml_meta": {'alias': 'url',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/ExtraData/url'} })
    datetime_created: Optional[str] = Field(None, description="""date and time when data was created""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_created',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/datetime_created'} })
    datetime_last_modified: Optional[str] = Field(None, description="""date and time when data was last modified""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/ExtraData/datetime_last_modified'} })
    description: Optional[str] = Field(None, description="""description of extra data""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/ExtraData/description'} })
    image: Optional[str] = Field(None, description="""location room map rel. path/filename to image""", json_schema_extra = { "linkml_meta": {'alias': 'image',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/ExtraData/image'} })
    file: Optional[str] = Field(None, description="""rel. path/filename""", json_schema_extra = { "linkml_meta": {'alias': 'file',
         'domain_of': ['ExtraData'],
         'slot_uri': 'lara:material/ExtraData/file'} })


class PartDeviceClass(ConfiguredBaseModel):
    """
    \" classes for parts and device, e.g. screw, bolt, robot, spectrometer, HPLC, NMR, \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/PartDeviceClass',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    partdeviceclass_id: Optional[str] = Field(None, description="""PartDeviceClass - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'partdeviceclass_id',
         'domain_of': ['PartDeviceClass'],
         'slot_uri': 'lara:material/PartDeviceClass/partdeviceclass_id'} })
    name: str = Field(..., description="""name of the part/device class, like screw, bolt, robot, spectrometer, HPLC, NMR,""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartDeviceClass/name'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation in a related ontology""", json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/PartDeviceClass/iri'} })
    description: Optional[str] = Field(None, description="""description""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartDeviceClass/description'} })


class PartShape(ConfiguredBaseModel):
    """
    \" Part shape and dimensions \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/PartShape',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name: str = Field(..., description="""name of shape dimension set, e.g. SBS.96.round.fb""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartShape/name'} })
    name_standard: Optional[str] = Field(None, description="""underlying labware/shape standard, e.g. SBS""", json_schema_extra = { "linkml_meta": {'alias': 'name_standard',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/name_standard'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/PartShape/iri'} })
    width: Optional[float] = Field(None, description="""cont. total width, """, json_schema_extra = { "linkml_meta": {'alias': 'width',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/width'} })
    length: Optional[float] = Field(None, description="""cont. total length """, json_schema_extra = { "linkml_meta": {'alias': 'length',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/length'} })
    height_unlidded: Optional[float] = Field(None, description="""cont. height, without lid""", json_schema_extra = { "linkml_meta": {'alias': 'height_unlidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/height_unlidded'} })
    hight_lidded: Optional[float] = Field(None, description="""cont. height incl. lid""", json_schema_extra = { "linkml_meta": {'alias': 'hight_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/hight_lidded'} })
    hight_stacked: Optional[float] = Field(None, description="""stacking height, unlidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/hight_stacked'} })
    hight_stacked_lidded: Optional[float] = Field(None, description="""stacking height lidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/hight_stacked_lidded'} })
    model_2d: Optional[str] = Field(None, description="""2D model of part shape in, e.g., SVG format""", json_schema_extra = { "linkml_meta": {'alias': 'model_2d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/model_2d'} })
    model_3d: Optional[str] = Field(None, description="""3D model of part shape in, e.g., STL format""", json_schema_extra = { "linkml_meta": {'alias': 'model_3d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/model_3d'} })
    model_json: Optional[dict] = Field(None, description="""2D or 3D model of part shape in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'model_json',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/PartShape/model_json'} })
    description: Optional[str] = Field(None, description="""description of part shape""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartShape/description'} })
    partshape_id: Optional[str] = Field(None, description="""PartShape - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'partshape_id',
         'domain_of': ['PartShape'],
         'slot_uri': 'lara:material/PartShape/partshape_id'} })
    data_extra: Optional[List[str]] = Field(None, description="""part shape extra data, like 3D model or polygon describing the part shape/dimensions""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class Part(ConfiguredBaseModel):
    """
    \" generic Part class \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/Part',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name_display: Optional[str] = Field(None, description="""human readable name of the part/device/labware for the UI.""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Part/name_display'} })
    name: Optional[str] = Field(None, description="""short part or device name, should not contain whitespaces""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Part/name'} })
    url: Optional[str] = Field(None, description="""Universal Resource Locator - URL""", json_schema_extra = { "linkml_meta": {'alias': 'url',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/url'} })
    pid: Optional[str] = Field(None, description="""handle URI""", json_schema_extra = { "linkml_meta": {'alias': 'pid',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/pid'} })
    pac_id: Optional[str] = Field(None, description="""Publicly Addressable Content IDentifier is a globally unique identifier that operates independently of a central registry.""", json_schema_extra = { "linkml_meta": {'alias': 'pac_id',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/pac_id'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/Part/iri'} })
    manufacturer: Optional[str] = Field(None, description="""manufacturer of the part/device/labware""", json_schema_extra = { "linkml_meta": {'alias': 'manufacturer',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:people/Entity'} })
    model_no: Optional[str] = Field(None, description="""model or part number (of e.g. manufacturer)""", json_schema_extra = { "linkml_meta": {'alias': 'model_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/model_no'} })
    product_type: Optional[str] = Field(None, description="""(manufacturer) product type""", json_schema_extra = { "linkml_meta": {'alias': 'product_type',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/product_type'} })
    type_barcode: Optional[str] = Field(None, description="""barcode of the general part type, not of a specific part !!.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/type_barcode'} })
    type_barcode_json: Optional[dict] = Field(None, description="""Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/type_barcode_json'} })
    product_no: Optional[str] = Field(None, description="""manufacturer product number""", json_schema_extra = { "linkml_meta": {'alias': 'product_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/product_no'} })
    weight: Optional[float] = Field(None, description="""part net weight in kg""", json_schema_extra = { "linkml_meta": {'alias': 'weight',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/weight'} })
    energy_consumption: Optional[float] = Field(None, description="""energy consumption in W""", json_schema_extra = { "linkml_meta": {'alias': 'energy_consumption',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/energy_consumption'} })
    spec_json: Optional[dict] = Field(None, description="""part specifications in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'spec_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/spec_json'} })
    hardware_config: Optional[dict] = Field(None, description="""Configuration of the item hardware setup, like connection schemata, wiring, tubing, ...""", json_schema_extra = { "linkml_meta": {'alias': 'hardware_config',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/hardware_config'} })
    icon: Optional[str] = Field(None, description="""XML/SVG icon/logo/drawing of the part""", json_schema_extra = { "linkml_meta": {'alias': 'icon',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/icon'} })
    image: Optional[str] = Field(None, description="""rel. path/filename to image""", json_schema_extra = { "linkml_meta": {'alias': 'image',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part/image'} })
    datetime_last_modified: Optional[str] = Field(None, description="""date and time when data was last modified""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Part/datetime_last_modified'} })
    description: Optional[str] = Field(None, description="""description of the device""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Part/description'} })
    part_id: Optional[str] = Field(None, description="""Part - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'part_id',
         'domain_of': ['Part'],
         'slot_uri': 'lara:material/Part/part_id'} })
    part_class: Optional[str] = Field(None, description="""screw, bolt, nut, lamp, diode, resitor, IC, ...""", json_schema_extra = { "linkml_meta": {'alias': 'part_class',
         'domain_of': ['Part'],
         'slot_uri': 'lara:material/PartDeviceClass'} })
    shape: Optional[str] = Field(None, description="""shape of Part""", json_schema_extra = { "linkml_meta": {'alias': 'shape',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/PartShape'} })
    resources_external: Optional[List[str]] = Field(None, description="""external resources, like literature, websites, manuals, ...""", json_schema_extra = { "linkml_meta": {'alias': 'resources_external',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:base/ExternalResource'} })
    tags: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    components: Optional[List[str]] = Field(None, description="""components of the part""", json_schema_extra = { "linkml_meta": {'alias': 'components',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part'} })
    data_extra: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class PartsSubset(ConfiguredBaseModel):
    """
    \" Parts subset. It can be used for user or group defined subsets of parts. \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/PartsSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    namespace: Optional[str] = Field(None, description="""namespace of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'namespace',
         'domain_of': ['ExtraData',
                       'PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Namespace'} })
    name_display: Optional[str] = Field(None, description="""display name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartsSubset/name_display'} })
    name: Optional[str] = Field(None, description="""name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartsSubset/name'} })
    entity: Optional[str] = Field(None, description="""user who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'entity',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Entity'} })
    group: Optional[str] = Field(None, description="""group who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'group',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Group'} })
    description: Optional[str] = Field(None, description="""description of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartsSubset/description'} })
    datetime_last_modified: Optional[str] = Field(None, description="""datetime of last modification of the selection""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/PartsSubset/datetime_last_modified'} })
    partssubset_id: Optional[str] = Field(None, description="""PartsSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'partssubset_id',
         'domain_of': ['PartsSubset'],
         'slot_uri': 'lara:material/PartsSubset/partssubset_id'} })
    tags: Optional[List[str]] = Field(None, description="""tags""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    parts: Optional[List[str]] = Field(None, description="""parts in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'parts',
         'domain_of': ['PartsSubset', 'Container'],
         'slot_uri': 'lara:material/Part'} })


class OrderedPartsSubset(ConfiguredBaseModel):
    """
    \" Through model for the many-to-many relationship between PartsSubset and Part.
        This class can be used to store the order of parts in a subset
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/OrderedPartsSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    orderedpartssubset_id: Optional[str] = Field(None, description="""OrderedPartsSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'orderedpartssubset_id',
         'domain_of': ['OrderedPartsSubset'],
         'slot_uri': 'lara:material/OrderedPartsSubset/orderedpartssubset_id'} })
    part: str = Field(..., description="""part in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'part',
         'domain_of': ['OrderedPartsSubset'],
         'slot_uri': 'lara:material/Part'} })
    parts_subset: str = Field(..., description="""subset of parts""", json_schema_extra = { "linkml_meta": {'alias': 'parts_subset',
         'domain_of': ['OrderedPartsSubset'],
         'slot_uri': 'lara:material/PartsSubset'} })
    order: int = Field(..., description="""order of part in subset""", json_schema_extra = { "linkml_meta": {'alias': 'order',
         'domain_of': ['OrderedPartsSubset',
                       'OrderedDevicesSubset',
                       'OrderedDeviceSetupsSubset',
                       'OrderedLabwareSubset'],
         'slot_uri': 'lara:material/OrderedPartsSubset/order'} })


class DeviceShape(ConfiguredBaseModel):
    """
    \" Part shape and dimensions \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/DeviceShape',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name: str = Field(..., description="""name of shape dimension set, e.g. SBS.96.round.fb""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceShape/name'} })
    name_standard: Optional[str] = Field(None, description="""underlying labware/shape standard, e.g. SBS""", json_schema_extra = { "linkml_meta": {'alias': 'name_standard',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/name_standard'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/DeviceShape/iri'} })
    width: Optional[float] = Field(None, description="""cont. total width, """, json_schema_extra = { "linkml_meta": {'alias': 'width',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/width'} })
    length: Optional[float] = Field(None, description="""cont. total length """, json_schema_extra = { "linkml_meta": {'alias': 'length',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/length'} })
    height_unlidded: Optional[float] = Field(None, description="""cont. height, without lid""", json_schema_extra = { "linkml_meta": {'alias': 'height_unlidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/height_unlidded'} })
    hight_lidded: Optional[float] = Field(None, description="""cont. height incl. lid""", json_schema_extra = { "linkml_meta": {'alias': 'hight_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/hight_lidded'} })
    hight_stacked: Optional[float] = Field(None, description="""stacking height, unlidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/hight_stacked'} })
    hight_stacked_lidded: Optional[float] = Field(None, description="""stacking height lidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/hight_stacked_lidded'} })
    model_2d: Optional[str] = Field(None, description="""2D model of part shape in, e.g., SVG format""", json_schema_extra = { "linkml_meta": {'alias': 'model_2d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/model_2d'} })
    model_3d: Optional[str] = Field(None, description="""3D model of part shape in, e.g., STL format""", json_schema_extra = { "linkml_meta": {'alias': 'model_3d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/model_3d'} })
    model_json: Optional[dict] = Field(None, description="""2D or 3D model of part shape in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'model_json',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/DeviceShape/model_json'} })
    description: Optional[str] = Field(None, description="""description of part shape""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceShape/description'} })
    deviceshape_id: Optional[str] = Field(None, description="""DeviceShape - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'deviceshape_id',
         'domain_of': ['DeviceShape'],
         'slot_uri': 'lara:material/DeviceShape/deviceshape_id'} })
    data_extra: Optional[List[str]] = Field(None, description="""device shape extra data, like 3D model or polygon describing the device shape/dimensions""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class Device(ConfiguredBaseModel):
    """
    \" generic device class \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/Device',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name_display: Optional[str] = Field(None, description="""human readable name of the part/device/labware for the UI.""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Device/name_display'} })
    name: Optional[str] = Field(None, description="""short part or device name, should not contain whitespaces""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Device/name'} })
    url: Optional[str] = Field(None, description="""Universal Resource Locator - URL""", json_schema_extra = { "linkml_meta": {'alias': 'url',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/url'} })
    pid: Optional[str] = Field(None, description="""handle URI""", json_schema_extra = { "linkml_meta": {'alias': 'pid',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/pid'} })
    pac_id: Optional[str] = Field(None, description="""Publicly Addressable Content IDentifier is a globally unique identifier that operates independently of a central registry.""", json_schema_extra = { "linkml_meta": {'alias': 'pac_id',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/pac_id'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/Device/iri'} })
    manufacturer: Optional[str] = Field(None, description="""manufacturer of the part/device/labware""", json_schema_extra = { "linkml_meta": {'alias': 'manufacturer',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:people/Entity'} })
    model_no: Optional[str] = Field(None, description="""model or part number (of e.g. manufacturer)""", json_schema_extra = { "linkml_meta": {'alias': 'model_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/model_no'} })
    product_type: Optional[str] = Field(None, description="""(manufacturer) product type""", json_schema_extra = { "linkml_meta": {'alias': 'product_type',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/product_type'} })
    type_barcode: Optional[str] = Field(None, description="""barcode of the general part type, not of a specific part !!.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/type_barcode'} })
    type_barcode_json: Optional[dict] = Field(None, description="""Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/type_barcode_json'} })
    product_no: Optional[str] = Field(None, description="""manufacturer product number""", json_schema_extra = { "linkml_meta": {'alias': 'product_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/product_no'} })
    weight: Optional[float] = Field(None, description="""part net weight in kg""", json_schema_extra = { "linkml_meta": {'alias': 'weight',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/weight'} })
    energy_consumption: Optional[float] = Field(None, description="""energy consumption in W""", json_schema_extra = { "linkml_meta": {'alias': 'energy_consumption',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/energy_consumption'} })
    spec_json: Optional[dict] = Field(None, description="""part specifications in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'spec_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/spec_json'} })
    hardware_config: Optional[dict] = Field(None, description="""Configuration of the item hardware setup, like connection schemata, wiring, tubing, ...""", json_schema_extra = { "linkml_meta": {'alias': 'hardware_config',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/hardware_config'} })
    icon: Optional[str] = Field(None, description="""XML/SVG icon/logo/drawing of the part""", json_schema_extra = { "linkml_meta": {'alias': 'icon',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/icon'} })
    image: Optional[str] = Field(None, description="""rel. path/filename to image""", json_schema_extra = { "linkml_meta": {'alias': 'image',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device/image'} })
    datetime_last_modified: Optional[str] = Field(None, description="""date and time when data was last modified""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Device/datetime_last_modified'} })
    description: Optional[str] = Field(None, description="""description of the device""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Device/description'} })
    device_id: Optional[str] = Field(None, description="""Device - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'device_id',
         'domain_of': ['Device'],
         'slot_uri': 'lara:material/Device/device_id'} })
    device_class: Optional[str] = Field(None, description="""UV-spectrometer, MALDI-TOF-MS, HPLC, ..""", json_schema_extra = { "linkml_meta": {'alias': 'device_class',
         'domain_of': ['Device'],
         'slot_uri': 'lara:material/PartDeviceClass'} })
    labware_min_capacity: Optional[int] = Field(None, description="""labware capacity, min. number of labware, some centrifuges need at least 2 labware, e.g. for balancing""", json_schema_extra = { "linkml_meta": {'alias': 'labware_min_capacity',
         'domain_of': ['Device'],
         'slot_uri': 'lara:material/Device/labware_min_capacity'} })
    labware_max_capacity: Optional[int] = Field(None, description="""labware capacity, max. number of labware""", json_schema_extra = { "linkml_meta": {'alias': 'labware_max_capacity',
         'domain_of': ['Device'],
         'slot_uri': 'lara:material/Device/labware_max_capacity'} })
    labware_max_height: Optional[float] = Field(None, description="""max. labware height""", json_schema_extra = { "linkml_meta": {'alias': 'labware_max_height',
         'domain_of': ['Device'],
         'slot_uri': 'lara:material/Device/labware_max_height'} })
    labware_storage_layout: Optional[dict] = Field(None, description="""labware storage layout, like num. of stackers, num. of labware per stacker, orientation of nests, heights, ... """, json_schema_extra = { "linkml_meta": {'alias': 'labware_storage_layout',
         'domain_of': ['Device'],
         'slot_uri': 'lara:material/Device/labware_storage_layout'} })
    shape: Optional[str] = Field(None, description="""shape of Device""", json_schema_extra = { "linkml_meta": {'alias': 'shape',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceShape'} })
    resources_external: Optional[List[str]] = Field(None, description="""external resources, like literature, websites, manuals, ...""", json_schema_extra = { "linkml_meta": {'alias': 'resources_external',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:base/ExternalResource'} })
    tags: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    components: Optional[List[str]] = Field(None, description="""components of the device setup""", json_schema_extra = { "linkml_meta": {'alias': 'components',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part'} })
    data_extra: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class DevicesSubset(ConfiguredBaseModel):
    """
    \" Devices subset. It can be used for user or group defined subsets of devices. \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/DevicesSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    namespace: Optional[str] = Field(None, description="""namespace of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'namespace',
         'domain_of': ['ExtraData',
                       'PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Namespace'} })
    name_display: Optional[str] = Field(None, description="""display name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DevicesSubset/name_display'} })
    name: Optional[str] = Field(None, description="""name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DevicesSubset/name'} })
    entity: Optional[str] = Field(None, description="""user who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'entity',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Entity'} })
    group: Optional[str] = Field(None, description="""group who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'group',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Group'} })
    description: Optional[str] = Field(None, description="""description of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DevicesSubset/description'} })
    datetime_last_modified: Optional[str] = Field(None, description="""datetime of last modification of the selection""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DevicesSubset/datetime_last_modified'} })
    devicessubset_id: Optional[str] = Field(None, description="""DevicesSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'devicessubset_id',
         'domain_of': ['DevicesSubset'],
         'slot_uri': 'lara:material/DevicesSubset/devicessubset_id'} })
    tags: Optional[List[str]] = Field(None, description="""tags""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    devices: Optional[List[str]] = Field(None, description="""devices in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'devices',
         'domain_of': ['DevicesSubset', 'Container'],
         'slot_uri': 'lara:material/Device'} })


class OrderedDevicesSubset(ConfiguredBaseModel):
    """
    \" Through model for the many-to-many relationship between DevicesSubset and Device.
        This class can be used to store the order of devices in a subset
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/OrderedDevicesSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    ordereddevicessubset_id: Optional[str] = Field(None, description="""OrderedDevicesSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'ordereddevicessubset_id',
         'domain_of': ['OrderedDevicesSubset', 'OrderedDeviceSetupsSubset'],
         'slot_uri': 'lara:material/OrderedDevicesSubset/ordereddevicessubset_id'} })
    device: str = Field(..., description="""device in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'device',
         'domain_of': ['OrderedDevicesSubset'],
         'slot_uri': 'lara:material/Device'} })
    devices_subset: str = Field(..., description="""subset of devices""", json_schema_extra = { "linkml_meta": {'alias': 'devices_subset',
         'domain_of': ['OrderedDevicesSubset'],
         'slot_uri': 'lara:material/DevicesSubset'} })
    order: int = Field(..., description="""order of device in subset""", json_schema_extra = { "linkml_meta": {'alias': 'order',
         'domain_of': ['OrderedPartsSubset',
                       'OrderedDevicesSubset',
                       'OrderedDeviceSetupsSubset',
                       'OrderedLabwareSubset'],
         'slot_uri': 'lara:material/OrderedDevicesSubset/order'} })


class DeviceSetupShape(ConfiguredBaseModel):
    """
    \" Part shape and dimensions \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/DeviceSetupShape',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name: str = Field(..., description="""name of shape dimension set, e.g. SBS.96.round.fb""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetupShape/name'} })
    name_standard: Optional[str] = Field(None, description="""underlying labware/shape standard, e.g. SBS""", json_schema_extra = { "linkml_meta": {'alias': 'name_standard',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/name_standard'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/DeviceSetupShape/iri'} })
    width: Optional[float] = Field(None, description="""cont. total width, """, json_schema_extra = { "linkml_meta": {'alias': 'width',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/width'} })
    length: Optional[float] = Field(None, description="""cont. total length """, json_schema_extra = { "linkml_meta": {'alias': 'length',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/length'} })
    height_unlidded: Optional[float] = Field(None, description="""cont. height, without lid""", json_schema_extra = { "linkml_meta": {'alias': 'height_unlidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/height_unlidded'} })
    hight_lidded: Optional[float] = Field(None, description="""cont. height incl. lid""", json_schema_extra = { "linkml_meta": {'alias': 'hight_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/hight_lidded'} })
    hight_stacked: Optional[float] = Field(None, description="""stacking height, unlidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/hight_stacked'} })
    hight_stacked_lidded: Optional[float] = Field(None, description="""stacking height lidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/hight_stacked_lidded'} })
    model_2d: Optional[str] = Field(None, description="""2D model of part shape in, e.g., SVG format""", json_schema_extra = { "linkml_meta": {'alias': 'model_2d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/model_2d'} })
    model_3d: Optional[str] = Field(None, description="""3D model of part shape in, e.g., STL format""", json_schema_extra = { "linkml_meta": {'alias': 'model_3d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/model_3d'} })
    model_json: Optional[dict] = Field(None, description="""2D or 3D model of part shape in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'model_json',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/model_json'} })
    description: Optional[str] = Field(None, description="""description of part shape""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetupShape/description'} })
    devicesetupshape_id: Optional[str] = Field(None, description="""DeviceSetupShape - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'devicesetupshape_id',
         'domain_of': ['DeviceSetupShape'],
         'slot_uri': 'lara:material/DeviceSetupShape/devicesetupshape_id'} })
    data_extra: Optional[List[str]] = Field(None, description="""part shape extra data, like 3D model or polygon describing the part shape/dimensions""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class DeviceSetup(ConfiguredBaseModel):
    """
    \" Setup or assembly of devices, e.g. setup of a physical or chemical experiment:
           distillation apparatus, flow reactor, ultra-high vacuum depositor, ... with all connectivities_

        :param PartAbstr: _description_
        :type PartAbstr: _type_
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/DeviceSetup',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name_display: Optional[str] = Field(None, description="""human readable name of the part/device/labware for the UI.""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetup/name_display'} })
    name: Optional[str] = Field(None, description="""short part or device name, should not contain whitespaces""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetup/name'} })
    url: Optional[str] = Field(None, description="""Universal Resource Locator - URL""", json_schema_extra = { "linkml_meta": {'alias': 'url',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/url'} })
    pid: Optional[str] = Field(None, description="""handle URI""", json_schema_extra = { "linkml_meta": {'alias': 'pid',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/pid'} })
    pac_id: Optional[str] = Field(None, description="""Publicly Addressable Content IDentifier is a globally unique identifier that operates independently of a central registry.""", json_schema_extra = { "linkml_meta": {'alias': 'pac_id',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/pac_id'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/iri'} })
    manufacturer: Optional[str] = Field(None, description="""manufacturer of the part/device/labware""", json_schema_extra = { "linkml_meta": {'alias': 'manufacturer',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:people/Entity'} })
    model_no: Optional[str] = Field(None, description="""model or part number (of e.g. manufacturer)""", json_schema_extra = { "linkml_meta": {'alias': 'model_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/model_no'} })
    product_type: Optional[str] = Field(None, description="""(manufacturer) product type""", json_schema_extra = { "linkml_meta": {'alias': 'product_type',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/product_type'} })
    type_barcode: Optional[str] = Field(None, description="""barcode of the general part type, not of a specific part !!.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/type_barcode'} })
    type_barcode_json: Optional[dict] = Field(None, description="""Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/type_barcode_json'} })
    product_no: Optional[str] = Field(None, description="""manufacturer product number""", json_schema_extra = { "linkml_meta": {'alias': 'product_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/product_no'} })
    weight: Optional[float] = Field(None, description="""part net weight in kg""", json_schema_extra = { "linkml_meta": {'alias': 'weight',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/weight'} })
    energy_consumption: Optional[float] = Field(None, description="""energy consumption in W""", json_schema_extra = { "linkml_meta": {'alias': 'energy_consumption',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/energy_consumption'} })
    spec_json: Optional[dict] = Field(None, description="""part specifications in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'spec_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/spec_json'} })
    hardware_config: Optional[dict] = Field(None, description="""Configuration of the item hardware setup, like connection schemata, wiring, tubing, ...""", json_schema_extra = { "linkml_meta": {'alias': 'hardware_config',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/hardware_config'} })
    icon: Optional[str] = Field(None, description="""XML/SVG icon/logo/drawing of the part""", json_schema_extra = { "linkml_meta": {'alias': 'icon',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/icon'} })
    image: Optional[str] = Field(None, description="""rel. path/filename to image""", json_schema_extra = { "linkml_meta": {'alias': 'image',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetup/image'} })
    datetime_last_modified: Optional[str] = Field(None, description="""date and time when data was last modified""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetup/datetime_last_modified'} })
    description: Optional[str] = Field(None, description="""description of the device""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetup/description'} })
    devicesetup_id: Optional[str] = Field(None, description="""DeviceSetup - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'devicesetup_id',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/DeviceSetup/devicesetup_id'} })
    setup_class: Optional[str] = Field(None, description="""screw, bolt, nut, lamp, diode, resistor, IC, ...""", json_schema_extra = { "linkml_meta": {'alias': 'setup_class',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/PartDeviceClass'} })
    shape: Optional[str] = Field(None, description="""shape of Device setup""", json_schema_extra = { "linkml_meta": {'alias': 'shape',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/DeviceSetupShape'} })
    blueprint_image: Optional[str] = Field(None, description="""rel. path/filename to image""", json_schema_extra = { "linkml_meta": {'alias': 'blueprint_image',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/DeviceSetup/blueprint_image'} })
    blueprint_media_type: Optional[str] = Field(None, description="""file type of extra data file""", json_schema_extra = { "linkml_meta": {'alias': 'blueprint_media_type',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:base/MediaType'} })
    blueprint_file: Optional[str] = Field(None, description="""blueprint file""", json_schema_extra = { "linkml_meta": {'alias': 'blueprint_file',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/DeviceSetup/blueprint_file'} })
    blueprint_pdf: Optional[str] = Field(None, description="""blueprint PDF file""", json_schema_extra = { "linkml_meta": {'alias': 'blueprint_pdf',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/DeviceSetup/blueprint_pdf'} })
    scheme_svg: Optional[str] = Field(None, description="""setup scheme in SVG""", json_schema_extra = { "linkml_meta": {'alias': 'scheme_svg',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/DeviceSetup/scheme_svg'} })
    assembly: Optional[dict] = Field(None, description="""assembly with connections in JSON""", json_schema_extra = { "linkml_meta": {'alias': 'assembly',
         'domain_of': ['DeviceSetup'],
         'slot_uri': 'lara:material/DeviceSetup/assembly'} })
    resources_external: Optional[List[str]] = Field(None, description="""external resources, like literature, websites, manuals, ...""", json_schema_extra = { "linkml_meta": {'alias': 'resources_external',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:base/ExternalResource'} })
    tags: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    components: Optional[List[str]] = Field(None, description="""components of the device setup""", json_schema_extra = { "linkml_meta": {'alias': 'components',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Device'} })
    data_extra: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class DeviceSetupsSubset(ConfiguredBaseModel):
    """
    \" DeviceSetups subset. It can be used for user or group defined subsets of device setups. \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/DeviceSetupsSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    namespace: Optional[str] = Field(None, description="""namespace of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'namespace',
         'domain_of': ['ExtraData',
                       'PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Namespace'} })
    name_display: Optional[str] = Field(None, description="""display name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetupsSubset/name_display'} })
    name: Optional[str] = Field(None, description="""name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetupsSubset/name'} })
    entity: Optional[str] = Field(None, description="""user who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'entity',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Entity'} })
    group: Optional[str] = Field(None, description="""group who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'group',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Group'} })
    description: Optional[str] = Field(None, description="""description of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetupsSubset/description'} })
    datetime_last_modified: Optional[str] = Field(None, description="""datetime of last modification of the selection""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/DeviceSetupsSubset/datetime_last_modified'} })
    devicesetupssubset_id: Optional[str] = Field(None, description="""DeviceSetupsSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'devicesetupssubset_id',
         'domain_of': ['DeviceSetupsSubset'],
         'slot_uri': 'lara:material/DeviceSetupsSubset/devicesetupssubset_id'} })
    tags: Optional[List[str]] = Field(None, description="""tags""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    devicesetups: Optional[List[str]] = Field(None, description="""device setups in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'devicesetups',
         'domain_of': ['DeviceSetupsSubset', 'Container'],
         'slot_uri': 'lara:material/DeviceSetup'} })


class OrderedDeviceSetupsSubset(ConfiguredBaseModel):
    """
    \" Through model for the many-to-many relationship between DeviceSetupsSubset and DeviceSetup.
        This class can be used to store the order of device setups in a subset
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/OrderedDeviceSetupsSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    ordereddevicessubset_id: Optional[str] = Field(None, description="""OrderedDeviceSetupsSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'ordereddevicessubset_id',
         'domain_of': ['OrderedDevicesSubset', 'OrderedDeviceSetupsSubset'],
         'slot_uri': 'lara:material/OrderedDeviceSetupsSubset/ordereddevicessubset_id'} })
    devicesetup: str = Field(..., description="""device setup in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'devicesetup',
         'domain_of': ['OrderedDeviceSetupsSubset'],
         'slot_uri': 'lara:material/DeviceSetup'} })
    devicesetups_subset: str = Field(..., description="""subset of device setups""", json_schema_extra = { "linkml_meta": {'alias': 'devicesetups_subset',
         'domain_of': ['OrderedDeviceSetupsSubset'],
         'slot_uri': 'lara:material/DeviceSetupsSubset'} })
    order: int = Field(..., description="""order of device setup in subset""", json_schema_extra = { "linkml_meta": {'alias': 'order',
         'domain_of': ['OrderedPartsSubset',
                       'OrderedDevicesSubset',
                       'OrderedDeviceSetupsSubset',
                       'OrderedLabwareSubset'],
         'slot_uri': 'lara:material/OrderedDeviceSetupsSubset/order'} })


class LabwareClass(ConfiguredBaseModel):
    """
    \" Labware classes, e.g., microtiterplate / MTP, tube, flask, ... \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/LabwareClass',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    labwareclass_id: Optional[str] = Field(None, description="""LabwareClass - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'labwareclass_id',
         'domain_of': ['LabwareClass'],
         'slot_uri': 'lara:material/LabwareClass/labwareclass_id'} })
    name: str = Field(..., json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareClass/name'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/LabwareClass/iri'} })
    description: Optional[str] = Field(None, description="""description of the labware class""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareClass/description'} })


class WellShape(ConfiguredBaseModel):
    """
    \" Labware Well Shape: describing shapes of wells, round, square, buffeled, flowered,...
        this can also be used to describe the bottom shape, like flat, conical
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/WellShape',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    wellshape_id: Optional[str] = Field(None, description="""WellShape - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'wellshape_id',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/wellshape_id'} })
    name: str = Field(..., description="""Well shape name""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/WellShape/name'} })
    uri: Optional[str] = Field(None, description="""Universal Resource Indentifier - URI - can be used to identify the shape """, json_schema_extra = { "linkml_meta": {'alias': 'uri',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/uri'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/WellShape/iri'} })
    depth_well: Optional[float] = Field(None, description="""total well depth=hight""", json_schema_extra = { "linkml_meta": {'alias': 'depth_well',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/depth_well'} })
    shape_well: str = Field(..., description="""labware well shape,e.g. round, square, buffeled,...""", json_schema_extra = { "linkml_meta": {'alias': 'shape_well',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/shape_well'} })
    shape_bottom: str = Field(..., description="""well, bottom shape, flat, round, conical""", json_schema_extra = { "linkml_meta": {'alias': 'shape_bottom',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/shape_bottom'} })
    top_radius_xy: Optional[float] = Field(None, description="""radius of a round well at the top opening""", json_schema_extra = { "linkml_meta": {'alias': 'top_radius_xy',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/top_radius_xy'} })
    bottom_radius_xy: Optional[float] = Field(None, description="""radius of a round bottom in xy direction""", json_schema_extra = { "linkml_meta": {'alias': 'bottom_radius_xy',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/bottom_radius_xy'} })
    bottom_radius_z: Optional[float] = Field(None, description="""radius of a round bottom in z (hight) direction""", json_schema_extra = { "linkml_meta": {'alias': 'bottom_radius_z',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/bottom_radius_z'} })
    cone_angle: Optional[float] = Field(None, description="""opening angle of cone in deg""", json_schema_extra = { "linkml_meta": {'alias': 'cone_angle',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/cone_angle'} })
    cone_depth: Optional[float] = Field(None, description="""opening angle of cone in deg""", json_schema_extra = { "linkml_meta": {'alias': 'cone_depth',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/cone_depth'} })
    shape_polygon_xy: str = Field(..., description="""generalized shape polygon for more complex well shapes, in xy direction""", json_schema_extra = { "linkml_meta": {'alias': 'shape_polygon_xy',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/shape_polygon_xy'} })
    shape_polygon_z: str = Field(..., description="""generalized shape polygon for more complex well shapes, in z direction""", json_schema_extra = { "linkml_meta": {'alias': 'shape_polygon_z',
         'domain_of': ['WellShape'],
         'slot_uri': 'lara:material/WellShape/shape_polygon_z'} })
    model_2d: Optional[str] = Field(None, description="""2D model of well shape in, e.g., SVG format""", json_schema_extra = { "linkml_meta": {'alias': 'model_2d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/WellShape/model_2d'} })
    model_3d: Optional[str] = Field(None, description="""3D model of well shape in, e.g., STL format""", json_schema_extra = { "linkml_meta": {'alias': 'model_3d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/WellShape/model_3d'} })
    model_json: Optional[dict] = Field(None, description="""2D or 3D model of Well shape""", json_schema_extra = { "linkml_meta": {'alias': 'model_json',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/WellShape/model_json'} })
    description: Optional[str] = Field(None, description="""description of labware shape""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/WellShape/description'} })
    data_extra: Optional[List[str]] = Field(None, description="""well shape extra data, like 3D model or polygon describing the well shape/dimensions""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class LabwareShape(ConfiguredBaseModel):
    """
    \" Labware and Lid shape and dimensions:
        e.g. special characteristics of a labware, like material, shape, well-shape ...
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/LabwareShape',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name: str = Field(..., description="""name of shape dimension set, e.g. SBS.96.round.fb""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareShape/name'} })
    name_standard: Optional[str] = Field(None, description="""underlying labware/shape standard, e.g. SBS""", json_schema_extra = { "linkml_meta": {'alias': 'name_standard',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/name_standard'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/LabwareShape/iri'} })
    width: Optional[float] = Field(None, description="""cont. total width, """, json_schema_extra = { "linkml_meta": {'alias': 'width',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/width'} })
    length: Optional[float] = Field(None, description="""cont. total length """, json_schema_extra = { "linkml_meta": {'alias': 'length',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/length'} })
    height_unlidded: Optional[float] = Field(None, description="""cont. height, without lid""", json_schema_extra = { "linkml_meta": {'alias': 'height_unlidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/height_unlidded'} })
    hight_lidded: Optional[float] = Field(None, description="""cont. height incl. lid""", json_schema_extra = { "linkml_meta": {'alias': 'hight_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/hight_lidded'} })
    hight_stacked: Optional[float] = Field(None, description="""stacking height, unlidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/hight_stacked'} })
    hight_stacked_lidded: Optional[float] = Field(None, description="""stacking height lidded""", json_schema_extra = { "linkml_meta": {'alias': 'hight_stacked_lidded',
         'domain_of': ['PartShape', 'DeviceShape', 'DeviceSetupShape', 'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/hight_stacked_lidded'} })
    model_2d: Optional[str] = Field(None, description="""2D model of part shape in, e.g., SVG format""", json_schema_extra = { "linkml_meta": {'alias': 'model_2d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/model_2d'} })
    model_3d: Optional[str] = Field(None, description="""3D model of part shape in, e.g., STL format""", json_schema_extra = { "linkml_meta": {'alias': 'model_3d',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/model_3d'} })
    model_json: Optional[dict] = Field(None, description="""2D or 3D model of part shape in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'model_json',
         'domain_of': ['PartShape',
                       'DeviceShape',
                       'DeviceSetupShape',
                       'WellShape',
                       'LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/model_json'} })
    labwareshape_id: Optional[str] = Field(None, description="""LabwareShape - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'labwareshape_id',
         'domain_of': ['LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/labwareshape_id'} })
    well_dist_row: Optional[float] = Field(None, description="""well-to-well distance in row direction""", json_schema_extra = { "linkml_meta": {'alias': 'well_dist_row',
         'domain_of': ['LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/well_dist_row'} })
    well_dist_col: Optional[float] = Field(None, description="""well-to-well distance in column direction""", json_schema_extra = { "linkml_meta": {'alias': 'well_dist_col',
         'domain_of': ['LabwareShape'],
         'slot_uri': 'lara:material/LabwareShape/well_dist_col'} })
    well_shape: Optional[str] = Field(None, description="""Well shape description""", json_schema_extra = { "linkml_meta": {'alias': 'well_shape',
         'domain_of': ['LabwareShape'],
         'slot_uri': 'lara:material/WellShape'} })
    description: Optional[str] = Field(None, description="""description of labware shape""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareShape/description'} })
    data_extra: Optional[List[str]] = Field(None, description="""labware shape extra data, like 3D model or polygon describing the labware shape/dimensions""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class Labware(ConfiguredBaseModel):
    """
    \" generic labware class modelling/describing the general properties of a labware
        like geometry, volume, manufacturer.
        All concrete instances shall appear in the labware store
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/Labware',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    name_display: Optional[str] = Field(None, description="""human readable name of the part/device/labware for the UI.""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Labware/name_display'} })
    name: Optional[str] = Field(None, description="""short part or device name, should not contain whitespaces""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Labware/name'} })
    url: Optional[str] = Field(None, description="""Universal Resource Locator - URL""", json_schema_extra = { "linkml_meta": {'alias': 'url',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/url'} })
    pid: Optional[str] = Field(None, description="""handle URI""", json_schema_extra = { "linkml_meta": {'alias': 'pid',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/pid'} })
    pac_id: Optional[str] = Field(None, description="""Publicly Addressable Content IDentifier is a globally unique identifier that operates independently of a central registry.""", json_schema_extra = { "linkml_meta": {'alias': 'pac_id',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/pac_id'} })
    iri: Optional[str] = Field(None, description="""International Resource Identifier - IRI: is used for semantic representation """, json_schema_extra = { "linkml_meta": {'alias': 'iri',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/Labware/iri'} })
    manufacturer: Optional[str] = Field(None, description="""manufacturer of the part/device/labware""", json_schema_extra = { "linkml_meta": {'alias': 'manufacturer',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:people/Entity'} })
    model_no: Optional[str] = Field(None, description="""model or part number (of e.g. manufacturer)""", json_schema_extra = { "linkml_meta": {'alias': 'model_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/model_no'} })
    product_type: Optional[str] = Field(None, description="""(manufacturer) product type""", json_schema_extra = { "linkml_meta": {'alias': 'product_type',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/product_type'} })
    type_barcode: Optional[str] = Field(None, description="""barcode of the general part type, not of a specific part !!.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/type_barcode'} })
    type_barcode_json: Optional[dict] = Field(None, description="""Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.""", json_schema_extra = { "linkml_meta": {'alias': 'type_barcode_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/type_barcode_json'} })
    product_no: Optional[str] = Field(None, description="""manufacturer product number""", json_schema_extra = { "linkml_meta": {'alias': 'product_no',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/product_no'} })
    weight: Optional[float] = Field(None, description="""part net weight in kg""", json_schema_extra = { "linkml_meta": {'alias': 'weight',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/weight'} })
    energy_consumption: Optional[float] = Field(None, description="""energy consumption in W""", json_schema_extra = { "linkml_meta": {'alias': 'energy_consumption',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/energy_consumption'} })
    spec_json: Optional[dict] = Field(None, description="""part specifications in JSON format""", json_schema_extra = { "linkml_meta": {'alias': 'spec_json',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/spec_json'} })
    hardware_config: Optional[dict] = Field(None, description="""Configuration of the item hardware setup, like connection schemata, wiring, tubing, ...""", json_schema_extra = { "linkml_meta": {'alias': 'hardware_config',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/hardware_config'} })
    icon: Optional[str] = Field(None, description="""XML/SVG icon/logo/drawing of the part""", json_schema_extra = { "linkml_meta": {'alias': 'icon',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/icon'} })
    image: Optional[str] = Field(None, description="""rel. path/filename to image""", json_schema_extra = { "linkml_meta": {'alias': 'image',
         'domain_of': ['ExtraData', 'Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Labware/image'} })
    datetime_last_modified: Optional[str] = Field(None, description="""date and time when data was last modified""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Labware/datetime_last_modified'} })
    description: Optional[str] = Field(None, description="""description of the device""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/Labware/description'} })
    labware_id: Optional[str] = Field(None, description="""Labware - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'labware_id',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/labware_id'} })
    labware_class: Optional[str] = Field(None, description="""class of the labware, e.g. MTP-SBS/SBS, vial/HPLC,  ...""", json_schema_extra = { "linkml_meta": {'alias': 'labware_class',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/LabwareClass'} })
    num_rows: Optional[int] = Field(None, description="""number of rows of labware""", json_schema_extra = { "linkml_meta": {'alias': 'num_rows',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/num_rows'} })
    num_cols: Optional[int] = Field(None, description="""number of columns of labware""", json_schema_extra = { "linkml_meta": {'alias': 'num_cols',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/num_cols'} })
    num_wells: Optional[int] = Field(None, description="""number of wells - could be auto generated""", json_schema_extra = { "linkml_meta": {'alias': 'num_wells',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/num_wells'} })
    height: Optional[float] = Field(None, description="""height of labware in mm""", json_schema_extra = { "linkml_meta": {'alias': 'height',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/height'} })
    shape: Optional[str] = Field(None, description="""shape of labware""", json_schema_extra = { "linkml_meta": {'alias': 'shape',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/LabwareShape'} })
    liddable: Optional[bool] = Field(None, description="""labware is liddable""", json_schema_extra = { "linkml_meta": {'alias': 'liddable',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/liddable'} })
    sealable: bool = Field(..., description="""labware is sealable""", json_schema_extra = { "linkml_meta": {'alias': 'sealable',
         'domain_of': ['Labware'],
         'slot_uri': 'lara:material/Labware/sealable'} })
    resources_external: Optional[List[str]] = Field(None, description="""external resources, like literature, websites, manuals, ...""", json_schema_extra = { "linkml_meta": {'alias': 'resources_external',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:base/ExternalResource'} })
    tags: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    components: Optional[List[str]] = Field(None, description="""components of the labware""", json_schema_extra = { "linkml_meta": {'alias': 'components',
         'domain_of': ['Part', 'Device', 'DeviceSetup', 'Labware'],
         'slot_uri': 'lara:material/Part'} })
    lids: Optional[List[str]] = Field(None, description="""reference to possible lids""", json_schema_extra = { "linkml_meta": {'alias': 'lids', 'domain_of': ['Labware'], 'slot_uri': 'lara:material/Labware'} })
    data_extra: Optional[List[str]] = Field(None, description="""e.g. manual""", json_schema_extra = { "linkml_meta": {'alias': 'data_extra',
         'domain_of': ['PartShape',
                       'Part',
                       'DeviceShape',
                       'Device',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'WellShape',
                       'LabwareShape',
                       'Labware'],
         'slot_uri': 'lara:material/ExtraData'} })


class LabwareSubset(ConfiguredBaseModel):
    """
    \" Labware subset. It can be used for user or group defined subsets of labware. \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/LabwareSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    namespace: Optional[str] = Field(None, description="""namespace of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'namespace',
         'domain_of': ['ExtraData',
                       'PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Namespace'} })
    name_display: Optional[str] = Field(None, description="""display name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name_display',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareSubset/name_display'} })
    name: Optional[str] = Field(None, description="""name of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'name',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareSubset/name'} })
    entity: Optional[str] = Field(None, description="""user who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'entity',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Entity'} })
    group: Optional[str] = Field(None, description="""group who created the subset""", json_schema_extra = { "linkml_meta": {'alias': 'group',
         'domain_of': ['PartsSubset',
                       'DevicesSubset',
                       'DeviceSetupsSubset',
                       'LabwareSubset'],
         'slot_uri': 'lara:people/Group'} })
    description: Optional[str] = Field(None, description="""description of the subset""", json_schema_extra = { "linkml_meta": {'alias': 'description',
         'domain_of': ['ExtraData',
                       'PartDeviceClass',
                       'PartShape',
                       'Part',
                       'PartsSubset',
                       'DeviceShape',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetupShape',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'LabwareClass',
                       'WellShape',
                       'LabwareShape',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareSubset/description'} })
    datetime_last_modified: Optional[str] = Field(None, description="""datetime of last modification of the selection""", json_schema_extra = { "linkml_meta": {'alias': 'datetime_last_modified',
         'domain_of': ['ExtraData',
                       'Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:material/LabwareSubset/datetime_last_modified'} })
    labwaresubset_id: Optional[str] = Field(None, description="""LabwareSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'labwaresubset_id',
         'domain_of': ['LabwareSubset'],
         'slot_uri': 'lara:material/LabwareSubset/labwaresubset_id'} })
    tags: Optional[List[str]] = Field(None, description="""tags""", json_schema_extra = { "linkml_meta": {'alias': 'tags',
         'domain_of': ['Part',
                       'PartsSubset',
                       'Device',
                       'DevicesSubset',
                       'DeviceSetup',
                       'DeviceSetupsSubset',
                       'Labware',
                       'LabwareSubset'],
         'slot_uri': 'lara:base/Tag'} })
    labware: Optional[List[str]] = Field(None, description="""labwares in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'labware',
         'domain_of': ['LabwareSubset', 'OrderedLabwareSubset'],
         'slot_uri': 'lara:material/Labware'} })


class OrderedLabwareSubset(ConfiguredBaseModel):
    """
    \" Through model for the many-to-many relationship between LabwareSubset and Labware.
        This class can be used to store the order of labwares in a subset
    \"
    """
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'class_uri': 'lara:material/OrderedLabwareSubset',
         'from_schema': 'https://w3id.org/lara/lara_django_material'})

    orderedlabwaresubset_id: Optional[str] = Field(None, description="""OrderedLabwareSubset - primary key (UUID)""", json_schema_extra = { "linkml_meta": {'alias': 'orderedlabwaresubset_id',
         'domain_of': ['OrderedLabwareSubset'],
         'slot_uri': 'lara:material/OrderedLabwareSubset/orderedlabwaresubset_id'} })
    labware: str = Field(..., description="""labware in the subset""", json_schema_extra = { "linkml_meta": {'alias': 'labware',
         'domain_of': ['LabwareSubset', 'OrderedLabwareSubset'],
         'slot_uri': 'lara:material/Labware'} })
    labwares_subset: str = Field(..., description="""subset of labwares""", json_schema_extra = { "linkml_meta": {'alias': 'labwares_subset',
         'domain_of': ['OrderedLabwareSubset'],
         'slot_uri': 'lara:material/LabwareSubset'} })
    order: int = Field(..., description="""order of labware in subset""", json_schema_extra = { "linkml_meta": {'alias': 'order',
         'domain_of': ['OrderedPartsSubset',
                       'OrderedDevicesSubset',
                       'OrderedDeviceSetupsSubset',
                       'OrderedLabwareSubset'],
         'slot_uri': 'lara:material/OrderedLabwareSubset/order'} })


class Container(ConfiguredBaseModel):
    linkml_meta: ClassVar[LinkMLMeta] = LinkMLMeta({'from_schema': 'https://w3id.org/lara/lara_django_material', 'tree_root': True})

    extradatas: Optional[List[ExtraData]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'extradatas', 'domain_of': ['Container']} })
    partdeviceclasss: Optional[List[PartDeviceClass]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'partdeviceclasss', 'domain_of': ['Container']} })
    partshapes: Optional[List[PartShape]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'partshapes', 'domain_of': ['Container']} })
    parts: Optional[List[Part]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'parts', 'domain_of': ['PartsSubset', 'Container']} })
    partssubsets: Optional[List[PartsSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'partssubsets', 'domain_of': ['Container']} })
    orderedpartssubsets: Optional[List[OrderedPartsSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'orderedpartssubsets', 'domain_of': ['Container']} })
    deviceshapes: Optional[List[DeviceShape]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'deviceshapes', 'domain_of': ['Container']} })
    devices: Optional[List[Device]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'devices', 'domain_of': ['DevicesSubset', 'Container']} })
    devicessubsets: Optional[List[DevicesSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'devicessubsets', 'domain_of': ['Container']} })
    ordereddevicessubsets: Optional[List[OrderedDevicesSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'ordereddevicessubsets', 'domain_of': ['Container']} })
    devicesetupshapes: Optional[List[DeviceSetupShape]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'devicesetupshapes', 'domain_of': ['Container']} })
    devicesetups: Optional[List[DeviceSetup]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'devicesetups', 'domain_of': ['DeviceSetupsSubset', 'Container']} })
    devicesetupssubsets: Optional[List[DeviceSetupsSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'devicesetupssubsets', 'domain_of': ['Container']} })
    ordereddevicesetupssubsets: Optional[List[OrderedDeviceSetupsSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'ordereddevicesetupssubsets', 'domain_of': ['Container']} })
    labwareclasss: Optional[List[LabwareClass]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'labwareclasss', 'domain_of': ['Container']} })
    wellshapes: Optional[List[WellShape]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'wellshapes', 'domain_of': ['Container']} })
    labwareshapes: Optional[List[LabwareShape]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'labwareshapes', 'domain_of': ['Container']} })
    labwares: Optional[List[Labware]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'labwares', 'domain_of': ['Container']} })
    labwaresubsets: Optional[List[LabwareSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'labwaresubsets', 'domain_of': ['Container']} })
    orderedlabwaresubsets: Optional[List[OrderedLabwareSubset]] = Field(None, json_schema_extra = { "linkml_meta": {'alias': 'orderedlabwaresubsets', 'domain_of': ['Container']} })


# Model rebuild
# see https://pydantic-docs.helpmanual.io/usage/models/#rebuilding-a-model
MediaType.model_rebuild()
ExternalResource.model_rebuild()
Tag.model_rebuild()
Group.model_rebuild()
Entity.model_rebuild()
Namespace.model_rebuild()
DataType.model_rebuild()
ExtraData.model_rebuild()
PartDeviceClass.model_rebuild()
PartShape.model_rebuild()
Part.model_rebuild()
PartsSubset.model_rebuild()
OrderedPartsSubset.model_rebuild()
DeviceShape.model_rebuild()
Device.model_rebuild()
DevicesSubset.model_rebuild()
OrderedDevicesSubset.model_rebuild()
DeviceSetupShape.model_rebuild()
DeviceSetup.model_rebuild()
DeviceSetupsSubset.model_rebuild()
OrderedDeviceSetupsSubset.model_rebuild()
LabwareClass.model_rebuild()
WellShape.model_rebuild()
LabwareShape.model_rebuild()
Labware.model_rebuild()
LabwareSubset.model_rebuild()
OrderedLabwareSubset.model_rebuild()
Container.model_rebuild()

