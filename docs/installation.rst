.. highlight:: shell

============
Installation
============


Stable release
--------------

To install LARA-django Material, run this command in your terminal:

.. code-block:: console

    $ pip install lara_django_material

This is the preferred method to install LARA-django Material, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.


From source
-----------

* Check out the latest source with `Git`_: `git clone gitlab.com/larasuite/lara_django_material.git`
* cd into the directory where you cloned the repository.
* Install it with `pip`_: `pip install .`
* Install the development and testing requirements:
* Run the tests to make sure everything is working:


Uninstalling
------------

If you need to uninstall LARA-django Material, run this command in your terminal:

.. code-block:: console

    $ pip uninstall lara_django_material


.. _Git: https://git-scm.com
.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/

