"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material app *

:details: lara_django_material app configuration. 
         This provides a generic django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/4.0/ref/applications/
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


from django.apps import AppConfig


class LaraDjangoMaterialConfig(AppConfig):
    name = 'lara_django_material'
    url_path = 'material/'
    # enter a verbose name for your app: lara_django_material here - this will be used in the admin interface
    verbose_name = 'LARA-django Material'
    # lara_app_icon = 'lara_django_material_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.
    lara_app_color = 'amber'  # this will be used to highlight the app in the main LARA sidebar and app page
    lara_app_icon = "lara_material_icon.svg"
    verbose_name_short = "Material"
    lara_app_description = "Parts, Devices, Device Setups and Labware"
   
    
    def ready(self):
        # add urlpatterns 
        from django.urls import path, include
        from lara_django.urls import urlpatterns

        urlpatterns += [ 
            path(self.url_path, include('lara_django_material.urls', namespace='lara_django_material')),
        ]