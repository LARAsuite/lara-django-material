"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material views *

:details: lara_django_material views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""


from dataclasses import dataclass, field
from typing import List
import sys

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView

from .models import ExtraData, PartDeviceClass, PartShape, Part, Device, DeviceSetup, LabwareClass, WellShape, LabwareShape, Labware
from .forms import ExtraDataCreateForm, PartDeviceClassCreateForm, PartShapeCreateForm, PartCreateForm, DeviceCreateForm, DeviceSetupCreateForm, LabwareClassCreateForm, WellShapeCreateForm, LabwareShapeCreateForm, LabwareCreateForm, ExtraDataUpdateForm, PartDeviceClassUpdateForm, PartShapeUpdateForm, PartUpdateForm, DeviceUpdateForm, DeviceSetupUpdateForm, LabwareClassUpdateForm, WellShapeUpdateForm, LabwareShapeUpdateForm, LabwareUpdateForm
from .tables import ExtraDataTable, PartDeviceClassTable, PartShapeTable, PartTable, DeviceTable, DeviceSetupTable, LabwareClassTable, WellShapeTable, LabwareShapeTable, LabwareTable


@dataclass
class MaterialMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [   
        {'name': 'Part',
         'path': 'lara_django_material:part-list'},
        {'name': 'Part-Store',
                 'path': 'lara_django_material_store:part-list'},
        {'name': 'Devices',
         'path': 'lara_django_material:device-list'},
         {'name': 'Devices-Store',
         'path': 'lara_django_material_store:device-list'},
        {'name': 'Labware',
          'path': 'lara_django_material:labware-list'},
        {'name': 'Labware-Store',
          'path': 'lara_django_material_store:labware-list'}  
    ])


class ExtraDataSingleTableView(SingleTableView):
    model = ExtraData
    table_class = ExtraDataTable

    #fields = ('extradata_id', 'data_type', 'data_json',  'file_type', 'iri', 'URL', 'description', 'image', 'file')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/extradata/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "ExtraData - List"
        context['create_link'] = 'lara_django_material:extradata-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class ExtraDataDetailView(DetailView):
    model = ExtraData

    template_name = 'lara_django_material/extradata_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "ExtraData - Details"
        context['update_link'] = 'lara_django_material:extradata-update'
        context['list_views'] = ['ExtraData', 'PartDeviceClass', 'PartShape', 'Part',
                                 'Device', 'DeviceSetup', 'LabwareClass', 'WellShape', 'LabwareShape', 'Labware']

        return context


class ExtraDataCreateView(CreateView):
    model = ExtraData

    template_name = 'lara_django_material/create_form.html'
    form_class = ExtraDataCreateForm
    success_url = '/material/extradata/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "ExtraData - Create"
        return context


class ExtraDataUpdateView(UpdateView):
    model = ExtraData

    template_name = 'lara_django_material/update_form.html'
    form_class = ExtraDataUpdateForm
    success_url = '/material/extradata/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "ExtraData - Update"
        context['delete_link'] = 'lara_django_material:extradata-delete'
        return context


class ExtraDataDeleteView(DeleteView):
    model = ExtraData

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/extradata/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "ExtraData - Delete"
        context['delete_link'] = 'lara_django_material:extradata-delete'
        return context


class PartDeviceClassSingleTableView(SingleTableView):
    model = PartDeviceClass
    table_class = PartDeviceClassTable

    fields = ('name', 'description')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/partdeviceclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartDeviceClass - List"
        context['create_link'] = 'lara_django_material:partdeviceclass-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class PartDeviceClassDetailView(DetailView):
    model = PartDeviceClass

    template_name = 'lara_django_material/partdeviceclass_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartDeviceClass - Details"
        context['update_link'] = 'lara_django_material:partdeviceclass-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class PartDeviceClassCreateView(CreateView):
    model = PartDeviceClass

    template_name = 'lara_django_material/create_form.html'
    form_class = PartDeviceClassCreateForm
    success_url = '/material/partdeviceclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartDeviceClass - Create"
        return context


class PartDeviceClassUpdateView(UpdateView):
    model = PartDeviceClass

    template_name = 'lara_django_material/update_form.html'
    form_class = PartDeviceClassUpdateForm
    success_url = '/material/partdeviceclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartDeviceClass - Update"
        context['delete_link'] = 'lara_django_material:partdeviceclass-delete'
        return context


class PartDeviceClassDeleteView(DeleteView):
    model = PartDeviceClass

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/partdeviceclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartDeviceClass - Delete"
        context['delete_link'] = 'lara_django_material:partdeviceclass-delete'
        return context


class PartShapeSingleTableView(SingleTableView):
    model = PartShape
    table_class = PartShapeTable

    #fields = ('name', 'name_standard', 'width', 'length', 'height_unlidded', 'hight_lidded', 'hight_stacked', 'hight_stacked_lidded', 'model', 'description', 'partshape_id')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/partshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartShape - List"
        context['create_link'] = 'lara_django_material:partshape-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class PartShapeDetailView(DetailView):
    model = PartShape

    template_name = 'lara_django_material/partshape_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartShape - Details"
        context['update_link'] = 'lara_django_material:partshape-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class PartShapeCreateView(CreateView):
    model = PartShape

    template_name = 'lara_django_material/create_form.html'
    form_class = PartShapeCreateForm
    success_url = '/material/partshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartShape - Create"
        return context


class PartShapeUpdateView(UpdateView):
    model = PartShape

    template_name = 'lara_django_material/update_form.html'
    form_class = PartShapeUpdateForm
    success_url = '/material/partshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartShape - Update"
        context['delete_link'] = 'lara_django_material:partshape-delete'
        return context


class PartShapeDeleteView(DeleteView):
    model = PartShape

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/partshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "PartShape - Delete"
        context['delete_link'] = 'lara_django_material:partshape-delete'
        return context


class PartSingleTableView(SingleTableView):
    model = Part
    table_class = PartTable

    #fields = ('name', 'name_full', 'URL', 'pid', 'iri', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'spec_json', 'icon', 'image', 'description', 'part_id', 'part_class', 'shape')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part - List"
        context['create_link'] = 'lara_django_material:part-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class PartDetailView(DetailView):
    model = Part

    template_name = 'lara_django_material/part_detail.html'

    def get_context_data(self, **kwargs):

        object = get_object_or_404(Part, pk=self.kwargs['pk'])
        product_dict = { 
            "product_type": object.product_type,
            "maunfacturers":  object.manufacturer,
            "model_no": object.model_no,
             }
        
        part_specs_dict = {
            "Spec_JSON" : "height - 0.3 m", # object.spec_json,
            "Weight" : object.weight,
            "Energy Consumption" : object.energy_consumption,
            "Manual" : "manual comes here...",  #object.manual,
            "Service manual" : "service manual comes here ..", #object.service_manual,
            "Quickstart" : "quickstart comes here", #object.quickstart,
            "Brochure" : "brochure comes here", #object.brochure,
        }

        part_links_dict = {
           "url" : object.url,

        }

        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part - Details"
        context['update_link'] = 'lara_django_material:part-update'
        context['menu_items'] = MaterialMenu().menu_items

        context['product_dict'] = product_dict
        context['part_specs_dict'] = part_specs_dict
        context['part_links_dict'] = part_links_dict
        return context


class PartCreateView(CreateView):
    model = Part

    template_name = 'lara_django_material/create_form.html'
    form_class = PartCreateForm
    success_url = '/material/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part - Create"
        return context


class PartUpdateView(UpdateView):
    model = Part

    template_name = 'lara_django_material/update_form.html'
    form_class = PartUpdateForm
    success_url = '/material/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part - Update"
        context['delete_link'] = 'lara_django_material:part-delete'
        return context


class PartDeleteView(DeleteView):
    model = Part

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/part/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Part - Delete"
        context['delete_link'] = 'lara_django_material:part-delete'
        return context


class DeviceSingleTableView(SingleTableView):
    model = Device
    table_class = DeviceTable

    #fields = ('name', 'name_full', 'URL', 'pid', 'iri', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'spec_json', 'icon', 'image', 'description', 'device_id', 'device_class', 'shape')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device - List"
        context['create_link'] = 'lara_django_material:device-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class DeviceDetailView(DetailView):
    model = Device

    template_name = 'lara_django_material/device_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device - Details"
        context['update_link'] = 'lara_django_material:device-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class DeviceCreateView(CreateView):
    model = Device

    template_name = 'lara_django_material/create_form.html'
    form_class = DeviceCreateForm
    success_url = '/material/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device - Create"
        return context


class DeviceUpdateView(UpdateView):
    model = Device

    template_name = 'lara_django_material/update_form.html'
    form_class = DeviceUpdateForm
    success_url = '/material/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device - Update"
        context['delete_link'] = 'lara_django_material:device-delete'
        return context


class DeviceDeleteView(DeleteView):
    model = Device

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/device/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Device - Delete"
        context['delete_link'] = 'lara_django_material:device-delete'
        return context


class DeviceSetupSingleTableView(SingleTableView):
    model = DeviceSetup
    table_class = DeviceSetupTable

    #fields = ('name', 'name_full', 'URL', 'pid', 'iri', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'spec_json', 'icon', 'image', 'description', 'device_id', 'setup_class', 'blueprint_image', 'blueprint_file_type', 'blueprint_file', 'blueprint_pdf', 'scheme_svg', 'assembly')

    template_name = 'lara_django_material/list.html'
    success_url = '7material/devicesetup/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "DeviceSetup - List"
        context['create_link'] = 'lara_django_material:devicesetup-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class DeviceSetupDetailView(DetailView):
    model = DeviceSetup

    template_name = 'lara_django_material/devicesetup_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "DeviceSetup - Details"
        context['update_link'] = 'lara_django_material:devicesetup-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class DeviceSetupCreateView(CreateView):
    model = DeviceSetup

    template_name = 'lara_django_material/create_form.html'
    form_class = DeviceSetupCreateForm
    success_url = '/material/devicesetup/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "DeviceSetup - Create"
        return context


class DeviceSetupUpdateView(UpdateView):
    model = DeviceSetup

    template_name = 'lara_django_material/update_form.html'
    form_class = DeviceSetupUpdateForm
    success_url = '/material/devicesetup/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "DeviceSetup - Update"
        context['delete_link'] = 'lara_django_material:devicesetup-delete'
        return context


class DeviceSetupDeleteView(DeleteView):
    model = DeviceSetup

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/devicesetup/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "DeviceSetup - Delete"
        context['delete_link'] = 'lara_django_material:devicesetup-delete'
        return context


class LabwareClassSingleTableView(SingleTableView):
    model = LabwareClass
    table_class = LabwareClassTable

    #fields = ('labwareclass_id', 'name', 'description')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/labwareclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareClass - List"
        context['create_link'] = 'lara_django_material:labwareclass-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class LabwareClassDetailView(DetailView):
    model = LabwareClass

    template_name = 'lara_django_material/labwareclass_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareClass - Details"
        context['update_link'] = 'lara_django_material:labwareclass-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class LabwareClassCreateView(CreateView):
    model = LabwareClass

    template_name = 'lara_django_material/create_form.html'
    form_class = LabwareClassCreateForm
    success_url = '/material/labwareclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareClass - Create"
        return context


class LabwareClassUpdateView(UpdateView):
    model = LabwareClass

    template_name = 'lara_django_material/update_form.html'
    form_class = LabwareClassUpdateForm
    success_url = '/material/labwareclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareClass - Update"
        context['delete_link'] = 'lara_django_material:labwareclass-delete'
        return context


class LabwareClassDeleteView(DeleteView):
    model = LabwareClass

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/labwareclass/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareClass - Delete"
        context['delete_link'] = 'lara_django_material:labwareclass-delete'
        return context


class WellShapeSingleTableView(SingleTableView):
    model = WellShape
    table_class = WellShapeTable

    #fields = ('wellshape_id', 'depth_well', 'shape_well', 'shape_botton', 'top_radius_xy', 'bottom_radius_xy', 'bottom_radius_z', 'cone_angle', 'cone_depth', 'shape_polygon_xy', 'shape_polygon_z', 'model')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/wellshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "WellShape - List"
        context['create_link'] = 'lara_django_material:wellshape-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class WellShapeDetailView(DetailView):
    model = WellShape

    template_name = 'lara_django_material/wellshape_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "WellShape - Details"
        context['update_link'] = 'lara_django_material:wellshape-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class WellShapeCreateView(CreateView):
    model = WellShape

    template_name = 'lara_django_material/create_form.html'
    form_class = WellShapeCreateForm
    success_url = '/material/wellshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "WellShape - Create"
        return context


class WellShapeUpdateView(UpdateView):
    model = WellShape

    template_name = 'lara_django_material/update_form.html'
    form_class = WellShapeUpdateForm
    success_url = '/material/wellshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "WellShape - Update"
        context['delete_link'] = 'lara_django_material:wellshape-delete'
        return context


class WellShapeDeleteView(DeleteView):
    model = WellShape

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/wellshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "WellShape - Delete"
        context['delete_link'] = 'lara_django_material:wellshape-delete'
        return context


class LabwareShapeSingleTableView(SingleTableView):
    model = LabwareShape
    table_class = LabwareShapeTable

    #fields = ('name', 'name_standard', 'width', 'length', 'height_unlidded', 'hight_lidded', 'hight_stacked', 'hight_stacked_lidded', 'model', 'labwareshape_id', 'well_dist_row', 'well_dist_col', 'well_shape', 'description')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/labwareshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareShape - List"
        context['create_link'] = 'lara_django_material:labwareshape-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class LabwareShapeDetailView(DetailView):
    model = LabwareShape

    template_name = 'lara_django_material/labwareshape_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareShape - Details"
        context['update_link'] = 'lara_django_material:labwareshape-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class LabwareShapeCreateView(CreateView):
    model = LabwareShape

    template_name = 'lara_django_material/create_form.html'
    form_class = LabwareShapeCreateForm
    success_url = '/material/labwareshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareShape - Create"
        return context


class LabwareShapeUpdateView(UpdateView):
    model = LabwareShape

    template_name = 'lara_django_material/update_form.html'
    form_class = LabwareShapeUpdateForm
    success_url = '/material/labwareshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareShape - Update"
        context['delete_link'] = 'lara_django_material:labwareshape-delete'
        return context


class LabwareShapeDeleteView(DeleteView):
    model = LabwareShape

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/labwareshape/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "LabwareShape - Delete"
        context['delete_link'] = 'lara_django_material:labwareshape-delete'
        return context


class LabwareSingleTableView(SingleTableView):
    model = Labware
    table_class = LabwareTable

    #fields = ('name', 'name_full', 'URL', 'pid', 'iri', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'spec_json', 'icon', 'image', 'description', 'labware_id', 'labware_class', 'num_rows', 'num_cols', 'num_of_wells', 'shape', 'lid', 'liddable', 'sealable')

    template_name = 'lara_django_material/list.html'
    success_url = '/material/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware - List"
        context['create_link'] = 'lara_django_material:labware-create'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class LabwareDetailView(DetailView):
    model = Labware

    template_name = 'lara_django_material/labware_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware - Details"
        context['update_link'] = 'lara_django_material:labware-update'
        context['menu_items'] = MaterialMenu().menu_items
        return context


class LabwareCreateView(CreateView):
    model = Labware

    template_name = 'lara_django_material/create_form.html'
    form_class = LabwareCreateForm
    success_url = '/material/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware - Create"
        return context


class LabwareUpdateView(UpdateView):
    model = Labware

    template_name = 'lara_django_material/update_form.html'
    form_class = LabwareUpdateForm
    success_url = '/material/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware - Update"
        context['delete_link'] = 'lara_django_material:labware-delete'
        return context


class LabwareDeleteView(DeleteView):
    model = Labware

    template_name = 'lara_django_material/delete_form.html'
    success_url = '/material/labware/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Labware - Delete"
        context['delete_link'] = 'lara_django_material:labware-delete'
        return context
