"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material admin *

:details: lara_django_material admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_material >> tables.py" to update this file
________________________________________________________________________
"""

# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_material
# generated with django-extensions tests_generator  lara_django_material > tests.py (LARA-version)

import logging
from django.utils.html import format_html
import django_tables2 as tables


from .models import (
    ExtraData,
    PartDeviceClass,
    PartShape,
    Part,
    DeviceShape,
    Device,
    DeviceSetupShape,
    DeviceSetup,
    LabwareClass,
    WellShape,
    LabwareShape,
    Labware,
)


class ImageColumn(tables.Column):
    def render(self, value):
        return format_html('<img src="/media/{url}" height="48px", width="48px">', url=value)


class EditColumn(tables.Column):
    # add edit boostrap icon to column
    def render(self, value):
        return format_html(
            '<a href="/lara_django_people/entities/update/{url}"><i class="bi bi-pencil-square"></i></a>',
            url=value,
        )


class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
            "data_type",
            "namespace",
            "uri",
            "media_type",
            "iri",
            "url",
            "image",
            "file",
        )


class PartDeviceClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:partdeviceclass-detail', [tables.A('pk')]))

    class Meta:
        model = PartDeviceClass

        fields = ("name", "description")


class PartShapeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:partshape-detail', [tables.A('pk')]))

    class Meta:
        model = PartShape

        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
        )


class PartTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:part-detail', [tables.A('pk')]))

    name = tables.Column(linkify=("lara_django_material:part-detail", [tables.A("pk")]))

    edit = tables.Column(
        empty_values=(),
        verbose_name="Edit",
        default="Edit",
        linkify={
            "viewname": "lara_django_material:part-update",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-warning btn-sm"}},
    )

    delete = tables.Column(
        empty_values=(),
        verbose_name="Delete",
        default="Delete",
        linkify={
            "viewname": "lara_django_material:part-delete",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-danger btn-sm"}},
    )

    def render_edit(self):
        return format_html('<i class="bi bi-pencil-square"></i>')

    def render_delete(self):
        return format_html('<i class="bi bi-trash"></i>')

    class Meta:
        model = Part

        fields = (
            "image",
            "part_class",
            "name",
            # "manufacturer",
            "model_no",
            "product_type",
            "product_no",
            "edit",
            "delete",
        )


class DeviceShapeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:deviceshape-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceShape

        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
        )


class DeviceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    image = ImageColumn("image")
    name = tables.Column(linkify=("lara_django_material:device-detail", [tables.A("pk")]))

    edit = tables.Column(
        empty_values=(),
        verbose_name="Edit",
        default="Edit",
        linkify={
            "viewname": "lara_django_material:device-update",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-warning btn-sm"}},
    )

    order = tables.Column(
        empty_values=(),
        verbose_name="Order",
        default="Order",
        linkify={
            "viewname": "lara_django_material:device-update",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn bg-blue-300 btn-sm"}},
    )

    delete = tables.Column(
        empty_values=(),
        verbose_name="Delete",
        default="Delete",
        linkify={
            "viewname": "lara_django_material:device-delete",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-danger btn-sm"}},
    )

    def render_order(self):
        return format_html('<i class="bi bi-pencil-square"></i>')

    def render_edit(self):
        return format_html('<i class="bi bi-pencil-square"></i>')

    def render_delete(self):
        return format_html('<i class="bi bi-trash"></i>')

    class Meta:
        model = Device

        fields = (
            "image",
            "device_class",
            "name",
            # "manufacturer",
            "model_no",
            "product_type",
            "product_no",
            "energy_consumption",
            "edit",
            "order",
            "delete",
        )


class DeviceSetupShapeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:devicesetupshape-detail', [tables.A('pk')]))

    class Meta:
        model = DeviceSetupShape

        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
        )


class DeviceSetupTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=("lara_django_material:devicesetup-detail", [tables.A("pk")]))

    edit = tables.Column(
        empty_values=(),
        verbose_name="Edit",
        default="Edit",
        linkify={
            "viewname": "lara_django_material:devicesetup-update",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-warning btn-sm"}},
    )

    delete = tables.Column(
        empty_values=(),
        verbose_name="Delete",
        default="Delete",
        linkify={
            "viewname": "lara_django_material:devicesetup-delete",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-danger btn-sm"}},
    )

    def render_edit(self):
        return format_html('<i class="bi bi-pencil-square"></i>')

    def render_delete(self):
        return format_html('<i class="bi bi-trash"></i>')

    class Meta:
        model = DeviceSetup

        fields = (
            "image",
            "name",
            "setup_class",
            "url",
            # "manufacturer",
            "model_no",
            "product_type",
            "product_no",
            "energy_consumption",
            "edit",
            "delete",
        )


class LabwareClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:labwareclass-detail', [tables.A('pk')]))

    edit = tables.Column(
        empty_values=(),
        verbose_name="Edit",
        default="Edit",
        linkify={
            "viewname": "lara_django_material:labwareclass-update",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-warning btn-sm"}},
    )
    delete = tables.Column(
        empty_values=(),
        verbose_name="Delete",
        default="Delete",
        linkify={
            "viewname": "lara_django_material:labwareclass-delete",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-danger btn-sm"}},
    )

    def render_edit(self):
        return format_html('<i class="bi bi-pencil-square"></i>')

    def render_delete(self):
        return format_html('<i class="bi bi-trash"></i>')

    class Meta:
        model = LabwareClass

        fields = ("name", "iri", "description")


class WellShapeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:wellshape-detail', [tables.A('pk')]))

    class Meta:
        model = WellShape

        fields = (
            "name",
            "uri",
            "iri",
            "depth_well",
            "top_radius_xy",
            "bottom_radius_xy",
            "bottom_radius_z",
            "cone_angle",
            "cone_depth",
        )


class LabwareShapeTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_material:labwareshape-detail', [tables.A('pk')]))

    class Meta:
        model = LabwareShape

        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "well_dist_row",
            "well_dist_col",
            "well_shape",
        )


class LabwareTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=("lara_django_material:labware-detail", [tables.A("pk")]))

    edit = tables.Column(
        empty_values=(),
        verbose_name="Edit",
        default="Edit",
        linkify={
            "viewname": "lara_django_material:labware-update",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-warning btn-sm"}},
    )

    delete = tables.Column(
        empty_values=(),
        verbose_name="Delete",
        default="Delete",
        linkify={
            "viewname": "lara_django_material:labware-delete",
            "args": [(tables.A("pk"))],
        },
        attrs={"a": {"class": "btn btn-danger btn-sm"}},
    )

    def render_edit(self):
        return format_html('<i class="bi bi-pencil-square"></i>')

    def render_delete(self):
        return format_html('<i class="bi bi-trash"></i>')

    class Meta:
        model = Labware

        fields = (
            "icon",
            "labware_class",
            "name",
            "product_type",
            "product_no",
            # "manufacturer",
            "model_no",
            "image",
            "num_rows",
            "num_cols",
            "num_of_wells",
            "liddable",
            "sealable",
            "edit",
            "delete",
        )
