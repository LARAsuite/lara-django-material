"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material urls *

:details: lara_django_material urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings

from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
from django_socio_grpc.settings import grpc_settings

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_material"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view


urlpatterns = [
    path('extradata/list/', views.ExtraDataSingleTableView.as_view(),
         name='extradata-list'),
    path('extradata/create/', views.ExtraDataCreateView.as_view(),
         name='extradata-create'),
    path('extradata/update/<uuid:pk>',
         views.ExtraDataUpdateView.as_view(), name='extradata-update'),
    path('extradata/delete/<uuid:pk>',
         views.ExtraDataDeleteView.as_view(), name='extradata-delete'),
    path('extradata/<uuid:pk>/', views.ExtraDataDetailView.as_view(),
         name='extradata-detail'),
    path('partdeviceclass/list/', views.PartDeviceClassSingleTableView.as_view(),
         name='partdeviceclass-list'),
    path('partdeviceclass/create/', views.PartDeviceClassCreateView.as_view(),
         name='partdeviceclass-create'),
    path('partdeviceclass/update/<uuid:pk>',
         views.PartDeviceClassUpdateView.as_view(), name='partdeviceclass-update'),
    path('partdeviceclass/delete/<uuid:pk>',
         views.PartDeviceClassDeleteView.as_view(), name='partdeviceclass-delete'),
    path('partdeviceclass/<uuid:pk>/',
         views.PartDeviceClassDetailView.as_view(), name='partdeviceclass-detail'),
    path('partshape/list/', views.PartShapeSingleTableView.as_view(),
         name='partshape-list'),
    path('partshape/create/', views.PartShapeCreateView.as_view(),
         name='partshape-create'),
    path('partshape/update/<uuid:pk>',
         views.PartShapeUpdateView.as_view(), name='partshape-update'),
    path('partshape/delete/<uuid:pk>',
         views.PartShapeDeleteView.as_view(), name='partshape-delete'),
    path('partshape/<uuid:pk>/', views.PartShapeDetailView.as_view(),
         name='partshape-detail'),
    path('part/list/', views.PartSingleTableView.as_view(), name='part-list'),
    path('part/create/', views.PartCreateView.as_view(), name='part-create'),
    path('part/update/<uuid:pk>', views.PartUpdateView.as_view(), name='part-update'),
    path('part/delete/<uuid:pk>', views.PartDeleteView.as_view(), name='part-delete'),
    path('part/<uuid:pk>/', views.PartDetailView.as_view(), name='part-detail'),

    path('device/list/', views.DeviceSingleTableView.as_view(), name='device-list'),
    path('device/create/', views.DeviceCreateView.as_view(), name='device-create'),
    path('device/update/<uuid:pk>',
         views.DeviceUpdateView.as_view(), name='device-update'),
    path('device/delete/<uuid:pk>',
         views.DeviceDeleteView.as_view(), name='device-delete'),
    path('device/<uuid:pk>/', views.DeviceDetailView.as_view(),
         name='device-detail'),
         
    path('devicesetup/list/', views.DeviceSetupSingleTableView.as_view(),
         name='devicesetup-list'),
    path('devicesetup/create/', views.DeviceSetupCreateView.as_view(),
         name='devicesetup-create'),
    path('devicesetup/update/<uuid:pk>',
         views.DeviceSetupUpdateView.as_view(), name='devicesetup-update'),
    path('devicesetup/delete/<uuid:pk>',
         views.DeviceSetupDeleteView.as_view(), name='devicesetup-delete'),
    path('devicesetup/<uuid:pk>/', views.DeviceSetupDetailView.as_view(),
         name='devicesetup-detail'),
    path('labwareclass/list/', views.LabwareClassSingleTableView.as_view(),
         name='labwareclass-list'),
    path('labwareclass/create/', views.LabwareClassCreateView.as_view(),
         name='labwareclass-create'),
    path('labwareclass/update/<uuid:pk>',
         views.LabwareClassUpdateView.as_view(), name='labwareclass-update'),
    path('labwareclass/delete/<uuid:pk>',
         views.LabwareClassDeleteView.as_view(), name='labwareclass-delete'),
    path('labwareclass/<uuid:pk>/',
         views.LabwareClassDetailView.as_view(), name='labwareclass-detail'),
    path('wellshape/list/', views.WellShapeSingleTableView.as_view(),
         name='wellshape-list'),
    path('wellshape/create/', views.WellShapeCreateView.as_view(),
         name='wellshape-create'),
    path('wellshape/update/<uuid:pk>',
         views.WellShapeUpdateView.as_view(), name='wellshape-update'),
    path('wellshape/delete/<uuid:pk>',
         views.WellShapeDeleteView.as_view(), name='wellshape-delete'),
    path('wellshape/<uuid:pk>/', views.WellShapeDetailView.as_view(),
         name='wellshape-detail'),
    path('labwareshape/list/', views.LabwareShapeSingleTableView.as_view(),
         name='labwareshape-list'),
    path('labwareshape/create/', views.LabwareShapeCreateView.as_view(),
         name='labwareshape-create'),
    path('labwareshape/update/<uuid:pk>',
         views.LabwareShapeUpdateView.as_view(), name='labwareshape-update'),
    path('labwareshape/delete/<uuid:pk>',
         views.LabwareShapeDeleteView.as_view(), name='labwareshape-delete'),
    path('labwareshape/<uuid:pk>/',
         views.LabwareShapeDetailView.as_view(), name='labwareshape-detail'),
    path('labware/list/', views.LabwareSingleTableView.as_view(),
         name='labware-list'),
    path('labware/create/', views.LabwareCreateView.as_view(),
         name='labware-create'),
    path('labware/update/<uuid:pk>',
         views.LabwareUpdateView.as_view(), name='labware-update'),
    path('labware/delete/<uuid:pk>',
         views.LabwareDeleteView.as_view(), name='labware-delete'),
    path('labware/<uuid:pk>/', views.LabwareDetailView.as_view(),
         name='labware-detail'),
    path('', views.PartSingleTableView.as_view(), name='material-root'),
] 

# register handlers in settings
grpc_settings.user_settings["GRPC_HANDLERS"] += [
     "lara_django_material.grpc.handlers.grpc_handlers"]
