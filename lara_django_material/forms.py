"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material admin *

:details: lara_django_material admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_material > forms.py" to update this file
________________________________________________________________________
"""

# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import (
    ExtraData,
    PartDeviceClass,
    PartShape,
    Part,
    DeviceShape,
    Device,
    DeviceSetupShape,
    DeviceSetup,
    LabwareClass,
    WellShape,
    LabwareShape,
    Labware,
)


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            "data_type",
            "namespace",
            "name",
            
            "name_display",
            "version",
            "url",
            "description",
            "file",
            "media_type",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "data_type",
            "name",
            
            "name_display",
            "namespace",
            "version",
            "url",
            "description",
            "image",
            "file",
            Submit("submit", "Create"),
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            "data_type",
            "name",
            
            "name_display",
            "namespace",
            "version",
            
            "hash_sha256",
            "data_json",
            "media_type",
            "iri",
            "url",
            "description",
            "image",
            "file",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "data_type",
            "name",
            
            "name_display",
            "namespace",
            "version",
            
            "hash_sha256",
            "data_json",
            "media_type",
            "iri",
            "url",
            "description",
            "image",
            "file",
            Submit("submit", "Create"),
        )


class PartDeviceClassCreateForm(forms.ModelForm):
    class Meta:
        model = PartDeviceClass
        fields = ("name", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "description", Submit("submit", "Create"))


class PartDeviceClassUpdateForm(forms.ModelForm):
    class Meta:
        model = PartDeviceClass
        fields = ("name", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "description", Submit("submit", "Create"))


class PartShapeCreateForm(forms.ModelForm):
    class Meta:
        model = PartShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
            Submit("submit", "Create"),
        )


class PartShapeUpdateForm(forms.ModelForm):
    class Meta:
        model = PartShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
            Submit("submit", "Create"),
        )


class PartCreateForm(forms.ModelForm):
    class Meta:
        model = Part
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "part_class",
            "shape",
        )
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name", "required": True, "rows": 1}),
            
            "model_no": forms.TextInput(attrs={"placeholder": "Model number", "required": False, "rows": 1}),
            "product_type": forms.TextInput(attrs={"placeholder": "Product type", "required": False, "rows": 1}),
            "type_barcode": forms.TextInput(attrs={"placeholder": "Type barcode", "required": False, "rows": 1}),
            "product_no": forms.TextInput(attrs={"placeholder": "Product number", "required": False, "rows": 1}),
            "weight": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "energy_consumption": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "resources_external": forms.SelectMultiple(attrs={"placeholder": "resources_external", "required": False}),
            "url": forms.URLInput(),
            "pid": forms.URLInput(),
            #'icon': forms.TextInput(attrs={'type': 'file'}),
            #'image': forms.TextInput(attrs={'type': 'file'}),
            "description": forms.Textarea(attrs={"placeholder": "Description", "required": False, "rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("part_class", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("manufacturer", css_class="form-group col-md-3 mb-0"),
                Column("model_no", css_class="form-group col-md-3 mb-0"),
                Column("product_type", css_class="form-group col-md-2 mb-0"),
                Column("product_no", css_class="form-group col-md-2 mb-0"),
                Column("type_barcode", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column( css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("energy_consumption", css_class="form-group col-md-3 mb-0"),
                Column("weight", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("icon", css_class="form-group col-md-4 mb-0"),
                Column("image", css_class="form-group col-md-4 mb-0"),
                # Column('shape', css_class='form-group col-md-4 mb-0'),
            ),
            Row(
                Column("url", css_class="form-group col-md-6 mb-0"),
                Column("pid", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("description", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Create"),
        )


class PartUpdateForm(forms.ModelForm):
    class Meta:
        model = Part
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "part_class",
            "shape",
        )
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name", "required": True, "rows": 1}),
            
            "model_no": forms.TextInput(attrs={"placeholder": "Model number", "required": False, "rows": 1}),
            "product_type": forms.TextInput(attrs={"placeholder": "Product type", "required": False, "rows": 1}),
            "type_barcode": forms.TextInput(attrs={"placeholder": "Type barcode", "required": False, "rows": 1}),
            "product_no": forms.TextInput(attrs={"placeholder": "Product number", "required": False, "rows": 1}),
            "weight": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "energy_consumption": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "resources_external": forms.SelectMultiple(attrs={"placeholder": "resources_external", "required": False}),
            "url": forms.URLInput(),
            "pid": forms.URLInput(),
            "icon": forms.TextInput(attrs={"type": "file"}),
            "image": forms.TextInput(attrs={"type": "file"}),
            "description": forms.Textarea(attrs={"placeholder": "Description", "required": False, "rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("part_class", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("manufacturer", css_class="form-group col-md-3 mb-0"),
                Column("model_no", css_class="form-group col-md-3 mb-0"),
                Column("product_type", css_class="form-group col-md-2 mb-0"),
                Column("product_no", css_class="form-group col-md-2 mb-0"),
                Column("type_barcode", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column( css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("energy_consumption", css_class="form-group col-md-3 mb-0"),
                Column("weight", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("icon", css_class="form-group col-md-4 mb-0"),
                Column("image", css_class="form-group col-md-4 mb-0"),
                # Column('shape', css_class='form-group col-md-4 mb-0'),
            ),
            Row(
                Column("url", css_class="form-group col-md-6 mb-0"),
                Column("pid", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("description", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Update"),
        )


class DeviceShapeCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
            Submit("submit", "Create"),
        )


class DeviceShapeUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
            Submit("submit", "Create"),
        )


class DeviceCreateForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "device_class",
            "labware_min_capacity",
            "labware_max_capacity",
            "labware_max_height",
            "labware_storage_layout",
            "shape",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("device_class", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("manufacturer", css_class="form-group col-md-3 mb-0"),
                Column("model_no", css_class="form-group col-md-3 mb-0"),
                Column("product_type", css_class="form-group col-md-2 mb-0"),
                Column("product_no", css_class="form-group col-md-2 mb-0"),
                Column("type_barcode", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column( css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("energy_consumption", css_class="form-group col-md-3 mb-0"),
                Column("weight", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("url", css_class="form-group col-md-6 mb-0"),
                Column("pid", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("labware_min_capacity", css_class="form-group col-md-3 mb-0"),
                Column("labware_max_capacity", css_class="form-group col-md-3 mb-0"),
                Column("labware_max_height", css_class="form-group col-md-3 mb-0"),
                Column("labware_storage_layout", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("icon", css_class="form-group col-md-4 mb-0"),
                Column("image", css_class="form-group col-md-4 mb-0"),
                Column("shape", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
                Column("description", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Create"),
        )


class DeviceUpdateForm(forms.ModelForm):
    class Meta:
        model = Device
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "device_class",
            "labware_min_capacity",
            "labware_max_capacity",
            "labware_max_height",
            "labware_storage_layout",
            "shape",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("device_class", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("manufacturer", css_class="form-group col-md-3 mb-0"),
                Column("model_no", css_class="form-group col-md-3 mb-0"),
                Column("product_type", css_class="form-group col-md-2 mb-0"),
                Column("product_no", css_class="form-group col-md-2 mb-0"),
                Column("type_barcode", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column( css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("energy_consumption", css_class="form-group col-md-3 mb-0"),
                Column("weight", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("url", css_class="form-group col-md-6 mb-0"),
                Column("pid", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("labware_min_capacity", css_class="form-group col-md-3 mb-0"),
                Column("labware_max_capacity", css_class="form-group col-md-3 mb-0"),
                Column("labware_max_height", css_class="form-group col-md-3 mb-0"),
                Column("labware_storage_layout", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("icon", css_class="form-group col-md-4 mb-0"),
                Column("image", css_class="form-group col-md-4 mb-0"),
                Column("shape", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("description", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Update"),
        )


class DeviceSetupShapeCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceSetupShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
            Submit("submit", "Create"),
        )


class DeviceSetupShapeUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceSetupShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "description",
            Submit("submit", "Update"),
        )


class DeviceSetupCreateForm(forms.ModelForm):
    class Meta:
        model = DeviceSetup
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "setup_class",
            "shape",
            "blueprint_image",
            "blueprint_media_type",
            "blueprint_file",
            "blueprint_pdf",
            "scheme_svg",
            "assembly",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "setup_class",
            "shape",
            "blueprint_image",
            "blueprint_media_type",
            "blueprint_file",
            "blueprint_pdf",
            "scheme_svg",
            "assembly",
            Submit("submit", "Create"),
        )


class DeviceSetupUpdateForm(forms.ModelForm):
    class Meta:
        model = DeviceSetup
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "setup_class",
            "shape",
            "blueprint_image",
            "blueprint_media_type",
            "blueprint_file",
            "blueprint_pdf",
            "scheme_svg",
            "assembly",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "setup_class",
            "shape",
            "blueprint_image",
            "blueprint_media_type",
            "blueprint_file",
            "blueprint_pdf",
            "scheme_svg",
            "assembly",
            Submit("submit", "Create"),
        )


class LabwareClassCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareClass
        fields = ("name", "iri", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "iri", "description", Submit("submit", "Create"))


class LabwareClassUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareClass
        fields = ("name", "iri", "description")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout("name", "iri", "description", Submit("submit", "Update"))


class WellShapeCreateForm(forms.ModelForm):
    class Meta:
        model = WellShape
        fields = (
            "name",
            "iri",
            "depth_well",
            "shape_well",
            "shape_bottom",
            "top_radius_xy",
            "bottom_radius_xy",
            "bottom_radius_z",
            "cone_angle",
            "cone_depth",
            "shape_polygon_xy",
            "shape_polygon_z",
            "model_2d",
            "model_3d",
            "model_json",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "iri",
            "depth_well",
            "shape_well",
            "shape_bottom",
            "top_radius_xy",
            "bottom_radius_xy",
            "bottom_radius_z",
            "cone_angle",
            "cone_depth",
            "shape_polygon_xy",
            "shape_polygon_z",
            "model_2d",
            "model_3d",
            "model_json",
            Submit("submit", "Create"),
        )


class WellShapeUpdateForm(forms.ModelForm):
    class Meta:
        model = WellShape
        fields = (
            "name",
            "iri",
            "depth_well",
            "shape_well",
            "shape_bottom",
            "top_radius_xy",
            "bottom_radius_xy",
            "bottom_radius_z",
            "cone_angle",
            "cone_depth",
            "shape_polygon_xy",
            "shape_polygon_z",
            "model_2d",
            "model_3d",
            "model_json",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "iri",
            "depth_well",
            "shape_well",
            "shape_bottom",
            "top_radius_xy",
            "bottom_radius_xy",
            "bottom_radius_z",
            "cone_angle",
            "cone_depth",
            "shape_polygon_xy",
            "shape_polygon_z",
            "model_2d",
            "model_3d",
            "model_json",
            Submit("submit", "Update"),
        )


class LabwareShapeCreateForm(forms.ModelForm):
    class Meta:
        model = LabwareShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "well_dist_row",
            "well_dist_col",
            "well_shape",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "well_dist_row",
            "well_dist_col",
            "well_shape",
            "description",
            Submit("submit", "Create"),
        )


class LabwareShapeUpdateForm(forms.ModelForm):
    class Meta:
        model = LabwareShape
        fields = (
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "well_dist_row",
            "well_dist_col",
            "well_shape",
            "description",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            "name",
            "name_standard",
            "width",
            "length",
            "height_unlidded",
            "hight_lidded",
            "hight_stacked",
            "hight_stacked_lidded",
            "model_2d",
            "model_3d",
            "model_json",
            "well_dist_row",
            "well_dist_col",
            "well_shape",
            "description",
            Submit("submit", "Update"),
        )


class LabwareCreateForm(forms.ModelForm):
    class Meta:
        model = Labware
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "labware_class",
            "num_rows",
            "num_cols",
            "num_wells",
            "shape",
            "liddable",
            "sealable",
        )
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name", "required": True, "rows": 1}),
            
            "model_no": forms.TextInput(attrs={"placeholder": "Model number", "required": False, "rows": 1}),
            "product_type": forms.TextInput(attrs={"placeholder": "Product type", "required": False, "rows": 1}),
            "type_barcode": forms.TextInput(attrs={"placeholder": "Type barcode", "required": False, "rows": 1}),
            "product_no": forms.TextInput(attrs={"placeholder": "Product number", "required": False, "rows": 1}),
            "num_rows": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "num_cols": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "num_wells": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "liddable": forms.CheckboxInput(attrs={"placeholder": "Liddable", "required": False, "rows": 1}),
            "sealable": forms.CheckboxInput(attrs={"placeholder": "Sealable", "required": False, "rows": 1}),
            "weight": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "energy_consumption": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "resources_external": forms.SelectMultiple(attrs={"placeholder": "resources_external", "required": False}),
            "url": forms.URLInput(),
            "pid": forms.URLInput(),
            "icon": forms.TextInput(attrs={"type": "file"}),
            "image": forms.TextInput(attrs={"type": "file"}),
            "description": forms.Textarea(attrs={"placeholder": "Description", "required": False, "rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("labware_class", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("manufacturer", css_class="form-group col-md-3 mb-0"),
                Column("model_no", css_class="form-group col-md-3 mb-0"),
                Column("product_type", css_class="form-group col-md-2 mb-0"),
                Column("product_no", css_class="form-group col-md-2 mb-0"),
                Column("type_barcode", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column( css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("num_rows", css_class="form-group col-md-3 mb-0"),
                Column("num_cols", css_class="form-group col-md-3 mb-0"),
                Column("num_wells", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("liddable", css_class="form-group col-md-3 mb-0"),
                Column("sealable", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("energy_consumption", css_class="form-group col-md-3 mb-0"),
                Column("weight", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("url", css_class="form-group col-md-6 mb-0"),
                Column("pid", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("icon", css_class="form-group col-md-4 mb-0"),
                Column("image", css_class="form-group col-md-4 mb-0"),
                Column("shape", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("description", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Create"),
        )


class LabwareUpdateForm(forms.ModelForm):
    class Meta:
        model = Labware
        fields = (
            "name",
            
            "url",
            "pid",
            "iri",
            "manufacturer",
            "model_no",
            "product_type",
            "type_barcode",
            "product_no",
            "weight",
            "energy_consumption",
            "spec_json",
            "icon",
            "image",
            "resources_external",
            "description",
            "labware_class",
            "num_rows",
            "num_cols",
            "num_wells",
            "shape",
            "liddable",
            "sealable",
        )
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name", "required": True, "rows": 1}),
            
            "model_no": forms.TextInput(attrs={"placeholder": "Model number", "required": False, "rows": 1}),
            "product_type": forms.TextInput(attrs={"placeholder": "Product type", "required": False, "rows": 1}),
            "type_barcode": forms.TextInput(attrs={"placeholder": "Type barcode", "required": False, "rows": 1}),
            "product_no": forms.TextInput(attrs={"placeholder": "Product number", "required": False, "rows": 1}),
            "num_rows": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "num_cols": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "num_wells": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "liddable": forms.CheckboxInput(attrs={"placeholder": "Liddable", "required": False, "rows": 1}),
            "sealable": forms.CheckboxInput(attrs={"placeholder": "Sealable", "required": False, "rows": 1}),
            "weight": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "energy_consumption": forms.NumberInput(attrs={"placeholder": "1", "required": False, "rows": 1}),
            "resources_external": forms.SelectMultiple(attrs={"placeholder": "resources_external", "required": False}),
            "url": forms.URLInput(),
            "pid": forms.URLInput(),
            "icon": forms.TextInput(attrs={"type": "file"}),
            "image": forms.TextInput(attrs={"type": "file"}),
            "description": forms.Textarea(attrs={"placeholder": "Description", "required": False, "rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column("labware_class", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("name", css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("manufacturer", css_class="form-group col-md-3 mb-0"),
                Column("model_no", css_class="form-group col-md-3 mb-0"),
                Column("product_type", css_class="form-group col-md-2 mb-0"),
                Column("product_no", css_class="form-group col-md-2 mb-0"),
                Column("type_barcode", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column( css_class="form-group col-md-12 mb-0"),
            ),
            Row(
                Column("num_rows", css_class="form-group col-md-3 mb-0"),
                Column("num_cols", css_class="form-group col-md-3 mb-0"),
                Column("num_wells", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("liddable", css_class="form-group col-md-3 mb-0"),
                Column("sealable", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("energy_consumption", css_class="form-group col-md-3 mb-0"),
                Column("weight", css_class="form-group col-md-3 mb-0"),
            ),
            Row(
                Column("url", css_class="form-group col-md-6 mb-0"),
                Column("pid", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("resources_external", css_class="form-group col-md-6 mb-0"),
            ),
            Row(
                Column("icon", css_class="form-group col-md-4 mb-0"),
                Column("image", css_class="form-group col-md-4 mb-0"),
                Column("shape", css_class="form-group col-md-4 mb-0"),
            ),
            Row(
                Column("description", css_class="form-group col-md-12 mb-0"),
            ),
            Submit("submit", "Update"),
        )


# from .forms import ExtraDataCreateForm, PartDeviceClassCreateForm, PartShapeCreateForm, PartCreateForm, DeviceShapeCreateForm, DeviceCreateForm, DeviceSetupShapeCreateForm, DeviceSetupCreateForm, LabwareClassCreateForm, WellShapeCreateForm, LabwareShapeCreateForm, LabwareCreateFormExtraDataUpdateForm, PartDeviceClassUpdateForm, PartShapeUpdateForm, PartUpdateForm, DeviceShapeUpdateForm, DeviceUpdateForm, DeviceSetupShapeUpdateForm, DeviceSetupUpdateForm, LabwareClassUpdateForm, WellShapeUpdateForm, LabwareShapeUpdateForm, LabwareUpdateForm
