"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC handlers*

:details: lara_django_myproj gRPC handlers.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

# generated with django-socio-grpc generateprpcinterface lara_django_material  (LARA-version)

# import logging
from django_socio_grpc.services.app_handler_registry import AppHandlerRegistry
from lara_django_material.grpc.services import (
    ExtraDataService,
    PartDeviceClassService,
    PartDeviceClassByNameService,
    PartShapeService,
    PartService,
    # PartByNameService,
    PartsSubsetService,
    DeviceShapeService,
    DeviceService,
    # DeviceByNameService,
    DevicesSubsetService,
    DeviceSetupShapeService,
    DeviceSetupService,
    # DeviceSetupByNameService,
    DeviceSetupsSubsetService,
    LabwareClassService,
    WellShapeService,
    LabwareShapeService,
    LabwareService,
    # LabwareByNameService,
    LabwareSubsetService,
)


def grpc_handlers(server):
    app_registry = AppHandlerRegistry("lara_django_material", server)

    app_registry.get_grpc_module = lambda: "lara_django_material_grpc.v1"

    app_registry.register(ExtraDataService)

    app_registry.register(PartDeviceClassService)

    app_registry.register(PartDeviceClassByNameService)

    app_registry.register(PartShapeService)

    app_registry.register(PartService)

    # app_registry.register(PartByNameService)

    app_registry.register(PartsSubsetService)

    app_registry.register(DeviceShapeService)

    app_registry.register(DeviceService)

    # app_registry.register(DeviceByNameService)

    app_registry.register(DevicesSubsetService)

    app_registry.register(DeviceSetupShapeService)

    app_registry.register(DeviceSetupService)

    # app_registry.register(DeviceSetupByNameService)

    app_registry.register(DeviceSetupsSubsetService)

    app_registry.register(LabwareClassService)

    app_registry.register(WellShapeService)

    app_registry.register(LabwareShapeService)

    app_registry.register(LabwareService)

    # app_registry.register(LabwareByNameService)

    app_registry.register(LabwareSubsetService)
