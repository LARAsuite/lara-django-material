"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC services*

:details: lara_django_myproj gRPC services.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

## generated with django-socio-grpc generateprpcinterface lara_django_material  (LARA-version)

from django_socio_grpc import generics, mixins
from lara_django_base.grpc.mixins import AsyncUploadModelMixin, AsyncRetrieveByNameModelMixin, AsyncDownloadModelMixin

from .serializers import (
    ExtraDataProtoSerializer,
    PartDeviceClassProtoSerializer,
    PartDeviceClassByNameProtoSerializer,
    PartShapeProtoSerializer,
    PartProtoSerializer,
    PartByNameProtoSerializer,
    PartsSubsetProtoSerializer,
    DeviceShapeProtoSerializer,
    DeviceProtoSerializer,
    DeviceByNameProtoSerializer,
    DevicesSubsetProtoSerializer,
    DeviceSetupShapeProtoSerializer,
    DeviceSetupProtoSerializer,
    DeviceSetupByNameProtoSerializer,
    DeviceSetupsSubsetProtoSerializer,
    LabwareClassProtoSerializer,
    WellShapeProtoSerializer,
    LabwareShapeProtoSerializer,
    LabwareProtoSerializer,
    LabwareByNameProtoSerializer,
    LabwareSubsetProtoSerializer,
)


from lara_django_material.models import (
    ExtraData,
    PartDeviceClass,
    PartShape,
    Part,
    PartsSubset,
    DeviceShape,
    Device,
    DevicesSubset,
    DeviceSetupShape,
    DeviceSetup,
    DeviceSetupsSubset,
    LabwareClass,
    WellShape,
    LabwareShape,
    Labware,
    LabwareSubset,
)

from ..filters import (
    ExtraDataFilterSet,
    PartDeviceClassFilterSet,
    PartShapeFilterSet,
    PartFilterSet,
    DeviceShapeFilterSet,
    DeviceFilterSet,
    DeviceSetupShapeFilterSet,
    DeviceSetupFilterSet,
    LabwareClassFilterSet,
    WellShapeFilterSet,
    LabwareShapeFilterSet,
    LabwareFilterSet,
)


class ExtraDataService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = ExtraData.objects.all()
    serializer_class = ExtraDataProtoSerializer
    filterset_class = ExtraDataFilterSet
    search_fields = ["name", "description"]


class PartDeviceClassService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = PartDeviceClass.objects.all()
    serializer_class = PartDeviceClassProtoSerializer
    filterset_class = PartDeviceClassFilterSet
    search_fields = ["name", "description"]


class PartDeviceClassByNameService(generics.GenericService, mixins.AsyncListModelMixin, AsyncUploadModelMixin,
    AsyncRetrieveByNameModelMixin,
    AsyncDownloadModelMixin,):
    queryset = PartDeviceClass.objects.all().order_by("name")
    serializer_class = PartDeviceClassByNameProtoSerializer
    filterset_class = PartDeviceClassFilterSet

    lookup_field = "name"


class PartShapeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = PartShape.objects.all()
    serializer_class = PartShapeProtoSerializer
    filterset_class = PartShapeFilterSet
    search_fields = ["name", "description"]


class PartService(generics.AsyncModelService, mixins.AsyncStreamModelMixin, AsyncUploadModelMixin,
    AsyncRetrieveByNameModelMixin,
    AsyncDownloadModelMixin,):
    queryset = Part.objects.all()
    serializer_class = PartProtoSerializer
    filterset_class = PartFilterSet
    search_fields = ["name", "description"]


# class PartByNameService(generics.GenericService, mixins.AsyncListModelMixin, mixins.AsyncRetrieveModelMixin):
#     queryset = Part.objects.all().order_by("name")
#     serializer_class = PartByNameProtoSerializer
#     filterset_class = PartFilterSet

#     lookup_field = "name"

class PartsSubsetService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = PartsSubset.objects.all()
    serializer_class = PartsSubsetProtoSerializer
    search_fields = ["name", "description"]


class DeviceShapeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = DeviceShape.objects.all()
    serializer_class = DeviceShapeProtoSerializer
    filterset_class = DeviceShapeFilterSet
    search_fields = ["name", "description"]


class DeviceService(generics.AsyncModelService, mixins.AsyncStreamModelMixin, AsyncUploadModelMixin,
    AsyncRetrieveByNameModelMixin,
    AsyncDownloadModelMixin,):
    queryset = Device.objects.all()
    serializer_class = DeviceProtoSerializer
    filterset_class = DeviceFilterSet
    search_fields = ["name", "description"]


# class DeviceByNameService(generics.GenericService, mixins.AsyncListModelMixin, mixins.AsyncRetrieveModelMixin):
#     queryset = Device.objects.all().order_by("name")
#     serializer_class = DeviceByNameProtoSerializer
#     filterset_class = DeviceFilterSet

#     lookup_field = "name"

class DevicesSubsetService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = DevicesSubset.objects.all()
    serializer_class = DevicesSubsetProtoSerializer
    search_fields = ["name", "description"]


class DeviceSetupShapeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = DeviceSetupShape.objects.all()
    serializer_class = DeviceSetupShapeProtoSerializer
    filterset_class = DeviceSetupShapeFilterSet
    search_fields = ["name", "description"]


class DeviceSetupService(generics.AsyncModelService, mixins.AsyncStreamModelMixin, AsyncUploadModelMixin,
    AsyncRetrieveByNameModelMixin,
    AsyncDownloadModelMixin,):
    queryset = DeviceSetup.objects.all()
    serializer_class = DeviceSetupProtoSerializer
    filterset_class = DeviceSetupFilterSet
    search_fields = ["name", "description"]


# class DeviceSetupByNameService(generics.GenericService, mixins.AsyncListModelMixin, mixins.AsyncRetrieveModelMixin):
#     queryset = DeviceSetup.objects.all().order_by("name")
#     serializer_class = DeviceSetupByNameProtoSerializer
#     filterset_class = DeviceSetupFilterSet

#     lookup_field = "name"

class DeviceSetupsSubsetService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = DeviceSetupsSubset.objects.all()
    serializer_class = DeviceSetupsSubsetProtoSerializer
    search_fields = ["name", "description"]

class LabwareClassService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = LabwareClass.objects.all()
    serializer_class = LabwareClassProtoSerializer
    filterset_class = LabwareClassFilterSet
    search_fields = ["name", "description"]


class WellShapeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = WellShape.objects.all()
    serializer_class = WellShapeProtoSerializer
    filterset_class = WellShapeFilterSet
    search_fields = ["name", "description"]


class LabwareShapeService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = LabwareShape.objects.all()
    serializer_class = LabwareShapeProtoSerializer
    filterset_class = LabwareShapeFilterSet
    search_fields = ["name", "description"]


class LabwareService(generics.AsyncModelService, mixins.AsyncStreamModelMixin, AsyncUploadModelMixin,
    AsyncRetrieveByNameModelMixin,
    AsyncDownloadModelMixin,):
    queryset = Labware.objects.all()
    serializer_class = LabwareProtoSerializer
    filterset_class = LabwareFilterSet
    search_fields = ["name", "description"]


# class LabwareByNameService(generics.GenericService, mixins.AsyncListModelMixin, mixins.AsyncRetrieveModelMixin):
#     queryset = Labware.objects.all().order_by("name")
#     serializer_class = LabwareByNameProtoSerializer
#     filterset_class = LabwareFilterSet

#     lookup_field = "name"

class LabwareSubsetService(generics.AsyncModelService, mixins.AsyncStreamModelMixin):
    queryset = LabwareSubset.objects.all()
    serializer_class = LabwareSubsetProtoSerializer
    search_fields = ["name", "description"]