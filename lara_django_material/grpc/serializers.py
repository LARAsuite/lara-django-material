"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_myproj gRPC serializer*

:details: lara_django_myproj gRPC serializer.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

## generated with django-socio-grpc generateprpcinterface lara_django_material  (LARA-version)

import logging

from rest_framework.serializers import UUIDField, PrimaryKeyRelatedField
from django_socio_grpc import proto_serializers

from lara_django_base.models import (
    DataType,
    Namespace,
    MediaType,
    Tag,
    ItemStatus,
    ExternalResource,
)

from lara_django_people.models import (
    Entity,
    Group,
)
from lara_django_material.models import (
    ExtraData,
    PartDeviceClass,
    PartShape,
    Part,
    PartsSubset,
    DeviceShape,
    Device,
    DevicesSubset,
    DeviceSetupShape,
    DeviceSetup,
    DeviceSetupsSubset,
    LabwareClass,
    WellShape,
    LabwareShape,
    Labware,
    LabwareSubset,
)

import lara_django_material_grpc.v1.lara_django_material_pb2 as lara_django_material_pb2


class ExtraDataProtoSerializer(proto_serializers.ModelProtoSerializer):
    data_type = PrimaryKeyRelatedField(queryset=DataType.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True)
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    media_type = PrimaryKeyRelatedField(queryset=MediaType.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True)

    class Meta:
        model = ExtraData
        proto_class = lara_django_material_pb2.ExtraDataResponse

        proto_class_list = lara_django_material_pb2.ExtraDataListResponse

        fields = "__all__"  # [data_type', namespace', URI', text', XML', JSON', bin', media_type', IRI', URL', description', image', file']


class PartDeviceClassProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = PartDeviceClass
        proto_class = lara_django_material_pb2.PartDeviceClassResponse

        proto_class_list = lara_django_material_pb2.PartDeviceClassListResponse

        fields = "__all__"  # [name', description']

class PartDeviceClassByNameProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = PartDeviceClass
        proto_class = lara_django_material_pb2.PartDeviceClassResponse

        proto_class_list = lara_django_material_pb2.PartDeviceClassListResponse

        fields = "__all__"  # [name', description']

class PartShapeProtoSerializer(proto_serializers.ModelProtoSerializer):
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = PartShape
        proto_class = lara_django_material_pb2.PartShapeResponse

        proto_class_list = lara_django_material_pb2.PartShapeListResponse

        fields = "__all__"  # [name', name_standard', width', length', height_unlidded', hight_lidded', hight_stacked', hight_stacked_lidded', model_2d', model_3d', model_json', description']


class PartProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True,
        #required=False, allow_null=True
    )
    part_class = PrimaryKeyRelatedField(
        queryset=PartDeviceClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Part.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    shape = PrimaryKeyRelatedField(
        queryset=PartShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = Part
        proto_class = lara_django_material_pb2.PartResponse

        proto_class_list = lara_django_material_pb2.PartListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', part_class', shape']

class PartByNameProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True
    )
    part_class = PrimaryKeyRelatedField(
        queryset=PartDeviceClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Part.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    shape = PrimaryKeyRelatedField(
        queryset=PartShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = Part
        proto_class = lara_django_material_pb2.PartResponse

        proto_class_list = lara_django_material_pb2.PartListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', part_class', shape']


class PartsSubsetProtoSerializer(proto_serializers.ModelProtoSerializer):
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    entity = PrimaryKeyRelatedField(queryset=Entity.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    group = PrimaryKeyRelatedField(
        queryset=Group.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True
    )
    parts = PrimaryKeyRelatedField(
        queryset=Part.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True
    )
    tags = PrimaryKeyRelatedField(
        queryset=Tag.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = PartsSubset
        proto_class = lara_django_material_pb2.PartsSubsetResponse

        proto_class_list = lara_django_material_pb2.PartsSubsetListResponse

        fields = "__all__"

class DeviceShapeProtoSerializer(proto_serializers.ModelProtoSerializer):
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = DeviceShape
        proto_class = lara_django_material_pb2.DeviceShapeResponse

        proto_class_list = lara_django_material_pb2.DeviceShapeListResponse

        fields = "__all__"  # [name', name_standard', width', length', height_unlidded', hight_lidded', hight_stacked', hight_stacked_lidded', model_2d', model_3d', model_json', description']


class DeviceProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True
    )
    device_class = PrimaryKeyRelatedField(
        queryset=PartDeviceClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Part.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    shape = PrimaryKeyRelatedField(
        queryset=DeviceShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose" ),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = Device
        proto_class = lara_django_material_pb2.DeviceResponse

        proto_class_list = lara_django_material_pb2.DeviceListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', device_class', shape']


class DeviceByNameProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True
    )
    device_class = PrimaryKeyRelatedField(
        queryset=PartDeviceClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
        
    )
    components = PrimaryKeyRelatedField(
        queryset=Part.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    shape = PrimaryKeyRelatedField(
        queryset=DeviceShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose" ),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = Device
        proto_class = lara_django_material_pb2.DeviceResponse

        proto_class_list = lara_django_material_pb2.DeviceListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', device_class', shape']


class DevicesSubsetProtoSerializer(proto_serializers.ModelProtoSerializer):
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    entity = PrimaryKeyRelatedField(queryset=Entity.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    group = PrimaryKeyRelatedField(
        queryset=Group.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True
    )
    devices = PrimaryKeyRelatedField(
        queryset=Device.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True
    )
    tags = PrimaryKeyRelatedField(
        queryset=Tag.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = DevicesSubset
        proto_class = lara_django_material_pb2.DevicesSubsetResponse

        proto_class_list = lara_django_material_pb2.DevicesSubsetListResponse

        fields = "__all__"

class DeviceSetupShapeProtoSerializer(proto_serializers.ModelProtoSerializer):
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = DeviceSetupShape
        proto_class = lara_django_material_pb2.DeviceSetupShapeResponse

        proto_class_list = lara_django_material_pb2.DeviceSetupShapeListResponse

        fields = "__all__"  # [name', name_standard', width', length', height_unlidded', hight_lidded', hight_stacked', hight_stacked_lidded', model_2d', model_3d', model_json', description']


class DeviceSetupProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True,
        required=False, allow_null=True
    )
    setup_class = PrimaryKeyRelatedField(
        queryset=PartDeviceClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Device.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    shape = PrimaryKeyRelatedField(
        queryset=DeviceSetupShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    blueprint_media_type = PrimaryKeyRelatedField(
        queryset=MediaType.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Device.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )

    class Meta:
        model = DeviceSetup
        proto_class = lara_django_material_pb2.DeviceSetupResponse

        proto_class_list = lara_django_material_pb2.DeviceSetupListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', setup_class', shape', blueprint_image', blueprint_media_type', blueprint_file', blueprint_pdf', scheme_svg', assembly']

class DeviceSetupByNameProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True,
        required=False, allow_null=True
    )
    setup_class = PrimaryKeyRelatedField(
        queryset=PartDeviceClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    shape = PrimaryKeyRelatedField(
        queryset=DeviceSetupShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    blueprint_media_type = PrimaryKeyRelatedField(
        queryset=MediaType.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Device.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )

    class Meta:
        model = DeviceSetup
        proto_class = lara_django_material_pb2.DeviceSetupResponse

        proto_class_list = lara_django_material_pb2.DeviceSetupListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', setup_class', shape', blueprint_image', blueprint_media_type', blueprint_file', blueprint_pdf', scheme_svg', assembly']

class DeviceSetupsSubsetProtoSerializer(proto_serializers.ModelProtoSerializer):
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    entity = PrimaryKeyRelatedField(queryset=Entity.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    group = PrimaryKeyRelatedField(
        queryset=Group.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True
    )
    devicesetups = PrimaryKeyRelatedField(
        queryset=DeviceSetup.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True
    )
    tags = PrimaryKeyRelatedField(
        queryset=Tag.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = DeviceSetupsSubset
        proto_class = lara_django_material_pb2.DeviceSetupsSubsetResponse

        proto_class_list = lara_django_material_pb2.DeviceSetupsSubsetListResponse

        fields = "__all__"


class LabwareClassProtoSerializer(proto_serializers.ModelProtoSerializer):    
    class Meta:
        model = LabwareClass
        proto_class = lara_django_material_pb2.LabwareClassResponse

        proto_class_list = lara_django_material_pb2.LabwareClassListResponse

        fields = "__all__"  # [name', IRI', description']


class WellShapeProtoSerializer(proto_serializers.ModelProtoSerializer):
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )

    # ExtraDataProtoSerializer(many=True)
    class Meta:
        model = WellShape
        proto_class = lara_django_material_pb2.WellShapeResponse

        proto_class_list = lara_django_material_pb2.WellShapeListResponse

        fields = "__all__"  # [name', URI', IRI', depth_well', shape_well', shape_bottom', top_radius_xy', bottom_radius_xy', bottom_radius_z', cone_angle', cone_depth', shape_polygon_xy', shape_polygon_z', model_2d', model_3d', model_json']


class LabwareShapeProtoSerializer(proto_serializers.ModelProtoSerializer):
    well_shape = PrimaryKeyRelatedField(
        queryset=WellShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )

    # WellShapeProtoSerializer()
    class Meta:
        model = LabwareShape
        proto_class = lara_django_material_pb2.LabwareShapeResponse

        proto_class_list = lara_django_material_pb2.LabwareShapeListResponse

        fields = "__all__"  # [name', name_standard', width', length', height_unlidded', hight_lidded', hight_stacked', hight_stacked_lidded', model_2d', model_3d', model_json', well_dist_row', well_dist_col', well_shape', description']


class LabwareProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True,
        required=False, allow_null=True
    )
    labware_class = PrimaryKeyRelatedField(
        queryset=LabwareClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
    )
    shape = PrimaryKeyRelatedField(
        queryset=LabwareShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    lids = PrimaryKeyRelatedField(
        queryset=Labware.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    components = PrimaryKeyRelatedField(
        queryset=Part.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )

    class Meta:
        model = Labware
        proto_class = lara_django_material_pb2.LabwareResponse

        proto_class_list = lara_django_material_pb2.LabwareListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', labware_class', num_rows', num_cols', num_of_wells', shape', liddable', sealable']


class LabwareByNameProtoSerializer(proto_serializers.ModelProtoSerializer):
    manufacturer = PrimaryKeyRelatedField(
        queryset=Entity.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        # many=True,
    )
    labware_class = PrimaryKeyRelatedField(
        queryset=LabwareClass.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
    )
    shape = PrimaryKeyRelatedField(
        queryset=LabwareShape.objects.all(),
        pk_field=UUIDField(format="hex_verbose"),
        required=False, allow_null=True
    )
    resources_external = PrimaryKeyRelatedField(
        queryset=ExternalResource.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    tags =  PrimaryKeyRelatedField(
        queryset=Tag.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    lids = PrimaryKeyRelatedField(
        queryset=Labware.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    data_extra = PrimaryKeyRelatedField(
        queryset=ExtraData.objects.all(),
        pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )

    class Meta:
        model = Labware
        proto_class = lara_django_material_pb2.LabwareResponse

        proto_class_list = lara_django_material_pb2.LabwareListResponse

        fields = "__all__"  # [name', name_full', URL', handle', IRI', manufacturer', model_no', product_type', type_barcode', product_no', weight', energy_consumption', spec_json', icon', image', description', labware_class', num_rows', num_cols', num_of_wells', shape', liddable', sealable']

class LabwareSubsetProtoSerializer(proto_serializers.ModelProtoSerializer):
    namespace = PrimaryKeyRelatedField(queryset=Namespace.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    entity = PrimaryKeyRelatedField(queryset=Entity.objects.all(), pk_field=UUIDField(format="hex_verbose"))
    group = PrimaryKeyRelatedField(
        queryset=Group.objects.all(), pk_field=UUIDField(format="hex_verbose"), required=False, allow_null=True
    )
    labware = PrimaryKeyRelatedField(
        queryset=Labware.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True
    )
    tags = PrimaryKeyRelatedField(
        queryset=Tag.objects.all(), pk_field=UUIDField(format="hex_verbose"), many=True, required=False, allow_null=True
    )
    class Meta:
        model = LabwareSubset
        proto_class = lara_django_material_pb2.LabwareSubsetResponse

        proto_class_list = lara_django_material_pb2.LabwareSubsetListResponse

        fields = "__all__"