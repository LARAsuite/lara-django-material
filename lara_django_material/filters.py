"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_data filter app *

:details: lara_django_data filter app. 
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django_filters.rest_framework import FilterSet, CharFilter, DateRangeFilter

from .models import (
    ExtraData,
    PartDeviceClass,
    PartShape,
    Part,
    DeviceShape,
    Device,
    DeviceSetupShape,
    DeviceSetup,
    LabwareClass,
    WellShape,
    LabwareShape,
    Labware,
)


class ExtraDataFilterSet(FilterSet):
    class Meta:
        model = ExtraData
        fields = {
            "name_display": ["exact", "contains"],
            "name": ["exact", "contains"],
            "iri": ["exact", "contains"],
            "url": ["exact", "contains"],
            "datetime_created": ["lt", "gt", "exact", "range"],
            "datetime_last_modified": ["lt", "gt", "exact", "range"],
            "description": ["exact", "contains"],
        }


class PartDeviceClassFilterSet(FilterSet):
    class Meta:
        model = PartDeviceClass
        fields = {
            "name": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class PartShapeFilterSet(FilterSet):
    class Meta:
        model = PartShape
        fields = {
            "name": ["exact", "contains"],
            "description": ["exact", "contains"],
        }


class PartFilterSet(FilterSet):
    model = Part
    fields = {
        "name": ["exact", "contains"],
        "url": ["exact", "contains"],
        "pid": ["exact", "contains"],
        "iri": ["exact", "contains"],
        "model_no": ["exact", "contains"],
        "product_type": ["exact", "contains"],
        "type_barcode": ["exact", "contains"],
        "product_no": ["exact", "contains"],
        "weight": ["lt", "gt", "exact", "range"],
        "energy_consumption": ["lt", "gt", "exact", "range"],
        "datetime_last_modified": ["lt", "gt", "exact", "range"],
        "description": ["exact", "contains"],
    }


class DeviceShapeFilterSet(FilterSet):
    model = DeviceShape
    fields = {
        "name": ["exact", "contains"],
        "description": ["exact", "contains"],
    }


class DeviceFilterSet(FilterSet):
    model = Device
    fields = {
        "name": ["exact", "contains"],
        "url": ["exact", "contains"],
        "pid": ["exact", "contains"],
        "iri": ["exact", "contains"],
        "model_no": ["exact", "contains"],
        "product_type": ["exact", "contains"],
        "type_barcode": ["exact", "contains"],
        "product_no": ["exact", "contains"],
        "weight": ["lt", "gt", "exact", "range"],
        "energy_consumption": ["lt", "gt", "exact", "range"],
        "datetime_last_modified": ["lt", "gt", "exact", "range"],
        "description": ["exact", "contains"],
        "labware_min_capacity": ["lt", "gt", "exact"],
        "labware_max_capacity": ["lt", "gt", "exact"],
        "labware_max_height": ["lt", "gt", "exact"],
        "description": ["exact", "contains"],
    }


class DeviceSetupShapeFilterSet(FilterSet):
    model = DeviceSetupShape
    fields = {
        "name": ["exact", "contains"],
        "description": ["exact", "contains"],
    }


class DeviceSetupFilterSet(FilterSet):
    model = DeviceSetup
    fields = {
        "name": ["exact", "contains"],
        "url": ["exact", "contains"],
        "pid": ["exact", "contains"],
        "iri": ["exact", "contains"],
        "model_no": ["exact", "contains"],
        "product_type": ["exact", "contains"],
        "type_barcode": ["exact", "contains"],
        "product_no": ["exact", "contains"],
        "weight": ["lt", "gt", "exact", "range"],
        "energy_consumption": ["lt", "gt", "exact", "range"],
        "datetime_last_modified": ["lt", "gt", "exact", "range"],
        "description": ["exact", "contains"],
    }


class LabwareClassFilterSet(FilterSet):
    model = LabwareClass
    fields = {
        "name": ["exact", "contains"],
        "description": ["exact", "contains"],
    }


class WellShapeFilterSet(FilterSet):
    model = WellShape
    fields = {
        "name": ["exact", "contains"],
        "description": ["exact", "contains"],
    }


class LabwareShapeFilterSet(FilterSet):
    model = LabwareShape
    fields = {
        "name": ["exact", "contains"],
        "well_dist_row": ["lt", "gt", "exact"],
        "well_dist_col": ["lt", "gt", "exact"],
        "description": ["exact", "contains"],
    }


class LabwareFilterSet(FilterSet):
    model = Labware
    fields = {
        "name": ["exact", "contains"],
        "url": ["exact", "contains"],
        "pid": ["exact", "contains"],
        "iri": ["exact", "contains"],
        "model_no": ["exact", "contains"],
        "product_type": ["exact", "contains"],
        "type_barcode": ["exact", "contains"],
        "product_no": ["exact", "contains"],
        "weight": ["lt", "gt", "exact", "range"],
        "energy_consumption": ["lt", "gt", "exact", "range"],
        "datetime_last_modified": ["lt", "gt", "exact", "range"],
        "description": ["exact", "contains"],
        "num_rows": ["lt", "gt", "exact"],
        "num_cols": ["lt", "gt", "exact"],
        "num_wells": ["lt", "gt", "exact"],
        "description": ["exact", "contains"],
        #'labware_class': ['exact'],
    }
