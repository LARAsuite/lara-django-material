"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material models *

:details: lara_django_material database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import uuid
import hashlib
import json
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField

from django.db import models

from lara_django_base.models import (
    URNField,
    MediaType,
    ExtraDataAbstr,
    Tag,
    ExternalResource,
)
from lara_django_people.models import Entity, ItemSubsetAbstr


settings.FIXTURES += ["1_partdeviceclasses_fix", "2_labwareclasses_fix"]


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend material models by extra information,
    e.g., maintenance plan, maintenance history, incidents history,  image filename , ...
    """

    model_curi = "lara:material/ExtraData"
    image = models.ImageField(
        upload_to="material/images/",
        blank=True,
        null=True,
        help_text="location room map rel. path/filename to image",
    )
    file = models.FileField(
        upload_to="material/", blank=True, null=True, help_text="rel. path/filename"
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for type barcode and IRI
        """

        if self.version is None or self.version == "":
            self.version = "0.0.1"

        if self.hash_sha256 is None:
            if self.data_json is not None:
                to_hash = json.dumps(self.data_json) + str(self.extradata_id)
                self.hash_sha256 = hashlib.sha256(to_hash.encode("utf-8")).hexdigest()
            else:
                self.hash_sha256 = hashlib.sha256(
                    str(self.extradata_id).encode("utf-8")
                ).hexdigest()

        if self.name is None or self.name == "":
            if self.name_display is None or self.name_display == "":
                self.name = "_".join(
                    (
                        timezone.now().strftime("%Y%m%d_%H%M%S"),
                        "data",
                        self.hash_sha256[:8],
                    )
                )
            else:
                self.name = self.name_display.replace(" ", "_").lower().strip()

        else:
            self.name = self.name.replace(" ", "_").lower().strip()
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None and self.version is not None:
                self.name_full = f"{self.namespace.uri}/" + "_".join(
                    (self.name.replace(" ", "_"), self.version)
                )
            else:
                self.name_full = "_".join((self.name.replace(" ", "_"), self.version))

        if not self.iri:
            netloc = self.namespace.parse_URI().netloc
            path = self.namespace.parse_URI().path

            self.iri = f"{settings.LARA_PREFIXES['lara']}material/ExtraData/{netloc}{path}/{self.name.replace(' ', '_').lower().strip()}"

        if self.datetime_created is None:
            self.datetime_created = timezone.now()

        if self.datetime_last_modified is None:
            self.datetime_last_modified = timezone.now()

        super().save(*args, **kwargs)


class PartDeviceClass(models.Model):
    """classes for parts and device, e.g. screw, bolt, robot, spectrometer, HPLC, NMR,"""

    model_curi = "lara:material/PartDeviceClass"
    partdeviceclass_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    name = models.TextField(
        unique=True,
        help_text="name of the part/device class, like screw, bolt, robot, spectrometer, HPLC, NMR,",
    )
    iri = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation in a related ontology",
    )
    description = models.TextField(blank=True, help_text="description")

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for type barcode and IRI
        """
        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/PartDeviceClass/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ""

    class Meta:
        verbose_name_plural = "PartDeviceClasses"
        db_table = "lara_material_part_device_class"


class PartShapeAbstr(models.Model):
    """Abstract Part / Device / Labware shape  class, describing the shape and dimensions of a part/device/labware.
    e.g. SBS.96.round.fb
    """

    name = models.TextField(
        unique=True,
        null=True,
        help_text="name of shape dimension set, e.g. SBS.96.round.fb",
    )
    name_standard = models.TextField(
        blank=True, null=True, help_text="underlying labware/shape standard, e.g. SBS"
    )  # might need an extra table
    iri = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    width = models.FloatField(blank=True, null=True, help_text="cont. total width, ")
    length = models.FloatField(blank=True, null=True, help_text="cont. total length ")
    height_unlidded = models.FloatField(
        blank=True, null=True, help_text="cont. height, without lid"
    )
    hight_lidded = models.FloatField(
        blank=True, null=True, help_text="cont. height incl. lid"
    )
    hight_stacked = models.FloatField(
        blank=True, null=True, help_text="stacking height, unlidded"
    )
    hight_stacked_lidded = models.FloatField(
        blank=True, null=True, help_text="stacking height lidded"
    )
    model_2d = models.TextField(
        blank=True, null=True, help_text="2D model of part shape in, e.g., SVG format"
    )
    model_3d = models.TextField(
        blank=True, null=True, help_text="3D model of part shape in, e.g., STL format"
    )
    model_json = models.JSONField(
        blank=True, null=True, help_text="2D or 3D model of part shape in JSON format"
    )
    description = models.TextField(
        blank=True, null=True, help_text="description of part shape"
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""


class PartAbstr(models.Model):
    """A Part is an artifact that is used to build a device or perform an experiment
    (screw, nut, bolt, transistor, IC, lamp, diode, ... ).
    NOTE: model part/device state in process online, waiting, busy, shutting down, off; default: init
    """

    name_display = models.TextField(
        blank=True,
        null=True,
        help_text="human readable name of the part/device/labware for the UI.",
    )
    name = models.TextField(
        blank=True,
        null=True,
        help_text="short part or device name, should not contain whitespaces",
    )
    url = models.URLField(
        blank=True, null=True, help_text="Universal Resource Locator - URL"
    )
    pid = models.URLField(blank=True, null=True, unique=True, help_text="handle URI")
    # PAC-ID:  https://github.com/ApiniLabs/PAC-ID
    pac_id = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="Publicly Addressable Content IDentifier is a globally unique identifier that operates independently of a central registry.",
    )
    iri = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    # in case of mulitiple manufacturer, this could be modeled with data_extra
    manufacturer = models.ForeignKey(
        Entity,
        related_name="%(app_label)s_%(class)s_manufacturer_related",
        related_query_name="%(app_label)s_%(class)s_manufacturer_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="manufacturer of the part/device/labware",
    )
    model_no = models.TextField(
        blank=True, null=True, help_text="model or part number (of e.g. manufacturer)"
    )
    product_type = models.TextField(
        blank=True, null=True, help_text="(manufacturer) product type"
    )
    # ',UNIQUE(barcode) ON CONFLICT IGNORE )'),
    type_barcode = models.TextField(
        blank=True,
        null=True,
        unique=True,
        help_text="barcode of the general part type, not of a specific part !!.",
    )
    type_barcode_json = models.JSONField(
        blank=True,
        null=True,
        help_text="Advanced barcodes in JSON for multiple barcode representations, like QR codes etc.",
    )
    product_no = models.TextField(
        blank=True, null=True, help_text="manufacturer product number"
    )
    weight = models.FloatField(blank=True, null=True, help_text="part net weight in kg")
    energy_consumption = models.FloatField(
        null=True, blank=True, help_text="energy consumption in W"
    )
    spec_json = models.JSONField(
        blank=True, null=True, help_text="part specifications in JSON format"
    )
    hardware_config = models.JSONField(
        blank=True,
        null=True,
        help_text="Configuration of the item hardware setup, like connection schemata, wiring, tubing, ...",
    )

    resources_external = models.ManyToManyField(
        ExternalResource,
        related_name="%(app_label)s_%(class)s_external_resources_related",
        related_query_name="%(app_label)s_%(class)s_external_resources_related_query",
        blank=True,
        help_text="external resources, like literature, websites, manuals, ...",
    )
    icon = models.TextField(
        blank=True, null=True, help_text="XML/SVG icon/logo/drawing of the part"
    )
    image = models.ImageField(
        upload_to="material/",
        blank=True,
        null=True,
        help_text="rel. path/filename to image",
    )
    tags = models.ManyToManyField(
        Tag,
        related_name="%(app_label)s_%(class)s_tags_related",
        related_query_name="%(app_label)s_%(class)s_tags_related_query",
        blank=True,
        help_text="e.g. manual",
    )
    datetime_last_modified = models.DateTimeField(
        blank=True, null=True, help_text="date and time when data was last modified"
    )
    description = models.TextField(
        blank=True, null=True, help_text="description of the device"
    )
    search_vector = SearchVectorField(null=True)

    class Meta:
        abstract = True
        constraints = [
            models.UniqueConstraint(
                fields=["name", "manufacturer"],
                name="%(app_label)s_%(class)s_name_manufacturer_unique",
            ),
        ]
        indexes = (GinIndex(fields=["search_vector"]),)

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""


class PartShape(PartShapeAbstr):
    """Part shape and dimensions"""

    model_curi = "lara:material/PartShape"
    partshape_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_ps_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_ps_data_extra_related_query",
        blank=True,
        help_text="part shape extra data, like 3D model or polygon describing the part shape/dimensions",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/PartShape/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)


class Part(PartAbstr):
    """generic Part class"""

    model_curi = "lara:material/Part"
    part_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    part_class = models.ForeignKey(
        PartDeviceClass,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="screw, bolt, nut, lamp, diode, resitor, IC, ...",
    )
    shape = models.ForeignKey(
        PartShape,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="shape of Part",
    )
    components = models.ManyToManyField(
        "self",
        related_query_name="%(app_label)s_%(class)s_components_related_query",
        blank=True,
        help_text="components of the part",
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_parts_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_parts_data_extra_related_query",
        blank=True,
        help_text="e.g. manual",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for type barcode and IRI
        """
        if self.name is None or self.name == "":
            if self.name_display is not None or self.name_display != "":
                self.name = self.name_display.replace(" ", "_").lower().strip()

        if self.name is not None or self.name != "":
            self.name = self.name.replace(" ", "_").lower().strip()

        if self.name_display is None or self.name_display == "":
            self.name_display = self.name

        if self.type_barcode == "" or self.type_barcode is None:
            self.type_barcode = (
                f"AUTO_{str(uuid.uuid4())[:8]}"  # avoiding unique constraint violation
            )

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}/material/Part/{self.name.replace(' ', '_').lower().strip()}_{str(self.part_id)[:8]}"

        if self.datetime_last_modified is None:
            self.datetime_last_modified = timezone.now()

        super().save(*args, **kwargs)


class PartsSubset(ItemSubsetAbstr):
    """Parts subset. It can be used for user or group defined subsets of parts."""

    model_curi = "lara:material/PartsSubset"
    partssubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    parts = models.ManyToManyField(
        Part,
        related_name="%(app_label)s_%(class)s_parts_subset_related",
        related_query_name="%(app_label)s_%(class)s_parts_subset_related_query",
        blank=True,
        help_text="parts in the subset",
        through="OrderedPartsSubset",
    )


class OrderedPartsSubset(models.Model):
    """Through model for the many-to-many relationship between PartsSubset and Part.
    This class can be used to store the order of parts in a subset"""

    model_curi = "lara:material/OrderedPartsSubset"

    orderedpartssubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )

    part = models.ForeignKey(
        Part,
        related_name="%(app_label)s_%(class)s_part_related",
        related_query_name="%(app_label)s_%(class)s_part",
        on_delete=models.CASCADE,
        help_text="part in the subset",
    )

    parts_subset = models.ForeignKey(
        PartsSubset,
        related_name="%(app_label)s_%(class)s_parts_subset_related",
        related_query_name="%(app_label)s_%(class)s_parts_subset",
        on_delete=models.CASCADE,
        help_text="subset of parts",
    )

    order = models.IntegerField(help_text="order of part in subset")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["part", "parts_subset"],
                name="%(app_label)s_%(class)s_part_parts_subset_unique",
            ),
        ]
        ordering = ("order",)


class DeviceShape(PartShapeAbstr):
    """Part shape and dimensions"""

    model_curi = "lara:material/DeviceShape"
    deviceshape_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_dev_shape_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_dev_shape_data_extra_related_query",
        blank=True,
        help_text="device shape extra data, like 3D model or polygon describing the device shape/dimensions",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/DeviceShape/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)


class Device(PartAbstr):
    """generic device class"""

    model_curi = "lara:material/Device"
    device_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    device_class = models.ForeignKey(
        PartDeviceClass,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="UV-spectrometer, MALDI-TOF-MS, HPLC, ..",
    )
    labware_min_capacity = models.IntegerField(
        null=True,
        blank=True,
        help_text="labware capacity, min. number of labware, some centrifuges need at least 2 labware, e.g. for balancing",
    )
    labware_max_capacity = models.IntegerField(
        null=True, blank=True, help_text="labware capacity, max. number of labware"
    )
    labware_max_height = models.FloatField(
        null=True, blank=True, help_text="max. labware height"
    )
    labware_storage_layout = models.JSONField(
        null=True,
        blank=True,
        help_text="labware storage layout, like num. of stackers, num. of labware per stacker, orientation of nests, heights, ... ",
    )
    shape = models.ForeignKey(
        DeviceShape,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="shape of Device",
    )
    components = models.ManyToManyField(
        Part,
        related_query_name="%(app_label)s_%(class)s_components_related_query",
        blank=True,
        help_text="components of the device setup",
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_devices_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_devices_data_extra_related_query",
        blank=True,
        help_text="e.g. manual",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for type barcode and IRI
        """

        if self.name is None or self.name == "":
            if self.name_display is not None or self.name_display != "":
                self.name = self.name_display.replace(" ", "_").lower().strip()

        if self.name is not None or self.name != "":
            self.name = self.name.replace(" ", "_").lower().strip()

        if self.name_display is None or self.name_display == "":
            self.name_display = self.name

        if self.type_barcode == "" or self.type_barcode is None:
            self.type_barcode = (
                f"AUTO_{str(uuid.uuid4())[:8]}"  # avoiding unique constraint violation
            )

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/Device/{self.name.replace(' ', '_').lower().strip()}_{str(self.device_id)[:8]}"

        if self.datetime_last_modified is None:
            self.datetime_last_modified = timezone.now()

        super().save(*args, **kwargs)


class DevicesSubset(ItemSubsetAbstr):
    """Devices subset. It can be used for user or group defined subsets of devices."""

    model_curi = "lara:material/DevicesSubset"
    devicessubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    devices = models.ManyToManyField(
        Device,
        related_name="%(app_label)s_%(class)s_devices_subset_related",
        related_query_name="%(app_label)s_%(class)s_devices_subset_related_query",
        blank=True,
        help_text="devices in the subset",
        through="OrderedDevicesSubset",
    )


class OrderedDevicesSubset(models.Model):
    """Through model for the many-to-many relationship between DevicesSubset and Device.
    This class can be used to store the order of devices in a subset"""

    model_curi = "lara:material/OrderedDevicesSubset"

    ordereddevicessubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )

    device = models.ForeignKey(
        Device,
        related_name="%(app_label)s_%(class)s_device_related",
        related_query_name="%(app_label)s_%(class)s_device",
        on_delete=models.CASCADE,
        help_text="device in the subset",
    )

    devices_subset = models.ForeignKey(
        DevicesSubset,
        related_name="%(app_label)s_%(class)s_devices_subset_related",
        related_query_name="%(app_label)s_%(class)s_devices_subset",
        on_delete=models.CASCADE,
        help_text="subset of devices",
    )

    order = models.IntegerField(help_text="order of device in subset")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["device", "devices_subset"],
                name="%(app_label)s_%(class)s_device_devices_subset_unique",
            ),
        ]
        ordering = ("order",)


class DeviceSetupShape(PartShapeAbstr):
    """Part shape and dimensions"""

    model_curi = "lara:material/DeviceSetupShape"
    devicesetupshape_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_dss_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_dss_data_extra_related_query",
        blank=True,
        help_text="part shape extra data, like 3D model or polygon describing the part shape/dimensions",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/DeviceSetupShape/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)


class DeviceSetup(PartAbstr):
    """Setup or assembly of devices, e.g. setup of a physical or chemical experiment:
       distillation apparatus, flow reactor, ultra-high vacuum depositor, ... with all connectivities_

    :param PartAbstr: _description_
    :type PartAbstr: _type_
    """

    model_curi = "lara:material/DeviceSetup"
    devicesetup_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    setup_class = models.ForeignKey(
        PartDeviceClass,
        related_name="%(app_label)s_%(class)s_setup_classes_related",
        related_query_name="%(app_label)s_%(class)s_setup_classes_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="screw, bolt, nut, lamp, diode, resistor, IC, ...",
    )
    shape = models.ForeignKey(
        DeviceSetupShape,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="shape of Device setup",
    )
    components = models.ManyToManyField(
        Device,
        related_query_name="%(app_label)s_%(class)s_components_related_query",
        blank=True,
        help_text="components of the device setup",
    )
    blueprint_image = models.ImageField(
        upload_to="material/",
        blank=True,
        null=True,
        # default="",
        help_text="rel. path/filename to image",
    )
    blueprint_media_type = models.ForeignKey(
        MediaType,
        related_name="%(app_label)s_%(class)s_media_types_related",
        related_query_name="%(app_label)s_%(class)s_media_types_related_query",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="file type of extra data file",
    )
    blueprint_file = models.FileField(
        upload_to="material/", blank=True, null=True, help_text="blueprint file"
    )
    blueprint_pdf = models.FileField(
        upload_to="material/", blank=True, null=True, help_text="blueprint PDF file"
    )
    scheme_svg = models.TextField(
        blank=True, null=True, help_text="setup scheme in SVG"
    )
    assembly = models.JSONField(
        blank=True, null=True, help_text="assembly with connections in JSON"
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_ds_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_ds_data_extra_related_query",
        blank=True,
        help_text="e.g. manual",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for type_barcode and IRI
        """

        if self.name is None or self.name == "":
            if self.name_display is not None or self.name_display != "":
                self.name = self.name_display.replace(" ", "_").lower().strip()

        if self.name is not None or self.name != "":
            self.name = self.name.replace(" ", "_").lower().strip()

        if self.name_display is None or self.name_display == "":
            self.name_display = self.name

        if self.type_barcode == "" or self.type_barcode is None:
            self.type_barcode = (
                f"AUTO_{str(uuid.uuid4())[:8]}"  # avoiding unique constraint violation
            )

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/Device/{self.name.replace(' ', '_').lower().strip()}_{str(self.part_id)[:8]}"

        if self.datetime_last_modified is None:
            self.datetime_last_modified = timezone.now()

        super().save(*args, **kwargs)


class DeviceSetupsSubset(ItemSubsetAbstr):
    """DeviceSetups subset. It can be used for user or group defined subsets of device setups."""

    model_curi = "lara:material/DeviceSetupsSubset"
    devicesetupssubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    devicesetups = models.ManyToManyField(
        DeviceSetup,
        related_name="%(app_label)s_%(class)s_devicesetups_subset_related",
        related_query_name="%(app_label)s_%(class)s_devicesetups_subset_related_query",
        blank=True,
        help_text="device setups in the subset",
        through="OrderedDeviceSetupsSubset",
    )


class OrderedDeviceSetupsSubset(models.Model):
    """Through model for the many-to-many relationship between DeviceSetupsSubset and DeviceSetup.
    This class can be used to store the order of device setups in a subset"""

    model_curi = "lara:material/OrderedDeviceSetupsSubset"

    ordereddevicessubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )

    devicesetup = models.ForeignKey(
        DeviceSetup,
        related_name="%(app_label)s_%(class)s_devicesetup_related",
        related_query_name="%(app_label)s_%(class)s_devicesetup",
        on_delete=models.CASCADE,
        help_text="device setup in the subset",
    )

    devicesetups_subset = models.ForeignKey(
        DeviceSetupsSubset,
        related_name="%(app_label)s_%(class)s_devicesetups_subset_related",
        related_query_name="%(app_label)s_%(class)s_devicesetups_subset",
        on_delete=models.CASCADE,
        help_text="subset of device setups",
    )

    order = models.IntegerField(help_text="order of device setup in subset")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["devicesetup", "devicesetups_subset"],
                name="%(app_label)s_%(class)s_devicesetup_devicesetups_subset_unique",
            ),
        ]
        ordering = ("order",)


class LabwareClass(models.Model):
    """Labware classes, e.g., microtiterplate / MTP, tube, flask, ..."""

    model_curi = "lara:material/LabwareClass"
    labwareclass_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    name = models.TextField(unique=True)
    iri = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    description = models.TextField(
        default="no description",
        null=True,
        blank=True,
        help_text="description of the labware class",
    )

    class Meta:
        verbose_name_plural = "LabwareClasses"
        # db_table = "lara_labwares_labware_class"

    def __str__(self):
        return (self.name) or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/LabwareClass/{self.name.replace(' ', '_').lower().strip()}"
        super().save(*args, **kwargs)


class WellShape(models.Model):
    """Labware Well Shape: describing shapes of wells, round, square, buffeled, flowered,...
    this can also be used to describe the bottom shape, like flat, conical
    """

    model_curi = "lara:material/WellShape"
    wellshape_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    name = models.TextField(help_text="Well shape name")
    uri = models.URLField(
        blank=True,
        null=True,
        help_text="Universal Resource Indentifier - URI - can be used to identify the shape ",
    )
    iri = models.URLField(
        blank=True,
        null=True,
        unique=True,
        help_text="International Resource Identifier - IRI: is used for semantic representation ",
    )
    depth_well = models.FloatField(
        blank=True, null=True, help_text="total well depth=hight"
    )
    shape_well = models.TextField(
        unique=True,
        null=True,
        help_text="labware well shape,e.g. round, square, buffeled,...",
    )
    shape_bottom = models.TextField(
        unique=True, null=True, help_text="well, bottom shape, flat, round, conical"
    )
    top_radius_xy = models.FloatField(
        blank=True, null=True, help_text="radius of a round well at the top opening"
    )
    bottom_radius_xy = models.FloatField(
        blank=True, null=True, help_text="radius of a round bottom in xy direction"
    )
    bottom_radius_z = models.FloatField(
        blank=True,
        null=True,
        help_text="radius of a round bottom in z (hight) direction",
    )
    cone_angle = models.FloatField(
        blank=True, null=True, help_text="opening angle of cone in deg"
    )
    cone_depth = models.FloatField(
        blank=True, null=True, help_text="opening angle of cone in deg"
    )
    shape_polygon_xy = models.TextField(
        null=True,
        help_text="generalized shape polygon for more complex well shapes, in xy direction",
    )
    shape_polygon_z = models.TextField(
        null=True,
        help_text="generalized shape polygon for more complex well shapes, in z direction",
    )
    model_2d = models.TextField(
        blank=True, null=True, help_text="2D model of well shape in, e.g., SVG format"
    )
    model_3d = models.TextField(
        blank=True, null=True, help_text="3D model of well shape in, e.g., STL format"
    )
    model_json = models.JSONField(
        blank=True, null=True, help_text="2D or 3D model of Well shape"
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_data_extra_related_query",
        blank=True,
        help_text="well shape extra data, like 3D model or polygon describing the well shape/dimensions",
    )
    description = models.TextField(
        blank=True, null=True, help_text="description of labware shape"
    )

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/WellShape/{self.name.replace(' ', '_').lower().strip()}"

        super().save(*args, **kwargs)


class LabwareShape(PartShapeAbstr):
    """Labware and Lid shape and dimensions:
    e.g. special characteristics of a labware, like material, shape, well-shape ...
    """

    model_curi = "lara:material/LabwareShape"
    labwareshape_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    well_dist_row = models.FloatField(
        blank=True, null=True, help_text="well-to-well distance in row direction"
    )
    well_dist_col = models.FloatField(
        blank=True, null=True, help_text="well-to-well distance in column direction"
    )
    well_shape = models.ForeignKey(
        WellShape,
        related_name="%(app_label)s_%(class)s_well_shape_related",
        related_query_name="%(app_label)s_%(class)s_well_shape_related_query",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        help_text="Well shape description",
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_lws_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_lws_data_extra_related_query",
        blank=True,
        help_text="labware shape extra data, like 3D model or polygon describing the labware shape/dimensions",
    )
    description = models.TextField(
        blank=True, null=True, help_text="description of labware shape"
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for IRI
        """

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/LabwareShape/{self.name.replace(' ', '_').lower().strip()}"
        super().save(*args, **kwargs)


class Labware(PartAbstr):
    """generic labware class modelling/describing the general properties of a labware
    like geometry, volume, manufacturer.
    All concrete instances shall appear in the labware store
    """

    model_curi = "lara:material/Labware"
    labware_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    labware_class = models.ForeignKey(
        LabwareClass,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text="class of the labware, e.g. MTP-SBS/SBS, vial/HPLC,  ...",
    )
    num_rows = models.IntegerField(
        null=True, blank=True, help_text="number of rows of labware"
    )
    num_cols = models.IntegerField(
        null=True, blank=True, help_text="number of columns of labware"
    )
    num_wells = models.IntegerField(
        null=True, blank=True, help_text="number of wells - could be auto generated"
    )
    height = models.FloatField(
        null=True, blank=True, help_text="height of labware in mm"
    )
    shape = models.ForeignKey(
        LabwareShape,
        related_name="%(app_label)s_%(class)s_labware_shape_related",
        related_query_name="%(app_label)s_%(class)s_labware_shape_related_query",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text="shape of labware",
    )
    components = models.ManyToManyField(
        Part,
        related_query_name="%(app_label)s_%(class)s_components_related_query",
        blank=True,
        help_text="components of the labware",
    )
    # material # polymer, properties, like solvent tolerance, transparency, ....

    # lid info
    lids = models.ManyToManyField(
        "self", blank=True, help_text="reference to possible lids"
    )
    liddable = models.BooleanField(
        null=True, blank=True, help_text="labware is liddable"
    )

    # seal info
    sealable = models.BooleanField(
        default=False, null=True, help_text="labware is sealable"
    )
    data_extra = models.ManyToManyField(
        ExtraData,
        related_name="%(app_label)s_%(class)s_labware_data_extra_related",
        related_query_name="%(app_label)s_%(class)s_labware_data_extra_related_query",
        blank=True,
        help_text="e.g. manual",
    )

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for iri / type_barcode
        """
        if self.name is None or self.name == "":
            if self.name_display is not None or self.name_display != "":
                self.name = self.name_display.replace(" ", "_").lower().strip()

        if self.name is not None or self.name != "":
            self.name = self.name.replace(" ", "_").lower().strip()

        if self.name_display is None or self.name_display == "":
            self.name_display = self.name

        if self.type_barcode == "" or self.type_barcode is None:
            self.type_barcode = (
                f"AUTO_{str(uuid.uuid4())[:8]}"  # avoiding unique constraint violation
            )

        if self.iri is None or self.iri == "":
            self.iri = f"{settings.LARA_PREFIXES['lara']}material/Labware/{self.name.replace(' ', '_').lower().strip()}_{str(self.labware_id)[:8]}"

        if self.datetime_last_modified is None:
            self.datetime_last_modified = timezone.now()

        super().save(*args, **kwargs)


class LabwareSubset(ItemSubsetAbstr):
    """Labware subset. It can be used for user or group defined subsets of labware."""

    model_curi = "lara:material/LabwareSubset"
    labwaresubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    labware = models.ManyToManyField(
        Labware,
        related_name="%(app_label)s_%(class)s_labware_setup_related",
        related_query_name="%(app_label)s_%(class)s_labware_setup_related_query",
        blank=True,
        help_text="labwares in the subset",
        through="OrderedLabwareSubset",
    )


class OrderedLabwareSubset(models.Model):
    """Through model for the many-to-many relationship between LabwareSubset and Labware.
    This class can be used to store the order of labwares in a subset"""

    model_curi = "lara:material/OrderedLabwareSubset"

    orderedlabwaresubset_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )

    labware = models.ForeignKey(
        Labware,
        related_name="%(app_label)s_%(class)s_labware_related",
        related_query_name="%(app_label)s_%(class)s_labware",
        on_delete=models.CASCADE,
        help_text="labware in the subset",
    )

    labwares_subset = models.ForeignKey(
        LabwareSubset,
        related_name="%(app_label)s_%(class)s_labwares_subset_related",
        related_query_name="%(app_label)s_%(class)s_labwares_subset",
        on_delete=models.CASCADE,
        help_text="subset of labwares",
    )

    order = models.IntegerField(help_text="order of labware in subset")

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["labware", "labwares_subset"],
                name="%(app_label)s_%(class)s_labware_labwares_subset_unique",
            ),
        ]
        ordering = ("order",)
