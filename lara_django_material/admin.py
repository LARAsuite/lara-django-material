"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_material admin *

:details: lara_django_material admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_material >> admin.py" to update this file
________________________________________________________________________
"""

# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (
    ExtraData,
    PartDeviceClass,
    PartShape,
    Part,
    DeviceShape,
    Device,
    DeviceSetupShape,
    DeviceSetup,
    LabwareClass,
    WellShape,
    LabwareShape,
    Labware,
)


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "data_type",
        "namespace",
        "url",
        "description",
        "file",
        "media_type",
        "extradata_id",
    )
    list_filter = ("data_type", "namespace", "media_type")


@admin.register(PartDeviceClass)
class PartDeviceClassAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "partdeviceclass_id",
    )
    search_fields = ("name",)


@admin.register(PartShape)
class PartShapeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_standard",
        "width",
        "length",
        "height_unlidded",
        "hight_lidded",
        "hight_stacked",
        "hight_stacked_lidded",
        "description",
        "partshape_id",
    )
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(Part)
class PartAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        
        "product_type",
        "type_barcode",
        "model_no",
        "product_no",
        "energy_consumption",
        "part_class",
        "part_id",
    )
    list_filter = ("manufacturer", "part_class", "shape")
    raw_id_fields = ("tags", "data_extra")
    search_fields = ("name",)


@admin.register(DeviceShape)
class DeviceShapeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_standard",
        "width",
        "length",
        "height_unlidded",
        "hight_lidded",
        "hight_stacked",
        "description",
        "deviceshape_id",
    )
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        
        "product_type",
        "type_barcode",
        "model_no",
        "product_no",
        "energy_consumption",
        "device_class",
        "device_id",
    )
    list_filter = (
        "manufacturer",
        "device_class",
    )
    raw_id_fields = ("tags", "data_extra", "components")
    search_fields = ("name",)


@admin.register(DeviceSetupShape)
class DeviceSetupShapeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_standard",
        "width",
        "length",
        "height_unlidded",
        "hight_lidded",
        "hight_stacked",
        "hight_stacked_lidded",
        "description",
        "devicesetupshape_id",
    )
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(DeviceSetup)
class DeviceSetupAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "setup_class",
        "url",
       
        "model_no",
        "product_type",
        "type_barcode",
        "product_no",
        "weight",
        "energy_consumption",
        "description",
        "devicesetup_id",
    )
    list_filter = (
        "manufacturer",
        "setup_class",
        "shape",
        "blueprint_media_type",
    )
    raw_id_fields = ("tags", "components", "data_extra")
    search_fields = ("name",)


@admin.register(LabwareClass)
class LabwareClassAdmin(admin.ModelAdmin):
    list_display = ("labwareclass_id", "name", "iri", "description")
    search_fields = ("name",)


@admin.register(WellShape)
class WellShapeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "uri",
        "iri",
        "depth_well",
        "shape_well",
        "shape_bottom",
        "top_radius_xy",
        "bottom_radius_xy",
        "bottom_radius_z",
        "cone_angle",
        "cone_depth",
        "wellshape_id",
    )
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(LabwareShape)
class LabwareShapeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "name_standard",
        "width",
        "length",
        "height_unlidded",
        "hight_lidded",
        "hight_stacked",
        "hight_stacked_lidded",
        "well_dist_row",
        "well_dist_col",
        "well_shape",
        "description",
        "labwareshape_id",
    )
    list_filter = ("well_shape",)
    raw_id_fields = ("data_extra",)
    search_fields = ("name",)


@admin.register(Labware)
class LabwareAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "labware_class",
        
        "product_type",
        "type_barcode",
        "model_no",
        "product_no",
        "num_rows",
        "num_cols",
        "num_wells",
        "shape",
        "liddable",
        "sealable",
        "labware_id",
    )
    list_filter = (
        "manufacturer",
        "labware_class",
        "shape",
        "liddable",
        "sealable",
    )
    raw_id_fields = ("tags", "lids", "data_extra")
    search_fields = ("name",)
