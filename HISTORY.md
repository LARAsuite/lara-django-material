
# History  of lara_django_material 


* 0.2.10 (2023-04-30)


* First release ....

v0.2.42
-------

Bug fixes:

 - a0f8b09 Models - data_extra, api

Other changes:

 - 3cf4edc Bump version: 0.2.41 → 0.2.42

v0.2.45
-------

Bug fixes:

 - 7b07ff9 Changed some fields to be optional
 - 2514a31 GRPC API updated

Other changes:

 - ffbc3f2 Bump version: 0.2.44 → 0.2.45

v0.2.46
-------

Bug fixes:

 - 084483a Forms.py updated
 - 1ba243b Forms.py updated
 - 7297209 Forms.py updated
 - bd6ca69 GRPC API updated
 - 1cebab6 HISTORY.md
 - 9d0c3fd Literature->resources_external; handle->PID; create & update view; filters.py

Other changes:

 - ee4b88e Bump version: 0.2.45 → 0.2.46

v0.2.47
-------

Bug fixes:

 - 34c8800 Filters.py; forms.py;
 - 7f2679e HISTORY.md

Other changes:

 - 1d7df26 Bump version: 0.2.46 → 0.2.47

v0.2.48
-------

Features:

 - 71aea9b External resources added; literature, manuals, etc. removed
 - 4c836c6 Filters.py added;

Bug fixes:

 - 411d452 GRPC API updated
 - df1c455 GRPC API updated
 - 701feb2 HISTORY.md

Other changes:

 - cb539af Bump version: 0.2.47 → 0.2.48

v0.2.49
-------

Bug fixes:

 - 8a821e2 GRPC API updated
 - c75b4c3 HISTORY.md

Other changes:

 - d89e872 Bump version: 0.2.48 → 0.2.49

v0.2.50
-------

Bug fixes:

 - 5d1687c GRPC API updated
 - f3257e0 HISTORY.md

Other changes:

 - 96f5987 Bump version: 0.2.49 → 0.2.50

v0.2.51
-------

Bug fixes:

 - 3239935 HISTORY.md
 - 838f7f0 Serializers.py well_shape, extra_data

Other changes:

 - 4bb27ec Bump version: 0.2.50 → 0.2.51

v0.2.52
-------

Bug fixes:

 - 2d6e862 GRPC API updated
 - ea4498a HISTORY.md
 - d7c1ba4 Missing serializer import added

Other changes:

 - 8dec324 Bump version: 0.2.51 → 0.2.52

v0.2.53
-------

Bug fixes:

 - c5cd1d8 HISTORY.md
 - 5d806bd Static files moved to subfolder

Other changes:

 - bf825d3 Bump version: 0.2.52 → 0.2.53

v0.2.54
-------

Bug fixes:

 - 79b3205 GRPC API updated
 - df688b0 HISTORY.md

Other changes:

 - af7367d Bump version: 0.2.53 → 0.2.54

v0.2.55
-------

Features:

 - e6008a7 Improved gRPC filters / queries

Bug fixes:

 - 21191e3 HISTORY.md

Other changes:

 - 7e4ab75 Bump version: 0.2.54 → 0.2.55

v0.2.56
-------

Bug fixes:

 - 6bf70a6 ExtraDataProtoserializer - data_type, media_type added
 - 0960909 GRPC API updated
 - 4060ef0 GRPC API updated
 - e63be5f GRPC API updated
 - c00ae18 HISTORY.md
 - 1008842 More consistent naming of variables; improved gRPC interface.
 - 9443e95 Remove URN from fixture

Other changes:

 - d326f83 Bump version: 0.2.55 → 0.2.56

v0.2.57
-------

Features:

 - 49fd464 New LinkML schema added
 - e56f1bd New LinkML schema added
 - e84e16f New LinkML schema added
 - 4c628f3 New LinkML schema, data model and high-level interface added

Bug fixes:

 - b28a3e1 GRPC API updated
 - 2933775 HISTORY.md
 - 01bd309 Improved schema, serializer.py
 - e8eebfa More consistent naming of variables; improved gRPC interface.

Other changes:

 - bcc078c Bump version: 0.2.56 → 0.2.57

v0.2.57-29-g0c3a512
-------------------

Features:

 - 0381d0b New high-level interface generated
 - 0c3a512 New high-level interface generated
 - 1eec22d New high-level interface generated
 - 2bab0ff New high-level interface generated
 - 37b9692 New high-level interface generated
 - 3e40a8f New high-level interface generated
 - 6c1f7dc New high-level interface generated
 - 927a564 New high-level interface generated
 - a05978a New high-level interface generated
 - a489da8 New high-level interface generated
 - c6430b4 New high-level interface generated
 - f1ba414 New high-level interface generated
 - 08914ea New LinkML schema, data model
 - 0e54fb4 New LinkML schema, data model
 - 7f48c60 New LinkML schema, data model
 - f99028e New LinkML schema, data model
 - 2fab8b4 New LinkML schema, data model and high-level interface added
 - 7075487 New LinkML schema, data model and high-level interface added
 - db9d2b4 New LinkML schema, data model and high-level interface added
 - 1ffa356 New LinkML schema, template, data model and high-level interface added
 - 300a0af New LinkML schema, template, data model and high-level interface added

Bug fixes:

 - 0e1837a Barcode unique
 - 47b7320 GRPC API updated
 - c9fe2e9 GRPC API updated
 - d8916a9 GRPC API updated
 - 7638368 HISTORY.md
 - e733826 More consistent, PEP8 compliant naming of fields and variables;
 - c18b413 PEP8 compliant field names (all lower case); consitent id (PK) names;  gRPC API updated

v0.2.57-29-g0c3a512
-------------------

Features:

 - 0381d0b New high-level interface generated
 - 0c3a512 New high-level interface generated
 - 1eec22d New high-level interface generated
 - 2bab0ff New high-level interface generated
 - 37b9692 New high-level interface generated
 - 3e40a8f New high-level interface generated
 - 6c1f7dc New high-level interface generated
 - 927a564 New high-level interface generated
 - a05978a New high-level interface generated
 - a489da8 New high-level interface generated
 - c6430b4 New high-level interface generated
 - f1ba414 New high-level interface generated
 - 08914ea New LinkML schema, data model
 - 0e54fb4 New LinkML schema, data model
 - 7f48c60 New LinkML schema, data model
 - f99028e New LinkML schema, data model
 - 2fab8b4 New LinkML schema, data model and high-level interface added
 - 7075487 New LinkML schema, data model and high-level interface added
 - db9d2b4 New LinkML schema, data model and high-level interface added
 - 1ffa356 New LinkML schema, template, data model and high-level interface added
 - 300a0af New LinkML schema, template, data model and high-level interface added

Bug fixes:

 - 0e1837a Barcode unique
 - 47b7320 GRPC API updated
 - c9fe2e9 GRPC API updated
 - d8916a9 GRPC API updated
 - 7638368 HISTORY.md
 - e733826 More consistent, PEP8 compliant naming of fields and variables;
 - c18b413 PEP8 compliant field names (all lower case); consitent id (PK) names;  gRPC API updated

v0.2.69
-------

Features:

 - d586f26 New high-level interface generated
 - de22bb8 New high-level interface generated

Other changes:

 - 911a204 Bump version: 0.2.68 → 0.2.69

v0.2.70
-------

Bug fixes:

 - 51a4f5e             HISTORY.md
 - 456d816 [ issue #1] Manually made shape optional

Other changes:

 - fb5f0bb             Bump version: 0.2.69 → 0.2.70

v0.2.71
-------

Bug fixes:

 - 98df9f0             HISTORY.md
 - 3f9bd74 [ issue #1] Manually made shape optional

Other changes:

 - 3f33fd0             Bump version: 0.2.70 → 0.2.71

v0.2.72
-------

Bug fixes:

 - db57563 Extra_data optional
 - 2f59a9d HISTORY.md

Other changes:

 - 1b64d16 Bump version: 0.2.71 → 0.2.72

v0.2.73
-------

Bug fixes:

 - cbfa401 GRPC API updated
 - ffcb77e HISTORY.md
 - 227f210 Models.py URI -> uri; barcode -> type_barcode

Other changes:

 - bc756d3 Bump version: 0.2.72 → 0.2.73

v0.2.74
-------

Bug fixes:

 - 8797a3b HISTORY.md

Other changes:

 - 1f687ae Bump version: 0.2.73 → 0.2.74

v0.2.75
-------

Bug fixes:

 - e194ed8 HISTORY.md
 - 68e4d17 Serializers.py many2many relations fixed

Other changes:

 - 6fbc8ff Bump version: 0.2.74 → 0.2.75

v0.2.76
-------

Bug fixes:

 - a1510ea HISTORY.md
 - 42d9711 .proto making some fields optional manually;

Other changes:

 - 721ca98 Bump version: 0.2.75 → 0.2.76

v0.2.77
-------

Bug fixes:

 - a3b350c HISTORY.md
 - 72fd7ae .proto making some fields optional manually;

Other changes:

 - 5266d09 Bump version: 0.2.76 → 0.2.77

v0.2.78
-------

Bug fixes:

 - 4d506d7 HISTORY.md

Other changes:

 - 61282f5 Bump version: 0.2.77 → 0.2.78

v0.2.79
-------

Bug fixes:

 - 19c8727 HISTORY.md
 - 53c22ff .proto making some fields optional manually;

Other changes:

 - 3c02b8c Bump version: 0.2.78 → 0.2.79

v0.2.80
-------

Bug fixes:

 - e71813b HISTORY.md
 - ca02553 Improved API - DeviceShape creation is working now

Other changes:

 - b05cf1b Bump version: 0.2.79 → 0.2.80

v0.2.81
-------

Features:

 - 7ff08af GRPC unittests added
 - ad9293f GRPC unittests added
 - af0b601 New LinkML schema, template, data model and high-level interface added

Bug fixes:

 - 4115ed2 GRPC API updated
 - 5abbaff HISTORY.md

Other changes:

 - 2493ee0 Bump version: 0.2.80 → 0.2.81

v0.2.82
-------

Features:

 - 768ad68 Devicesetupshape tests working
 - c06bfff Extradata tests working
 - c2cc2ef GRPC unittests added
 - 3b102f5 Labwareclass tests working
 - e2cd6ee New LinkML schema, template, data model and high-level interface added
 - c74ad41 Partshape and deviceshape tests working

Bug fixes:

 - 6c6bb6c GRPC API updated
 - 7f7667d HISTORY.md
 - 1808069 Labware tests working

Other changes:

 - 1ef347d Bump version: 0.2.81 → 0.2.82

v0.2.83
-------

Bug fixes:

 - b87969c GRPC API updated
 - fdfc9e0 HISTORY.md
 - 2384d36 Improved gRPC interface: serializers, .proto, API, unittests

Other changes:

 - b0a05e3 Bump version: 0.2.82 → 0.2.83

v0.2.84
-------

Bug fixes:

 - 36396e7 HISTORY.md

Other changes:

 - 7b39092 Bump version: 0.2.83 → 0.2.84

v0.2.85
-------

Bug fixes:

 - 57f941c Device full   gRPC API tests pass
 - c4b970a Extradata full   gRPC API tests pass
 - e4aa3bb HISTORY.md
 - cf6d296 Tests - intermediate gRPC API tests pass

Other changes:

 - 8af8862 Bump version: 0.2.84 → 0.2.85

v0.2.86
-------

Features:

 - 51af338 New LinkML schema, template, data model

Bug fixes:

 - f541a91 GRPC API updated
 - 24f7ab6 HISTORY.md

Other changes:

 - 41ce16c Bump version: 0.2.85 → 0.2.86

v0.2.87
-------

Bug fixes:

 - 7e678e6 GRPC API updated
 - 57a71a2 HISTORY.md

Other changes:

 - d8141f4 Bump version: 0.2.86 → 0.2.87

v0.2.88
-------

Bug fixes:

 - cf16a1e GRPC API updated
 - 46bda41 HISTORY.md

Other changes:

 - 9706497 Bump version: 0.2.87 → 0.2.88

v0.2.89
-------

Features:

 - 4cc31bf New LinkML schema, template, data model

Bug fixes:

 - 5ed2378 GRPC API updated
 - b7ba26a HISTORY.md

Other changes:

 - 6a1599a Bump version: 0.2.88 → 0.2.89

v0.2.90
-------

Bug fixes:

 - 0086ff2 All full  gRPC API tests pass - minor problem in labware and DeviceSetup
 - da05ce7 HISTORY.md
 - ff14c10 PartDeviceClass full gRPC API tests pass
 - bfcafa3 Part full gRPC API tests pass

Other changes:

 - e9f37cb Bump version: 0.2.89 → 0.2.90

v0.2.91
-------

Features:

 - 9c6258c Components to all materials added

Bug fixes:

 - c4bef99 GRPC API updated
 - 7e98c92 HISTORY.md

Other changes:

 - 65823ad Bump version: 0.2.90 → 0.2.91

v0.2.92
-------

Features:

 - 7cf77d3 New LinkML schema, template, data model and high-level interface added

Bug fixes:

 - 88df263 CamelCase of class names; gRPC API updated
 - f6e0591 GRPC API updated
 - 792594d HISTORY.md

Other changes:

 - b818768 Bump version: 0.2.91 → 0.2.92

v0.2.93
-------

Features:

 - cf84f3a New gRPC unittests added
 - 3958d7f New LinkML schema, template, data model and high-level interface added

Bug fixes:

 - ca2ccc2 HISTORY.md

Other changes:

 - 47a92af Bump version: 0.2.92 → 0.2.93

v0.2.94
-------

Bug fixes:

 - 98bab05 HISTORY.md
 - 13133a5 Improved gRPC interface: serializers, .proto, API, unittests
 - 32d18f7 Part, DeviceSetup full gRPC API tests pass

Other changes:

 - 5dd52b5 Bump version: 0.2.93 → 0.2.94

v0.2.95
-------

Bug fixes:

 - 7fb6cde All full  gRPC API tests pass
 - defc005 HISTORY.md
 - 87f8add W3c.org -> w3id.org; apps.py path_url

Other changes:

 - 78dfb19 Bump version: 0.2.94 → 0.2.95

v0.2.96
-------

Features:

 - 454acbd Buf config updated
 - 1f129f3 PAC-ID added; buf config updated

Bug fixes:

 - 281ae33 HISTORY.md
 - 4865cf4 Name_full removed in model.py
 - 614ecb4 Name_full removed in model.py
 - bd35974 Nameing of constraints in models.py
 - 381e204 Old api version removed
 - 8016d51 PartAbstr - constraints added

Other changes:

 - c87a90e Bump version: 0.2.95 → 0.2.96

v0.2.97
-------

Features:

 - fb60206 New gRPC unittests added
 - 5fa31ff New LinkML schema, template, data model and high-level interface added

Bug fixes:

 - 2fbb8be HISTORY.md

Other changes:

 - 02feb9a Bump version: 0.2.96 → 0.2.97

v0.2.98
-------

Bug fixes:

 - de9c764 HISTORY.md
 - 065d9fb New api generated
 - 0bcaa35 New high-level interface generated

Other changes:

 - 4a2a7bd Bump version: 0.2.97 → 0.2.98

v0.2.99
-------

Features:

 - cc723e8 Fields renamed title -> name_display; full text search SearchVectorField added; through model for subset ordering added
 - 6d24b44 Hardware_config field added
 - 0cb047c New LinkML schema, template, data model generated

Bug fixes:

 - d3108b1 GRPC API updated
 - d263d72 HISTORY.md
 - 57ff073 Renamed: manufacturer -> manufacturers changed to many2many field
 - 4d7fdc1 Renamed: tag -> tags

Other changes:

 - abac0ee Bump version: 0.2.98 → 0.2.99

v0.2.100
--------

Features:

 - 697869a New  high-level interface added

Bug fixes:

 - 88273be GRPC API updated
 - d25209d GRPC API updated
 - e639f8b HISTORY.md

Other changes:

 - 7f71b8e Bump version: 0.2.99 → 0.2.100

v0.2.101
--------

Features:

 - 0927c1a Decentralised url settings

Bug fixes:

 - ddc24b8 HISTORY.md

Other changes:

 - a030750 Bump version: 0.2.100 → 0.2.101

